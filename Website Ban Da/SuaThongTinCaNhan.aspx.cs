﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class SuaThongTinCaNhan : System.Web.UI.Page
{
    KhachHangBusiness khbn = new KhachHangBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["KHACHHANGDN"] == null) Response.Redirect("~/DangNhap.aspx");
        DataTable dt = (DataTable)Session["KHACHHANGDN"];
        txtTenKhachHang.Text = dt.Rows[0]["TENKHACHHANG"].ToString();
        txtDiaChi.Text = dt.Rows[0]["DIACHI"].ToString();
        txtDienThoai.Text = dt.Rows[0]["DIENTHOAI"].ToString();
        txtEmail.Text = dt.Rows[0]["EMAIL"].ToString();
        DateTime NgaySinh = Convert.ToDateTime(dt.Rows[0]["NGAYSINH"].ToString());
        txtNgay.Text = NgaySinh.Day.ToString();
        txtThang.Text = NgaySinh.Month.ToString();
        txtNam.Text = NgaySinh.Year.ToString();
        ddlGioiTinh.SelectedIndex = (dt.Rows[0]["GIOITINH"].ToString() == "True") ? 0 : 1;
    }
    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = (DataTable)Session["KHACHHANGDN"];
            khbn.CapNhatKhachHang(dt.Rows[0]["MAKHACHHANG"].ToString(), txtTenKhachHang.Text, txtDienThoai.Text, txtThang.Text + "/" + txtNgay.Text + "/" + txtNam.Text, ddlGioiTinh.SelectedValue, dt.Rows[0]["TENDANGNHAP"].ToString(), dt.Rows[0]["MATKHAU"].ToString(), txtDiaChi.Text, txtEmail.Text, dt.Rows[0]["DADUYET"].ToString());
            Session["KHACHHANGDN"] = khbn.LoadMotKhachHang(dt.Rows[0]["MAKHACHHANG"].ToString());
            Response.Write("<script>alert('Cập Nhật Thành Công!')</script>");
        }
        catch
        {
            Response.Write("<script>alert('Cập Nhật Không Thành Công!')</script>");
        }
    }
}