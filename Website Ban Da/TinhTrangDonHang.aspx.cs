﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class TinhTrangDonHang : System.Web.UI.Page
{
    DonDatHangBusiness ddhbn = new DonDatHangBusiness();
    CTDatHang ctdh = new CTDatHang();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblBaoLoi.Text = "";
        if (Request.QueryString["SoDonHang"]!=null)
        {
            DataTable dtct = ddhbn.LoadDonHangCT(Request.QueryString["SoDonHang"]);
            if (dtct.Rows.Count == 0)
            {
                lblBaoLoi.Text = "Đơn Hàng Này Không Tồn Tại. Vui lòng kiểm tra lại!";
                return;
            }
            lblThongTinChung.Text = "Quý khách " + dtct.Rows[0]["TENKHACHHANG"].ToString() + " đã đặt vào ngày " + dtct.Rows[0]["NGAYDATHANG"].ToString() + " với hình thức thanh toán " + dtct.Rows[0]["TENHINHTHUCTT"].ToString() + " và " + dtct.Rows[0]["TENHINHTHUCGH"].ToString() + " với thời gian giao hàng dự kiến: " + dtct.Rows[0]["NGAYGIAO"].ToString();
            lblDiaChi.Text = "Giao tới " + dtct.Rows[0]["DIACHI"].ToString();
            if (dtct.Rows[0]["TINHTRANGDONHANG"].ToString() == "1")
            {
                imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/1.jpg";
            }
            if (dtct.Rows[0]["TINHTRANGDONHANG"].ToString() == "2")
            {
                imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/2.jpg";
            }
            if (dtct.Rows[0]["TINHTRANGDONHANG"].ToString() == "3")
            {
                imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/3.jpg";
            }
            if (dtct.Rows[0]["TINHTRANGDONHANG"].ToString() == "4")
            {
                imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/4.jpg";
            }
            if (dtct.Rows[0]["TINHTRANGDONHANG"].ToString() == "5")
            {
                imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/5.jpg";
            }
            gvChitiet.DataSource = ctdh.LoadCTDatHangCT(Request.QueryString["SoDonHang"]);
            gvChitiet.DataBind();
        }
    }
    protected void btnKiemTra_Click(object sender, EventArgs e)
    {
        lblBaoLoi.Text = "";
        DataTable dt = ddhbn.LoadDonHang();
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["SODONHANG"].ToString()==txtMaDonHang.Text)
            {
                DataTable dtct = ddhbn.LoadDonHangCT(dr["SODONHANG"].ToString());
                lblThongTinChung.Text = "Quý khách " + dtct.Rows[0]["TENKHACHHANG"].ToString() + " đã đặt vào ngày " + dtct.Rows[0]["NGAYDATHANG"].ToString() + " với hình thức thanh toán " + dtct.Rows[0]["TENHINHTHUCTT"].ToString() + " và " + dtct.Rows[0]["TENHINHTHUCGH"].ToString() + " với thời gian giao hàng dự kiến: " + dtct.Rows[0]["NGAYGIAO"].ToString();
                lblDiaChi.Text = "Giao tới " + dtct.Rows[0]["DIACHI"].ToString();
                if (dr["TINHTRANGDONHANG"].ToString()=="1")
                {
                    imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/1.jpg";
                }
                if (dr["TINHTRANGDONHANG"].ToString() == "2")
                {
                    imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/2.jpg";
                }
                if (dr["TINHTRANGDONHANG"].ToString() == "3")
                {
                    imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/3.jpg";
                }
                if (dr["TINHTRANGDONHANG"].ToString() == "4")
                {
                    imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/4.jpg";
                }
                if (dr["TINHTRANGDONHANG"].ToString() == "5")
                {
                    imgtinhtrang.ImageUrl = "~/Image/tinhtranggiaohang/5.jpg";
                }
                gvChitiet.DataSource = ctdh.LoadCTDatHangCT(dr["SODONHANG"].ToString());
                gvChitiet.DataBind();
                return;
            }
        }
        lblBaoLoi.Text = "Không Có Đơn Hàng Này. Vui lòng kiểm tra lại!";
    }
    protected void gvChitiet_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}