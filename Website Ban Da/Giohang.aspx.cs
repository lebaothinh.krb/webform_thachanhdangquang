﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class Giohang : System.Web.UI.Page
{
    GioHang gh = new GioHang();
    DonDatHangBusiness ddhbn = new DonDatHangBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["MUAHANG"] = null;
            if (Request.QueryString["MASANPHAM"] != null)
            {
                gh.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MASANPHAM"].ToString()));
            }
            if (Session["GIOHANG"] != null)
            {
                btnCapNhat.Visible = true;
                btnDatHang.Visible = true;
                btnXoaGioHang.Visible = true;
                lblThongBao.Visible = false;
                DataTable dt = new DataTable();
                dt = (DataTable)Session["GIOHANG"];
                System.Decimal TongThanhTien = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    dr["THANHTIEN"] = Convert.ToInt32(dr["SOLUONG"]) * Convert.ToDecimal(dr["DONGIA"]);
                    TongThanhTien += Convert.ToDecimal(dr["THANHTIEN"]);
                    lblTongThanhTien.Text = TongThanhTien.ToString();
                }
                gvGiohang.DataSource = dt;
                gvGiohang.DataBind();
            }
            else
            {
                btnCapNhat.Visible = false;
                btnDatHang.Visible = false;
                btnXoaGioHang.Visible = false;
                lblThongBao.Visible = true;
            }
        }
    }
    protected void btnDatHang_Click(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)Session["GIOHANG"];
        foreach (GridViewRow r in gvGiohang.Rows)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToString(gvGiohang.DataKeys[r.DataItemIndex].Value) == dr["MASANPHAM"].ToString())
                {
                    TextBox t = (TextBox)r.Cells[2].FindControl("txtSoLuong");
                    if (Convert.ToInt32(t.Text) <= 0)
                    {
                        dt.Rows.Remove(dr);
                    }
                    else dr["SOLUONG"] = t.Text;
                    break;
                }
            }
        }
        Session["GIOHANG"] = dt;
        
        if (Session["KHACHHANGDN"] == null)
        {
            Response.Redirect("~/ThanhToanKhongDangNhap.aspx");
        }
        else
        {
            Response.Redirect("~/ThanhToan.aspx");
        }
    }

    protected void btnTiepTucMua_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }
    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)Session["GIOHANG"];
        foreach (GridViewRow r in gvGiohang.Rows)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToString(gvGiohang.DataKeys[r.DataItemIndex].Value) == dr["MASANPHAM"].ToString())
                {
                    TextBox t = (TextBox)r.Cells[2].FindControl("txtSoLuong");
                    if (Convert.ToInt32(t.Text) <= 0)
                    {
                        dt.Rows.Remove(dr);
                    }
                    else dr["SOLUONG"] = t.Text;
                    break;
                }
            }
        }
        Session["GIOHANG"] = dt;
        Response.Redirect("~/Giohang.aspx");
    }
    protected void gvGiohang_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "XoaSanPham")
        {
            int chiso = int.Parse(e.CommandArgument.ToString());
            try
            {
                DataTable dt = (DataTable)Session["GIOHANG"];
                dt.Rows.RemoveAt(chiso);
                Session["GIOHANG"] = dt;
                Response.Redirect("~/Giohang.aspx");
            }
            catch
            {
                Response.Redirect("~/Giohang.aspx");
            }
        }
    }
    protected void btnXoaGioHang_Click(object sender, EventArgs e)
    {
        Session["GIOHANG"] = null;
        Response.Redirect("~/Default.aspx");
    }
}