﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThanhToan : System.Web.UI.Page
{
    SanPhamBusiness spbn = new SanPhamBusiness();
    KhachHangBusiness khbn = new KhachHangBusiness();
    HinhThucGiaoHang htgh = new HinhThucGiaoHang();
    HinhThucThanhToan httt = new HinhThucThanhToan();
    DonDatHangBusiness ddhbn = new DonDatHangBusiness();
    CTDatHang ctdh = new CTDatHang();
    GioHang ghbn = new GioHang();
    Email email = new Email();
    int ngaygiao;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["KHACHHANGDN"] == null)
            Response.Redirect("~/ThanhToanKhongDangNhap.aspx");
        if (rdlThanhToan.SelectedValue=="3")
        {
            btnDongY.Visible = false;
            ibtnNganLuong.Visible = true;
        }
        else
        {
            btnDongY.Visible = true;
            ibtnNganLuong.Visible = false;
        }
        DataTable dtgh = htgh.LoadHinhThucGiaoHang();
        foreach (DataRow dr in dtgh.Rows)
        {
            if (dr["MAHINHTHUCGH"].ToString()==rdlGiaoHang.SelectedValue.ToString())
            {
                Calendar1.SelectedDate = DateTime.Today.AddDays(Convert.ToInt32(dr["THOIGIANDUKIEN"]));
                ngaygiao = Convert.ToInt32(dr["THOIGIANDUKIEN"]);
                break;
            }
        }
        if (!IsPostBack)
        {
            rdlGiaoHang.DataSource = htgh.LoadHinhThucGiaoHang();
            rdlGiaoHang.DataValueField = "MAHINHTHUCGH";
            rdlGiaoHang.DataTextField = "TENHINHTHUCGH";
            rdlGiaoHang.DataBind();
            rdlThanhToan.DataSource = httt.LoadHinhThucTT();
            rdlThanhToan.DataTextField = "TENHINHTHUCTT";
            rdlThanhToan.DataValueField = "MAHINHTHUCTT";
            rdlThanhToan.DataBind();
            if (Session["GIOHANG"] != null)
            {
                DataTable dt1 = (DataTable)Session["KHACHHANGDN"];
                lblHoTen.Text = dt1.Rows[0]["TENKHACHHANG"].ToString();
                lblDiaChi.Text = dt1.Rows[0]["DIACHI"].ToString();
                lblDienThoai.Text = dt1.Rows[0]["DIENTHOAI"].ToString();
                lblEmail.Text = dt1.Rows[0]["EMAIL"].ToString();
                DataTable dt = new DataTable();
                dt = (DataTable)Session["GIOHANG"];
                System.Decimal TongThanhTien = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    dr["THANHTIEN"] = Convert.ToInt32(dr["SOLUONG"]) * Convert.ToDecimal(dr["DONGIA"]);
                    TongThanhTien += Convert.ToDecimal(dr["THANHTIEN"]);
                    lblTongThanhTien.Text = TongThanhTien.ToString();
                }
                gvGiohangcuoi.DataSource = dt;
                gvGiohangcuoi.DataBind();
            }
        }
    }
    protected void btnDongY_Click(object sender, EventArgs e)
    {
        Double TriGia=0;
        DataTable dthtgh = htgh.LoadHinhThucGiaoHang();
        foreach (DataRow dr in dthtgh.Rows)
        {
            if (dr["MAHINHTHUCGH"].ToString() ==rdlGiaoHang.SelectedValue.ToString())
            {
                TriGia+= Convert.ToDouble(dr["GIATIENGIAO"].ToString());
                TriGia+= ghbn.TinhTongTien();
                break;
            }
        }
        DataTable dt = (DataTable)Session["KHACHHANGDN"];
        ddhbn.ThemDonHang(dt.Rows[0]["MAKHACHHANG"].ToString(), DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year, Calendar1.SelectedDate.Month + "/" + Calendar1.SelectedDate.Day + "/" + Calendar1.SelectedDate.Year, TriGia.ToString(),"1","False", txtTenNguoiNhan.Text, txtDiaChi.Text, txtDienThoai.Text, rdlThanhToan.SelectedValue, rdlGiaoHang.SelectedValue, txtGhiChu.Text);
        DataTable dtct = (DataTable)Session["GIOHANG"];
        foreach (DataRow dr in dtct.Rows)
        {
            ctdh.ThemCTDatHang(ddhbn.TimMaDonHangMax(), dr["MASANPHAM"].ToString(), Convert.ToInt32(dr["SOLUONG"].ToString()), Convert.ToDouble(dr["DONGIA"].ToString()));
        }
        DataTable HinhMinhHoa = spbn.LoadSanPham();
        email.GuiMail(lblHoTen.Text,txtTenNguoiNhan.Text, lblEmail.Text, "Thạch Ảnh Đăng Quang Đã Nhận Được Đơn Hàng Của Quý Khách", Calendar1.SelectedDate.Day + "/" + Calendar1.SelectedDate.Month + "/" + Calendar1.SelectedDate.Year, DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year, txtDienThoai.Text, ddhbn.TimMaDonHangMax(), txtDiaChi.Text, rdlThanhToan.SelectedItem.Text,(DataTable)Session["GIOHANG"],HinhMinhHoa);
        Response.Redirect("~/Xacnhandonhang.aspx");
    }
    protected void ibtnNganLuong_Click(object sender, ImageClickEventArgs e)
    {
        Double TriGia = 0;
        DataTable dthtgh = htgh.LoadHinhThucGiaoHang();
        foreach (DataRow dr in dthtgh.Rows)
        {
            if (dr["MAHINHTHUCGH"].ToString() == rdlGiaoHang.SelectedValue.ToString())
            {
                TriGia += Convert.ToDouble(dr["GIATIENGIAO"].ToString());
                TriGia += ghbn.TinhTongTien();
                break;
            }
        }
        DataTable dt = (DataTable)Session["KHACHHANGDN"];
        ddhbn.ThemDonHang(dt.Rows[0]["MAKHACHHANG"].ToString(),DateTime.Now.Month+"/"+DateTime.Now.Day+"/"+DateTime.Now.Year,Calendar1.SelectedDate.Month+"/"+Calendar1.SelectedDate.Day+"/"+Calendar1.SelectedDate.Year, TriGia.ToString(),"1", "False", txtTenNguoiNhan.Text,txtDiaChi.Text,txtDienThoai.Text, rdlThanhToan.SelectedValue, rdlGiaoHang.SelectedValue,txtGhiChu.Text);
        DataTable dtct = (DataTable)Session["GIOHANG"];
        foreach (DataRow dr in dtct.Rows)
        {
            ctdh.ThemCTDatHang(ddhbn.TimMaDonHangMax(), dr["MASANPHAM"].ToString(), Convert.ToInt32(dr["SOLUONG"].ToString()), Convert.ToDouble(dr["DONGIA"].ToString()));
        }
        DataTable HinhMinhHoa = spbn.LoadSanPham();
        email.GuiMail(lblHoTen.Text, txtTenNguoiNhan.Text, lblEmail.Text, "Thạch Ảnh Đăng Quang Đã Nhận Được Đơn Hàng Của Quý Khách", Calendar1.SelectedDate.Day + "/" + Calendar1.SelectedDate.Month + "/" + Calendar1.SelectedDate.Year, DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year, txtDienThoai.Text, ddhbn.TimMaDonHangMax(), txtDiaChi.Text, rdlThanhToan.SelectedItem.Text, (DataTable)Session["GIOHANG"], HinhMinhHoa);
        Response.Redirect("https://www.nganluong.vn/button_payment.php?receiver=lebaothinh.krb@gmail.com&product_name=" + ddhbn.TimMaDonHangMax() + "&price=" + TriGia + "&return_url=(http://localhost:1170/Xacnhandonhang.aspx)&comments="+txtGhiChu.Text);
    }
    protected void rdlGiaoHang_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
}