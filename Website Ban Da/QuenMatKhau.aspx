﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="QuenMatKhau.aspx.cs" Inherits="QuenMatKhau" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewsanpham
        {
            margin:20px auto;
        }
        .nutguiquen
        {
            background:#EC7115;
            color:white;
            font-weight:bold;
        }
        p
        {
            color:black;
        }
        .auto-style5 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table class="viewsanpham">
        <tr>
            <td>
    <asp:Panel ID="pnQuenMatKhau" runat="server">
        <h2><span class="auto-style2">Bạn quên mật khẩu?</h2>
        <p>&nbsp;</p>
        <p>Nhập địa chỉ email của bạn dưới đây và chúng tôi sẽ gửi cho bạn một liên kết để đặt lại mật khẩu của bạn.</p>

        <p>&nbsp;</p>
        <p>Email:</span></p>
        <asp:TextBox ID="txtEmail" runat="server" Height="20px" TextMode="Email" Width="200px"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnGui" runat="server" Text="Gửi" CssClass="nutguiquen" Height="30px" Width="100px" BorderStyle="None" OnClick="btnGui_Click" />
        &nbsp;<asp:HyperLink ID="hyQuayLai" runat="server" NavigateUrl="~/Default.aspx">Quay Lại Chọn Sản Phẩm</asp:HyperLink>
        <br />
        <br />
    </asp:Panel>
    <asp:Panel ID="pnXacNhanEmail" runat="server" style="color: #000000">
        <br />
        <br />
        Vui lòng nhập lại email của bạn:&nbsp;<asp:TextBox ID="txtNhapLaiEmail" runat="server" Height="20px" TextMode="Email" Width="200px"></asp:TextBox>
        &nbsp;<asp:Button ID="btnNhapLaiEmail" runat="server" BorderStyle="None" CssClass="nutguiquen" OnClick="btnNhapLaiEmail_Click" Text="OK" Width="100px" />
        <br />
        <br />
    </asp:Panel>
    <asp:Panel ID="pnDoiMatKhau" runat="server" style="color: #000000; text-align: center; font-size: large">
        <table class="auto-style3">
            <tr>
                <td colspan="2">ĐẶT LẠI MẬT KHẨU</td>
            </tr>
            <tr>
                <td>Mật Khẩu</td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtMatKhau" runat="server" Height="20px" TextMode="Password" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Nhập Lại Mật Khẩu</td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtNhapLaiMatKhau" runat="server" Height="20px" TextMode="Password" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4" colspan="2">
                    <asp:Button ID="Button1" runat="server" BorderStyle="None" CssClass="nutguiquen" Height="30px" OnClick="Button1_Click" Text="OK" Width="100px" />
                </td>
            </tr>
        </table>

    </asp:Panel>
    <asp:Label ID="lblBaoLoi" runat="server" style="color: #000000"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

