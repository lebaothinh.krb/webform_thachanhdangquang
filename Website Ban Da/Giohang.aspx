﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="Giohang.aspx.cs" Inherits="Giohang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        a
        {
            text-decoration:none;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            text-align: center;
            height: 18px;
        }
        .viewsanpham
        {
            margin-left:auto;
            margin-right:auto;
            margin-top:20px;
            margin-bottom:20px;
        }
        .nut
        {
            background:#FF6A00;
            height:30px;
            width:100px;
            color:white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table class="viewsanpham">
        <tr>
            <td class="auto-style3" colspan="4">THÔNG TIN GIỎ HÀNG CỦA BẠN</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblThongBao" runat="server" Font-Bold="True" ForeColor="Black" Text="Giỏ Hàng Trống!"></asp:Label>
&nbsp;<asp:GridView ID="gvGiohang" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="MASANPHAM" OnRowCommand="gvGiohang_RowCommand" Width="745px">
        <Columns>
            <asp:BoundField DataField="MASANPHAM" HeaderText="Mã Sản Phẩm" />
            <asp:BoundField DataField="TENSANPHAM" HeaderText="Tên Sản Phẩm" />
            <asp:BoundField DataField="DONGIA" HeaderText="Đơn Giá" />
            <asp:TemplateField HeaderText="Số Lượng">
                <ItemTemplate>
                    <asp:TextBox ID="txtSoLuong" runat="server" Text='<%# Eval("SoLuong") %>'></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="THANHTIEN" HeaderText="Thành Tiền" />
            <asp:ButtonField ButtonType="Image" HeaderText="Xóa" ImageUrl="~/Image/delete.png" CommandName="XoaSanPham" />
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="auto-style3">TỔNG CỘNG:</td>
            <td>
                <asp:Label ID="lblTongThanhTien" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Button ID="btnCapNhat" runat="server" CssClass="nut" OnClick="btnCapNhat_Click" Text="Cập Nhật" BorderStyle="None" />
            </td>
            <td class="auto-style2">
                <asp:Button ID="btnTiepTucMua" runat="server" CssClass="nut" OnClick="btnTiepTucMua_Click" Text="Tiếp Tục Mua" BorderStyle="None" />
            </td>
            <td class="auto-style4">
                <asp:Button ID="btnXoaGioHang" runat="server" CssClass="nut" OnClick="btnXoaGioHang_Click" Text="Xóa Giỏ Hàng" BorderStyle="None" />
            </td>
            <td class="auto-style2">
                <asp:Button ID="btnDatHang" runat="server" CssClass="nut" OnClick="btnDatHang_Click" Text="Đặt Hàng" BorderStyle="None" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="lblThongBaoLoi" runat="server"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

