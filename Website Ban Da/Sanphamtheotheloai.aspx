﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="Sanphamtheotheloai.aspx.cs" Inherits="Sanphamtheotheloai" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="Scripts/CSS/Sanphamtheochude.css" />
    <link rel="stylesheet" href="Scripts/CSS/DaMoi.css" />
    <link rel="stylesheet" href="Scripts/CSS/Chitietsanpham.css" />
    <style type="text/css">
        .auto-style2 {
            width: 206px;
            text-align: center;
        }
        .auto-style12 {
            width: 206px;
            height: 32px;
            text-align: center;
        }
        .auto-style13 {
            height: 18px;
            font-size: medium;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="viewsanpham">
        <tr>
            <td class="otensanpham">
                <asp:Label ID="lblTheLoai" runat="server" CssClass="cttensanpham"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataList ID="ddlSanpham" runat="server" CssClass="sanphamindex" RepeatColumns="4">
                    <ItemTemplate>
                        <table class="sanpham">
                            <tr>
                                <td>
                                    <asp:HyperLink ID="HyperLink2" runat="server" CssClass="tensanpham" ImageUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' NavigateUrl='<%# "~/Chitietda?MaSP="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>'></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="hinhminhhao">
                                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hinhminhhoa" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="dongia">Đơn Giá:
                                    <asp:Label ID="Label1" runat="server" CssClass="gia" style="color: #FF3300; font-weight: 700" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HyperLink ID="hyMuaNgay" runat="server" BorderStyle="None" CssClass="nutmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>MUANGAY</asp:HyperLink>
                                    <asp:HyperLink ID="hyThemVaoGio" runat="server" CssClass="nutthemvaogio" Height="30px" NavigateUrl='<%# "~/Sanphamtheochude.aspx?MaChuDe="+Eval("MACHUDE")+"&MaMuaHang="+Eval("MASANPHAM") %>' Width="130px">THÊM VÀO GIỎ</asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr>
            <td class="auto-style13">Có Thể Bạn Muốn Xem</td>
        </tr>
        <tr>
            <td style="text-align: center" class="ofooter"><asp:DataList ID="ddlSanPhamNgang" runat="server" RepeatColumns="5" CssClass="ddlspn" EnableTheming="True" RepeatDirection="Horizontal">
                    <ItemTemplate>
                        <table class="ospnviewsanpham">
                            <tr>
                                <td class="auto-style2">
                                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="spnhinhanh" ImageHeight="260px" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospntensanpham">
                                    <asp:HyperLink ID="HyperLink2" runat="server" ForeColor="Black" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>'></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospngiasanpham">
                                    <asp:Label ID="spnlblGia" runat="server" CssClass="spngia" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:HyperLink ID="hyMuaNgay" runat="server" BorderStyle="None" CssClass="spnmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MaSanPham") %>'>MUA NGAY</asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>

</asp:Content>

