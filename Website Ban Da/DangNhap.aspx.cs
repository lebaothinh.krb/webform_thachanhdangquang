﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;
public partial class DangNhap : System.Web.UI.Page
{
    KhachHangBusiness khbn = new KhachHangBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DataTable dt = khbn.LoadKhachHang();
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["TENDANGNHAP"].ToString()==txtTaiKhoan.Text && dr["MATKHAU"].ToString()==txtMatKhau.Text)
            {
                Response.Write("<script>alert('Đăng Nhập Thành Công!')</script>");
                Session["KHACHHANGDN"] = khbn.LoadMotKhachHang(dr["MAKHACHHANG"].ToString());
                if (Session["MUAHANG"]!= null)
                {
                    Session["MUAHANG"] = null;
                    Response.Redirect("~/ThanhToan.aspx");
                }
                else
                Response.Redirect("~/Default.aspx");
            }
        }
        lblBaoLoi.Text = "Sai tên đăng nhập hoặc mật khẩu!";
    }
}