﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GocTrai.ascx.cs" Inherits="UC_GocTrai" %>
<style>
    #ktdonhang1
    {
        position:fixed;
        left:0;
        top:300px;
        transition:all 1s 0.2s;
    }
    #ktdonhang2
    {
        position:fixed;
        left:40px;
        top:300px;
        transition:all 1s 1s ease-in;
        display:none;
    }
    #ktdonhang1:hover>#ktdonhang2
    {
        transition:all 1s 0.2s;
        z-index:999999;
        display:block;
    }
    #giohangfix
    {
        position:fixed;
        left:0;
        top:352px;
    }
    .chudem
    {
        position:fixed;
        left:18px;
        top:365px;
    }
</style>
<div id="tong">
    <div id ="ktdonhang1"><a href="../TinhTrangDonHang.aspx"><img src="../Image/ktdonhang1.png" /></a>
        <div id ="ktdonhang2"><img src="../Image/ktdonhang2.png" /></div>
    </div>
    
    <div id="giohangfix"><a href="../Giohang.aspx"><img src="../Image/giohangfix.png" /></a></div>
    <asp:Label CssClass="chudem" ID="lblDem" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
</div>