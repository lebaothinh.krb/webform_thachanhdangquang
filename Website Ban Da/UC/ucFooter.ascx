﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFooter.ascx.cs" Inherits="UC_ucFooter" %>
<link rel="stylesheet" type="text/css" href="../Scripts/CSS/Footer.css"/>
<footer>
    <div id="footer">
        <div id="fleft">
            <p class="p"><strong>ĐỊA CHỈ: ĐƯỜNG NGUYỄN THỊ ĐỊNH, KHỐI 4, THỊ TRẤN KRÔNG KMAR, HUYỆN KRÔNG BÔNG, TỈNH DAKLAK</strong></p>
        <p class="p">Email:</p> 
            <p class="p1">ledangkhoa.krb@gmail.com</p>
             <p class="p1">lebaothinh.krb@gmail.com</p>
            <a href="https://www.facebook.com/ledangkhoa1983"><img id="f" src="../Image/036-facebook.png"/></a>
            <img id="l" src="../Image/005-whatsapp.png"/>
            <p class="p1" id="the1">01206222535</p>
            <p class="p1" id="the2">01654141413</p>
            <p class="p1" id="the3">01645398501</p>
        </div>
        <div id="fmid">
            <p style="font-weight: 700">
                PHƯƠNG THỨC MUA HÀNG
            </p>
            <p>
                1. MUA HÀNG TRỰC TIẾP TẠI CƠ SỞ
            </p>
            <p>
                Các bạn đến chọn & mua trực tiếp tại ĐỊA CHỈ BÊN
</p>
            <p>
                2.MUA HÀNG QUA ĐIỆN THOẠI
            </p>
            <p>
                Chỉ việc cầm máy và gọi cho chúng tôi , và bạn sẽ nhận được mọi sự tư vấn cần thiết.
</p>
            <p>
                3.MUA HÀNG TRỰC TUYẾN TRÊN WEBSITE
            </p>
            <p>
                Các bạn chọnCác bạn chọn hàng trên website và chúng tôi sẽ chuyển hàng tới tận nơi bạn yêu cầu.
            </p>
        </div>
        <div id="fright">
            <p style="font-weight: 700">ĐĂNG KÝ NHẬN TIN TỪ WEBSITE</p>
            <asp:TextBox ID="txtInputEmail" runat="server" Height="25px" TextMode="Email" Width="203px" AutoPostBack="True">example@gmail.com</asp:TextBox>
            <asp:Button ID="dangkyemail" runat="server" Text="Đăng Ký" Height="25px" OnClick="dangkyemail_Click" OnClientClick="return false;" CommandName="DangKyEmail" />
            <br />
            <br />
            <asp:Label ID="lblBaoLoi" runat="server" ForeColor="Black"></asp:Label>
        </div>
        <script type='text/javascript'>window._sbzq || function (e) { e._sbzq = []; var t = e._sbzq; t.push(["_setAccount", 73847]); var n = e.location.protocol == "https:" ? "https:" : "http:"; var r = document.createElement("script"); r.type = "text/javascript"; r.async = true; r.src = n + "//static.subiz.com/public/js/loader.js"; var i = document.getElementsByTagName("script")[0]; i.parentNode.insertBefore(r, i) }(window);</script>
    </div>
</footer>
