﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class UC_Chitietsanpham : System.Web.UI.UserControl
{
    SanPhamBusiness spbn = new SanPhamBusiness();
    LoaiBussiness lbn = new LoaiBussiness();
    TheLoaiBussiness tlbn = new TheLoaiBussiness();
    ChuDeBussiness cdbn = new ChuDeBussiness();
    HinhThucGiaoHang htgh = new HinhThucGiaoHang();
    HinhThucThanhToan httt = new HinhThucThanhToan();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = spbn.LoadMotSanPham(Request.QueryString["MaSanPham"]);
            lblTenSanPham.Text = dt.Rows[0]["TENSANPHAM"].ToString();
            imgHinhMinhHoa.ImageUrl = "~/Image/Da/" + dt.Rows[0]["HINHMINHHOA"].ToString();
            lblDonGia.Text = Convert.ToInt64(dt.Rows[0]["DONGIA"]).ToString();
            lblMoTa.Text = dt.Rows[0]["MOTA"].ToString();
            lblChuDe.Text = cdbn.TenChuDe(dt.Rows[0]["MACHUDE"].ToString());
            lblLoaiTheLoai.Text = tlbn.TenTheLoai(dt.Rows[0]["THELOAI"].ToString()) + " / " + lbn.TenLoai(dt.Rows[0]["LOAI"].ToString());
            ddlSanPhamNgang.DataSource = spbn.LoadTop5SoLanXem();
            ddlSanPhamNgang.DataBind();
            ddlGiaoHang.DataSource = htgh.LoadHinhThucGiaoHang();
            ddlGiaoHang.DataBind();
            ddlHinhThucTT.DataSource = httt.LoadHinhThucTT();
            ddlHinhThucTT.DataBind();
            Session["SoLuong"] = 1;
            txtSoLuong.Text = Session["SoLuong"].ToString();
        }
    }
    protected void btnTangSoLuong_Click(object sender, EventArgs e)
    {

    }
    protected void btnDatMua_Click(object sender, EventArgs e)
    {

    }
}