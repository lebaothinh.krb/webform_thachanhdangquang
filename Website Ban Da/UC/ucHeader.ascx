﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucHeader.ascx.cs" Inherits="UC_ucHeader" %>
<script language="javascript" type="text/javascript">
    function timkiem_onclick() {
        str = document.getElementById("chuoitim").value;
        window.location = "TimKiem.aspx?LenhTimKiem=" + str;
    }
</script>
<link rel="stylesheet" type="text/css" href="../Scripts/CSS/Header.css"/>
<style type="text/css">
    a
    {
        text-decoration:none;
        font-style:normal;
    }
    .auto-style1 {
        text-decoration: none;
        font-style:normal;
    }
    .tongsoluong
    {
    z-index:999;
	position:absolute;
	top:9px;
    left:1293px;
	height:30px;
    }
    </style>
<header>
      <div id="logovatimkiem">
        	<div id="logo">
            	<a href="../Default.aspx"><img src="../Image/logo.png"/></a>
            </div>
            <div id="giohang">
            	<a href="../Giohang.aspx"><img src="../Image/giohang.png"/></a>
                <asp:Label CssClass="tongsoluong" ID="lblTongSoLuong" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div id="slogan">
				<a href="../Default.aspx"><p>Thaïch AÛnh Ñaêng Quang</p></a>
            </div>
            <div id="logomang">
            	<a href="https://www.facebook.com/ledangkhoa1983"><img id="facebook" /></a>
                <a href="#"><img id="zalo" /></a>
                <a href="https://plus.google.com/103296011582797556482"><img id="email"/></a>
                <a href="../ViTri.aspx"><img id="vitri"/></a>
                <a href="https://www.youtube.com/channel/UC4_AoYFbKBbVxiFd2bf8Z1Q?view_as=subscriber"><img id="youtube"/></a>
          </div>
            <div id="call">
            	<img src="../Image/office-phone-icon--25.png"/>
                <div id="sdt">
                	<p>0165 414141 3</p>
                	<p>0120 622 2535</p>
                	<p>0164 539 8501</p>
                </div>
            </div>
            <div id="timkiem">
                <input class="txtTimKiem" type="text" size="36" name="tsearch" id="chuoitim" placeholder="Tìm kiếm ở đây!...">
                <input class="btnTimKiem" type="button" value="" title="Tìm sản phẩm" runat="server" id="Button1" onclick="return timkiem_onclick()">
            </div>
            <asp:HyperLink ID="hyDangNhap" runat="server" CssClass="nutdangnhap" ForeColor="#009900" NavigateUrl="~/DangNhap.aspx">Đăng Nhập</asp:HyperLink>
          <asp:HyperLink ID="hyChao" runat="server" CssClass="nutchao" ForeColor="#009900" NavigateUrl="~/SuaThongTinCaNhan.aspx">Chào</asp:HyperLink>
            <asp:HyperLink ID="hyDangKy" runat="server" CssClass="nutdangky" ForeColor="#009900" NavigateUrl="~/DangKy.aspx">Đăng Ký</asp:HyperLink>
           <asp:LinkButton ID="hyDangXuat" runat="server" CssClass="nutdangky" ForeColor="#009900" NavigateUrl="~/DangKy.aspx" OnClick="hyDangXuat_Click">Đăng Xuất</asp:LinkButton>
        </div>
        <div id="mainmenu">
        <div id="menu">
        	<ul>
            	<li><a href="../Default.aspx" class="auto-style1">Trang Chủ</a></li>
                <li><a href="../Sanpham.aspx" class="auto-style1">Sản Phẩm
                    <ul class="submenu">
                    	<li>Theo Thể Loại
                        	<ul class="submenu2">
                                <li><a href="../Sanphamtheotheloai.aspx?MaTheLoai=1" class="auto-style1">Thạch Ảnh</a></li>
                            	<li><a href="../Sanphamtheotheloai.aspx?MaTheLoai=4" class="auto-style1">Mộc Ảnh</a></li>
                                <li><a href="../Sanphamtheotheloai.aspx?MaTheLoai=3" class="auto-style1">Diệp Ảnh</a></li>
                                <li><a href="../Sanphamtheotheloai.aspx?MaTheLoai=2" class="auto-style1">Vỏ ỐC</a></li>
                            </ul>
                        </li> 
                    </ul>
                	<ul class="submenu">
                    	<li><a href="../Sanpham.aspx">Theo Chủ Đề
                        	<ul class="submenu2">
                            	<li><a href="../Sanphamtheochude.aspx?MaChuDe=7" class ="auto-style1">Công Giáo</a></li>
                                <li><a href="../Sanphamtheochude.aspx?MaChuDe=8" class ="auto-style1">Phật Giáo</a></li>
                                <li><a href="../Sanphamtheochude.aspx?MaChuDe=2" class="auto-style1">Nghệ Thuật</a></li>
                                <li><a href="../Sanphamtheochude.aspx?MaChuDe=4" class="auto-style1">Chủ Tịch Hồ Chí Minh</a></li>
                                <li><a href="../Sanphamtheochude.aspx?MaChuDe=5" class="auto-style1">Thú Vật</a></li>
                                <li><a href="../Sanphamtheochude.aspx?MaChuDe=6" class="auto-style1">Tây Nguyên</a></li>
                                <li><a href="../Sanphamtheochude.aspx?MaChuDe=9" class ="auto-style1">Thư Pháp</a></li>
                            </ul>
                        </a></li> 
                    </ul>
                </a></li>
                <li><a href="#" class="auto-style1">Giới Thiệu
                	<ul class="submenu">
                    	<li><a href="../ThongTinSanPham.aspx" class="auto-style1">Thông Tin Sản Phẩm</a></li>
                    	<li><a href="../CoSoSanXuat.aspx" class="auto-style1">Cơ Sở Sản Xuất</a></li>
                    </ul>
                </a></li>
                <li><a href="../GocTuVan.aspx" class="auto-style1">Góc Tư Vấn</a></li>
                <li><a href="../DatHangTheoYeuCau.aspx" class="auto-style1">Đặt Hàng Theo Yêu Cầu</a></li>
            </ul>
        </div>
        </div>
    </header>
