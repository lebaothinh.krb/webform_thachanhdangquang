﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Chitietsanpham.ascx.cs" Inherits="UC_Chitietsanpham" %>
<link rel="stylesheet" href="../Scripts/CSS/Chitietsanpham.css" />

<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=175466316528342';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <table class="viewsanpham">
        <tr>
            <td class="otensanpham" colspan="3">
                <asp:Label ID="lblTenSanPham" runat="server" ForeColor="White" CssClass="cttensanpham"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="ochude" colspan="3">
                <strong>&nbsp;&nbsp; Chủ Đề:</strong>
                <asp:Label ID="lblChuDe" runat="server" CssClass="auto-style1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="otheloai" colspan="3">
                <strong>&nbsp;&nbsp; Loại, Thể Loại:</strong>
                <asp:Label ID="lblLoaiTheLoai" runat="server" CssClass="auto-style1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="ohinhminhhoa" rowspan="2">
                <asp:Image ID="imgHinhMinhHoa" runat="server" Height="300px" Width="240px" CssClass="cthinhminhhoa" />
            </td>
            <td class="othongtinct">&nbsp;&nbsp; Thông Tin Chi Tiết:</td>
            <td class="otuychongiaohang" rowspan="5">
                <br />
                <br />
                <br />
                <asp:DataList ID="ddlGiaoHang" runat="server" RepeatColumns="1" style="text-align: left" CssClass="giaohang">
                    <ItemTemplate>
                        <table class="auto-style4">
                            <tr>
                                <td>
                                    <asp:Image ID="Image2" runat="server" ImageUrl='<%# "~/Image/"+Eval("HINHANH") %>' />
                                </td>
                                <td class="ghtengh">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("TENHINHTHUCGH")+": "+Eval("THOIGIANDUKIEN")+" ngày sau." %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("GIATIENGIAO")+"/"+Eval("DONVITINH") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("MOTAHINHTHUCGH") %>'></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <br />
                <asp:DataList ID="ddlHinhThucTT" runat="server" RepeatColumns="3">
                    <ItemTemplate>
                        <table class="ohinhthuctt">
                            <tr>
                                <td class="auto-style11">
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("TENHINHTHUCTT") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style10">
                                    <asp:Image ID="Image3" runat="server" CssClass="htttimg" ImageUrl='<%# "~/Image/"+Eval("HINHANH") %>' />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr>
            <td class="omota" aria-multiline="True">
                <asp:Label ID="lblMoTa" runat="server" style="text-align: justify"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style13">
                <div class="fb-like" data-href="http://localhost:1170/Chitietsanpham.aspx" data-width="300" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </td>
            <td class="odongia">
                <asp:Label ID="lblDonGia" runat="server" CssClass="ctdongia"></asp:Label>
                <asp:Label ID="Label1" runat="server" CssClass="ctdongia" Text="VNĐ"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style12"></td>
            <td class="auto-style12">
                <asp:Button ID="btnGiamSoLuong" runat="server" Text="-" Width="28px" />
                <asp:TextBox ID="txtSoLuong" runat="server" Width="34px"></asp:TextBox>
                <asp:Button ID="btnTangSoLuong" runat="server" Text="+" OnClick="btnTangSoLuong_Click" Width="28px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="odatmua">
                <asp:Button ID="btnDatMua" runat="server" Text="Đặt Mua" BorderStyle="None" CssClass="ctdatmua" OnClick="btnDatMua_Click" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="osanphamngang" colspan="3">
                <asp:DataList ID="ddlSanPhamNgang" runat="server" RepeatColumns="5">
                    <ItemTemplate>
                        <table class="ospnviewsanpham">
                            <tr>
                                <td class="ospnhinhanh">
                                    <asp:Image ID="spnimg" runat="server" CssClass="spnhinhanh" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td class="ospntensanpham">
                                    <asp:Label ID="spnlblTenSanPham" runat="server" CssClass="spntensanpham" Text='<%# Eval("TENSANPHAM") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospngiasanpham">
                                    <asp:Label ID="spnlblGia" runat="server" CssClass="spngia" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospnmuangay">
                                    <asp:Button ID="spnmuangay" runat="server" CssClass="spnmuangay" Text="MUA NGAY" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr>
            <td class="auto-style3" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="ocoment" colspan="3">
                <div class="fb-comments" data-href="test.aspx?MaSanPham" data-numposts="5" data-width="1100">
                </div>
            </td>
        </tr>
    </table>