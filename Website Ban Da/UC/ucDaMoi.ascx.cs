﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Public;
using BusinessLogic;
public partial class UC_ucSachMoi : System.Web.UI.UserControl
{
    SanPhamBusiness spbn = new SanPhamBusiness();
    GioHang ghbn = new GioHang();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["MaMuaHang"] != null)
            {
                ghbn.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MaMuaHang"].ToString())); 
            }
            SanPhamBusiness sp = new SanPhamBusiness();
            dlSanPhamCongGiao.DataSource = sp.LoadTop8ChuDe("7");
            dlSanPhamCongGiao.DataBind();
            dlSanPhamPhatGiao.DataSource = sp.LoadTop8ChuDe("8");
            dlSanPhamPhatGiao.DataBind();
            dlSanPhamThuVat.DataSource = sp.LoadTop8ChuDe("5");
            dlSanPhamThuVat.DataBind();
            dlSanPhamNgheThuat.DataSource = sp.LoadTop8ChuDe("2");
            dlSanPhamNgheThuat.DataBind();
            dlSanPhamThuPhap.DataSource = sp.LoadTop8ChuDe("9");
            dlSanPhamThuPhap.DataBind();
        }
    }
    protected void btnThemVaoGio_Click(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["MASANPHAM"] != null)
            {
                ghbn.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MASANPHAM"].ToString()));
            }
        }
    }
}