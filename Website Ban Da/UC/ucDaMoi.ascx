﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDaMoi.ascx.cs" Inherits="UC_ucSachMoi" %>
 <link rel="stylesheet" type="text/css" href="../Scripts/CSS/DaMoi.css"/>
<style type="text/css">
    a
    {
        text-decoration:none;
    }
    .auto-style2 {
        height: 70px;
    }
    .auto-style3 {
        height: 23px;
    }
    .auto-style4 {
        border-bottom: 1px solid #DDDDDD;
        border-top: 1px solid #DDDDDD;
        height: 29px;
    }
   </style>
<table class="viewsanpham">
    <tr>
        <td class="auto-style3">
            &nbsp<br />
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Sanphamtheochude.aspx?MaChuDe=7" CssClass="tieudeindex" style="color: #000000">In Thạch Ảnh Chủ Đề Công Giáo</asp:HyperLink>
            </td>
    </tr>
    <tr>
        <td class="osanphamindex">
<asp:DataList ID="dlSanPhamCongGiao" runat="server" RepeatColumns="4" CssClass="sanphamindex">
            <ItemTemplate>
                <table class="sanpham">
                    <tr>
                        <td class="auto-style2">
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="tensanpham" NavigateUrl='<%# "~/Chitietda?MaSP="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>' ImageUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hinhminhhoa" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="dongia">Đơn Giá:
                            <asp:Label ID="Label1" runat="server" style="color: #FF3300; font-weight: 700" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>' CssClass="gia"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="hyMuaNgay" runat="server" CssClass="nutmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Height="30px" Width="90px" Font-Overline="False">MUA NGAY</asp:HyperLink>
                            <asp:HyperLink ID="hyThemVaoGio" runat="server" CssClass="nutthemvaogio" Height="30px" NavigateUrl='<%# "~/Default.aspx?MaMuaHang="+Eval("MASANPHAM") %>' Width="130px">THÊM VÀO GIỎ</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList></td>
    </tr>
    <tr>
        <td>
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Sanphamtheochude.aspx?MaChuDe=8" CssClass="tieudeindex" style="color: #000000">In Thạch Ảnh Chủ Đề Phật Giáo</asp:HyperLink>
            </td>
    </tr>
    <tr>
        <td class="osanphamindex">
<asp:DataList ID="dlSanPhamPhatGiao" runat="server" RepeatColumns="4" CssClass="sanphamindex">
            <ItemTemplate>
                <table class="sanpham">
                    <tr>
                        <td class="otensanpham">
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="tensanpham" NavigateUrl='<%# "~/Chitietda?MaSP="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="hinhminhhao">
                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hinhminhhoa" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="dongia">Đơn Giá:
                            <asp:Label ID="Label1" runat="server" style="color: #FF3300; font-weight: 700" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>' CssClass="gia"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink7" runat="server" CssClass="nutmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Height="30px" Width="90px">MUA NGAY</asp:HyperLink>
                            <asp:HyperLink ID="hyThemVaoGio" runat="server" CssClass="nutthemvaogio" Height="30px" NavigateUrl='<%# "~/Default.aspx?MaMuaHang="+Eval("MASANPHAM") %>' Width="130px">THÊM VÀO GIỎ</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList></td>
    </tr>
    <tr>
        <td>
            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Sanphamtheochude.aspx?MaChuDe=5" CssClass="tieudeindex" style="color: #000000">In Thạch Ảnh Chủ Đề Thú Vật</asp:HyperLink>
            </td>
    </tr>
    <tr>
        <td class="osanphamindex">
<asp:DataList ID="dlSanPhamThuVat" runat="server" RepeatColumns="4" CssClass="sanphamindex">
            <ItemTemplate>
                <table class="sanpham">
                    <tr>
                        <td class="auto-style2">
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="tensanpham" NavigateUrl='<%# "~/Chitietda?MaSP="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="hinhminhhao">
                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hinhminhhoa" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="dongia">Đơn Giá:
                            <asp:Label ID="Label1" runat="server" style="color: #FF3300; font-weight: 700" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>' CssClass="gia"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink7" runat="server" CssClass="nutmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Height="30px" Width="90px">MUA NGAY</asp:HyperLink>
                            <asp:HyperLink ID="hyThemVaoGio" runat="server" CssClass="nutthemvaogio" Height="30px" NavigateUrl='<%# "~/Sanphamtheochude.aspx?MaChuDe="+Eval("MACHUDE")+"&MaMuaHang="+Eval("MASANPHAM") %>' Width="130px">THÊM VÀO GIỎ</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList></td>
    </tr>
    <tr>
        <td>
            <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Sanphamtheochude.aspx?MaChuDe=2" CssClass="tieudeindex" style="color: #000000">In Thạch Ảnh Chủ Đề Nghệ Thuật</asp:HyperLink>
            </td>
    </tr>
    <tr>
        <td class="osanphamindex">
<asp:DataList ID="dlSanPhamNgheThuat" runat="server" RepeatColumns="4" CssClass="sanphamindex">
            <ItemTemplate>
                <table class="sanpham">
                    <tr>
                        <td class="otensanpham">
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="tensanpham" NavigateUrl='<%# "~/Chitietda?MaSP="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="hinhminhhao">
                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hinhminhhoa" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="dongia">Đơn Giá:
                            <asp:Label ID="Label1" runat="server" style="color: #FF3300; font-weight: 700" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>' CssClass="gia"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink7" runat="server" CssClass="nutmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Height="30px" Width="90px">MUA NGAY</asp:HyperLink>
                            <asp:HyperLink ID="hyThemVaoGio" runat="server" CssClass="nutthemvaogio" Height="30px" NavigateUrl='<%# "~/Sanphamtheochude.aspx?MaChuDe="+Eval("MACHUDE")+"&MaMuaHang="+Eval("MASANPHAM") %>' Width="130px">THÊM VÀO GIỎ</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList></td>
    </tr>
    <tr>
        <td>
            <asp:HyperLink ID="hyThuPhap" runat="server" NavigateUrl="~/Sanphamtheochude.aspx?MaChuDe=9" CssClass="tieudeindex" style="color: #000000">In Thạch Ảnh Chủ Đề Thư Pháp</asp:HyperLink>
            </td>
    </tr>
    <tr>
        <td class="osanphamindex">
<asp:DataList ID="dlSanPhamThuPhap" runat="server" RepeatColumns="4" CssClass="sanphamindex">
            <ItemTemplate>
                <table class="sanpham">
                    <tr>
                        <td class="otensanpham">
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="tensanpham" NavigateUrl='<%# "~/Chitietda?MaSP="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="hinhminhhao">
                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hinhminhhoa" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="dongia">Đơn Giá:
                            <asp:Label ID="Label1" runat="server" style="color: #FF3300; font-weight: 700" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>' CssClass="gia"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink7" runat="server" CssClass="nutmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Height="30px" Width="90px">MUA NGAY</asp:HyperLink>
                            <asp:HyperLink ID="hyThemVaoGio" runat="server" CssClass="nutthemvaogio" Height="30px" NavigateUrl='<%# "~/Sanphamtheochude.aspx?MaChuDe="+Eval("MACHUDE")+"&MaMuaHang="+Eval("MASANPHAM") %>' Width="130px">THÊM VÀO GIỎ</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList></td>
    </tr>
</table>
