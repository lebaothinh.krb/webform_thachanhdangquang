﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ThuMucAdmin_UC_ucMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ADMINDN"] != null)
        {
            lblChao.Text = "Chào " + Session["ADMINDN"].ToString();
        }
    }
    protected void lbtnDangXuat_Click(object sender, EventArgs e)
    {
        Session["QuyenHanAdmin"] = null;
        Session["ADMINDN"] = null;
        Response.Redirect("~/Default.aspx");
    }
}