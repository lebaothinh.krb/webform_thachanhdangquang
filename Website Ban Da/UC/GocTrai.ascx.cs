﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class UC_GocTrai : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["GIOHANG"]!=null)
        {
            DataTable dt = (DataTable)Session["GIOHANG"];
            int dem = 0;
            foreach (DataRow dr in dt.Rows)
            {
                dem += Convert.ToInt32(dr["SOLUONG"].ToString());
            }
            lblDem.Text = dem.ToString();
        }
        else
        {
            lblDem.Text = "0";
        }
    }
}