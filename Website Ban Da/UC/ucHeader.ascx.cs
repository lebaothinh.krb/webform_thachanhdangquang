﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class UC_ucHeader : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["GIOHANG"] != null)
            {
                DataTable dt = (DataTable)Session["GIOHANG"];
                int dem = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    dem++;
                }
                lblTongSoLuong.Text = dem.ToString();
            }
            if (Session["KHACHHANGDN"] != null)
            {
                hyDangNhap.Visible = false;
                hyDangKy.Visible = false;
                DataTable dt = (DataTable)Session["KHACHHANGDN"];
                hyChao.Text = "Chào " + dt.Rows[0]["TENKHACHHANG"].ToString();
                hyChao.Visible = true;
                hyDangXuat.Visible = true;
            }
            else
            {
                hyDangNhap.Visible = true;
                hyDangKy.Visible = true;
                hyChao.Visible = false;
                hyDangXuat.Visible = false;
            }
        }
    }
    protected void hyDangXuat_Click(object sender, EventArgs e)
    {
        Session["KHACHHANGDN"] = null;
        Response.Redirect("~/Default.aspx");
    }
}