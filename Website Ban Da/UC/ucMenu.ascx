﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucMenu.ascx.cs" Inherits="ThuMucAdmin_UC_ucMenu" %>
<link rel="stylesheet" href="../../Scripts/CSS/MenuHeader.css" />
<style>
    .btndangxuat
    {
        float:right;
        margin-right:10px;
    }
</style>
<div id="menu">
    <ul>
      <li><a href="../ThuMucAdmin/QuanTriHeThong.aspx">Quản Trị Hệ Thống</a></li>
      <li><a href="#">Quản Trị Khách Hàng</a>
        <ul class="submenu">
          <li><a href="../ThuMucAdmin/QuanLyKhachHang.aspx">Khách Hàng</a></li>
          <li><a href="../ThuMucAdmin/QuanLyDonDatHang.aspx">Đơn Đặt Hàng</a></li>
          <li><a href="../ThuMucAdmin/QuanLyDatHangTheoYeuCau.aspx">Đặt Hàng Theo Yêu Cầu</a></li>
        </ul>
      </li>
      <li><a href="#">Quản Trị Danh Mục</a>
        <ul class="submenu">
          <li><a href="../ThuMucAdmin/QuanLyChuDeLoaiTheLoai.aspx">Loại, Thể Loại, Chủ Đề</a></li>
          <li><a href="../ThuMucAdmin/QuanTriDanhMucSanPham.aspx">Sản Phẩm</a></li>
        </ul>
      </li>
      <li><a href="../ThuMucAdmin/QuanLyTaiChinh.aspx">Quản Lý Tài Chính</a></li>
      <li><a href="../ThuMucAdmin/ChatOnline.aspx">Chat Online</a></li>
  </ul>
    <asp:Label ID="lblChao" CssClass="btndangxuat" runat="server" Text="Label"></asp:Label>
    <asp:LinkButton ID="lbtnDangXuat" runat="server" CssClass="btndangxuat" style="font-style: italic; color: #000000; font-weight: 700" OnClick="lbtnDangXuat_Click">Đăng Xuất</asp:LinkButton>
 </div>