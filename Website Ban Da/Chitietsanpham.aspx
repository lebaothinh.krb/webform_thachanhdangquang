﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="Chitietsanpham.aspx.cs" Inherits="Chitietsanpham" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <link rel="stylesheet" href="Scripts/CSS/Chitietsanpham.css" type="text/css" />
    <style type="text/css">
        a
        {
            text-decoration:none;
            font-style:normal;
        }
        .auto-style1 {
            font-style: italic;
        }
        .auto-style3 {
            text-align:center;
            height: 23px;
        }
        .auto-style4 {
            margin-left:10px;
            width: 100%;
        }
        .auto-style10 {
            height: 18px;
            width: 49px;
        }
        .auto-style11 {
            width: 49px;
        }
        .auto-style12 {
            height: 28px;
        }
        .auto-style13 {
            text-align: center;
            height: 30px;
        }
    .auto-style14 {
        max-height: 260px;
        max-width: 206px;
        width: 206px;
            text-align: center;
        }
    </style>
    <div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=175466316528342';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <table class="viewsanpham">
        <tr>
            <td class="otensanpham" colspan="3">
                <asp:Label ID="lblTenSanPham" runat="server" ForeColor="White" CssClass="cttensanpham"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="ochude" colspan="3">
                <strong>&nbsp;&nbsp; Chủ Đề:</strong>
                <asp:Label ID="lblChuDe" runat="server" CssClass="auto-style1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="otheloai" colspan="3">
                <strong>&nbsp;&nbsp; Loại, Thể Loại:</strong>
                <asp:Label ID="lblLoaiTheLoai" runat="server" CssClass="auto-style1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="ohinhminhhoa" rowspan="2">
                <asp:Image ID="imgHinhMinhHoa" runat="server" Height="300px" Width="240px" CssClass="cthinhminhhoa" />
            </td>
            <td class="othongtinct">&nbsp;&nbsp; Thông Tin Chi Tiết:</td>
            <td class="otuychongiaohang" rowspan="5">
                <br />
                <br />
                <br />
                <asp:DataList ID="ddlGiaoHang" runat="server" RepeatColumns="1" style="text-align: left">
                    <ItemTemplate>
                        <table class="auto-style4">
                            <tr>
                                <td>
                                    <asp:Image ID="Image2" runat="server" ImageUrl='<%# "~/Image/"+Eval("HINHANH") %>' />
                                </td>
                                <td class="ghtengh">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("TENHINHTHUCGH")+": "+Eval("THOIGIANDUKIEN")+" ngày sau." %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("GIATIENGIAO","{0:#,##0}")+"/"+Eval("DONVITINH") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("MOTAHINHTHUCGH") %>'></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <br />
                <br />
                <br />
                <br />
                <asp:DataList ID="ddlHinhThucTT" runat="server" RepeatColumns="3">
                    <ItemTemplate>
                        <table class="ohinhthuctt">
                            <tr>
                                <td class="auto-style11">
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("TENHINHTHUCTT") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style10">
                                    <asp:Image ID="Image3" runat="server" CssClass="htttimg" ImageUrl='<%# "~/Image/"+Eval("HINHANH") %>' />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr>
            <td class="omota" aria-multiline="True">
                <asp:Label ID="lblMoTa" runat="server" style="text-align: justify"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style13">
                <div class="fb-like" data-href="http://localhost:1170/Chitietsanpham.aspx" data-width="300" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </td>
            <td class="odongia">
                &nbsp;&nbsp;
                <asp:Label ID="lblDonGia" runat="server" CssClass="ctdongia"></asp:Label>
                <asp:Label ID="Label1" runat="server" CssClass="ctdongia" Text="VNĐ"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style12"></td>
            <td class="auto-style12">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="odatmua">
                <asp:Button ID="btnDatMua" runat="server" Text="Đặt Mua" BorderStyle="Solid" CssClass="ctdatmua" OnClick="btnDatMua_Click" />
                <br />
                <br />
                <asp:Button ID="Button1" runat="server" BorderStyle="Solid" CssClass="ctthemvaogio" OnClick="Button1_Click" Text="THÊM VÀO GIỎ" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3" colspan="3"></td>
        </tr>
        <tr>
            <td class="osanphamngang" colspan="3">
                <p><b>Top 5 Sản Phẩm Được Xem Nhiều Nhất</b></p><asp:DataList ID="ddlSanPhamNgang" runat="server" RepeatColumns="5" CssClass="ddlspn" EnableTheming="True" RepeatDirection="Horizontal">
                    <ItemTemplate>
                        <table class="ospnviewsanpham">
                            <tr>
                                <td class="auto-style14">
                                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="spnhinhanh" ImageHeight="260px" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospntensanpham">
                                    <asp:HyperLink ID="HyperLink2" runat="server" ForeColor="Black" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>'></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospngiasanpham">
                                    <asp:Label ID="spnlblGia" runat="server" CssClass="spngia" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospnmuangay">
                                    <asp:HyperLink ID="HyperLink3" runat="server" CssClass="spnmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MASANPHAM="+Eval("MASANPHAM") %>'>MUA NGAY</asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr>
            <td class="auto-style3" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="ocoment" colspan="3">
                <div class="fb-comments" data-href="test.aspx?MaSanPham" data-numposts="5" data-width="1100">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

