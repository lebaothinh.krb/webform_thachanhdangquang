﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class TimKiem : System.Web.UI.Page
{
    SanPhamBusiness spbn = new SanPhamBusiness();
    GioHang ghbn = new GioHang();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["MUAHANG"] = null;
            if (Request.QueryString["MaMuaHang"] != null) ghbn.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MaMuaHang"].ToString()));
            string Lenh = Request.QueryString["LenhTimKiem"];
            lblKetQua.Text = "Kết Quả Tìm Kiếm Cho: " + "'" + Lenh + "'";
            dlSanPham.DataSource = spbn.TimKiem(Lenh);
            dlSanPham.DataBind();
        }
    }
}