﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="ThanhToanKhongDangNhap.aspx.cs" Inherits="ThanhToan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <style type="text/css">
        a
        {
            text-decoration:none;
        }
        .viewsanpham
        {
            margin: 20px auto;
            color:black;
        }
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 17px;
            text-align: left;
            font-weight: bold;
        }
        .nut
        {
            background:#206AB5;
            width:100px;
            height:30px;
            color:white;
            font-weight:bold;
        }
        .auto-style3 {
            height: 21px;
            color: red;
            font-size: large;
        }
        .auto-style4 {
            height: 16px;
            text-align: left;
            font-weight: bold;
        }
        .auto-style7 {
            height: 20px;
            text-align: left;
        }
        .auto-style8 {
            height: 26px;
            text-align: left;
        }
        .auto-style10 {
            height: 200px;
            text-align: left;
        }
        .auto-style11 {
            text-align: left;
        }
        .auto-style12 {
            text-align: left;
            height: 18px;
        }
        .auto-style13 {
            width: 597px;
        }
        .textbox
        {
            border-radius:5px;
        }
    </style>
    <table class="viewsanpham">
        <tr>
            <td style="font-weight: 700; text-align: center;" class="auto-style3">THÔNG TIN ĐƠN ĐẶT HÀNG</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">THÔNG TIN GIỎ HÀNG</td>
        </tr>
        <tr>
            <td>
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style13">
    <asp:GridView ID="gvGiohangcuoi" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="MASANPHAM" GridLines="Horizontal" Width="745px">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <Columns>
            <asp:BoundField DataField="MASANPHAM" HeaderText="Mã Sản Phẩm" />
            <asp:BoundField DataField="TENSANPHAM" HeaderText="Tên Sản Phẩm" />
            <asp:BoundField DataField="DONGIA" HeaderText="Đơn Giá" />
            <asp:TemplateField HeaderText="Số Lượng">
                <ItemTemplate>
                    <asp:TextBox ID="txtSoLuong" runat="server" Text='<%# Eval("SoLuong") %>' Enabled="False"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ThanhTien" HeaderText="Thành Tiền" />
        </Columns>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style13">TỔNG TIỀN: <asp:Label ID="lblTongThanhTien" runat="server"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">THÔNG TIN GIAO HÀNG</td>
        </tr>
        <tr>
            <td>
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style10">NGÀY NHẬN HÀNG:</td>
                        <td class="auto-style10">
                            <asp:Calendar ID="Calendar1" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" ShowGridLines="True" Width="300px" Enabled="False">
                                <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                                <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                                <OtherMonthDayStyle ForeColor="#CC9966" />
                                <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                                <SelectorStyle BackColor="#FFCC66" />
                                <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                            </asp:Calendar>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style7">TÊN NGƯỜI NHẬN:</td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txtTenNguoiNhan" runat="server" CssClass="textbox" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style11">ĐỊA CHỈ (ghi chi tiết):</td>
                        <td class="auto-style11">
                            <asp:TextBox ID="txtDiaChi" runat="server" CssClass="textbox" Height="43px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style12">ĐIỆN THOẠI NGƯỜI NHẬN:</td>
                        <td class="auto-style12">
                            <asp:TextBox ID="txtDienThoai" runat="server" CssClass="textbox" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style12">EMAIL NGƯỜI NHẬN</td>
                        <td class="auto-style12">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox" Width="300px" TextMode="Email"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style12">GHI CHÚ ĐƠN HÀNG:</td>
                        <td class="auto-style12">
                            <asp:TextBox ID="txtGhiChu" runat="server" Height="38px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style8">HÌNH THỨC THANH TOÁN:</td>
                        <td class="auto-style8">
                            <asp:RadioButtonList ID="rdlThanhToan" runat="server" AutoPostBack="True" Width="300px">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style11">HÌNH THỨC GIAO HÀNG:</td>
                        <td class="auto-style11">
                            <asp:RadioButtonList ID="rdlGiaoHang" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="rdlGiaoHang_SelectedIndexChanged">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <br />
                <asp:Button ID="btnDongY" runat="server" CssClass="nut" Text="Đồng Ý" OnClick="btnDongY_Click" />
                <asp:ImageButton ID="ibtnNganLuong" runat="server" OnClick="ibtnNganLuong_Click" ImageUrl="https://www.nganluong.vn/css/newhome/img/button/pay-sm.png" />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

