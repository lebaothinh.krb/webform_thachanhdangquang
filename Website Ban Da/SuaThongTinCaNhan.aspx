﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="SuaThongTinCaNhan.aspx.cs" Inherits="SuaThongTinCaNhan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewsanpham {
            margin:20px auto;
            height:300px;
        }
        .nutcapnhat
        {
            background:#ff6a00;
            height:30px;
            width:100px;
            color:white;
        }
        .auto-style3 {
            color: #000000;
            width: 104px;
        }
        .auto-style6 {
            font-size: large;
        }
        .auto-style7 {
            color: #0066FF;
            text-align: center;
        }
        .auto-style8 {
            width: 104px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="viewsanpham">
        <tr>
            <td class="auto-style7" colspan="2"><strong class="auto-style6" style="text-align: center">CẬP NHẬT HỒ SƠ</strong></td>
        </tr>
        <tr>
            <td style="color: #000000" class="auto-style8">&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="color: #000000" class="auto-style8">Tên Khách Hàng</td>
            <td>
                <asp:TextBox ID="txtTenKhachHang" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Điện Thoại</td>
            <td>
                <asp:TextBox ID="txtDienThoai" runat="server" TextMode="Phone" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Ngày Sinh</td>
            <td style="color: #000000">
                Ngày
                <asp:TextBox ID="txtNgay" runat="server" Width="40px"></asp:TextBox>
&nbsp; Tháng&nbsp;
                <asp:TextBox ID="txtThang" runat="server" Width="40px"></asp:TextBox>
&nbsp; Năm&nbsp;
                <asp:TextBox ID="txtNam" runat="server" Width="75px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Giới Tính</td>
            <td>
                <asp:DropDownList ID="ddlGioiTinh" runat="server" Width="300px">
                    <asp:ListItem Value="True">Nam</asp:ListItem>
                    <asp:ListItem Value="False">Nữ</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Địa Chỉ</td>
            <td>
                <asp:TextBox ID="txtDiaChi" runat="server" TextMode="MultiLine" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Email</td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" TextMode="Email" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnCapNhat" runat="server" Text="Cập Nhật" CssClass="nutcapnhat" OnClick="btnCapNhat_Click" BorderStyle="None" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/DoiMatKhau.aspx">Đổi Mật Khẩu</asp:HyperLink>
            </td>
        </tr>
    </table>

</asp:Content>

