﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class Sanphamtheotheloai : System.Web.UI.Page
{
    SanPhamBusiness spbn = new SanPhamBusiness();
    TheLoaiBussiness tlbn = new TheLoaiBussiness();
    GioHang ghbn = new GioHang();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["MUAHANG"] = null;
            if (Request.QueryString["MaMuaHang"] != null) ghbn.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MaMuaHang"].ToString()));

            lblTheLoai.Text = tlbn.TenTheLoai(Request.QueryString["MaTheLoai"].ToString());
            ddlSanpham.DataSource = spbn.LoadSanPhamTheLoai(Request.QueryString["MaTheLoai"].ToString());
            ddlSanpham.DataBind();

            ddlSanPhamNgang.DataSource = spbn.LoadTop5SoLanXem();
            ddlSanPhamNgang.DataBind();
        }
    }
    protected void btnThemVaoGio_Click(object sender, EventArgs e)
    {
        ghbn.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MaSanPham"].ToString()));
    }
}