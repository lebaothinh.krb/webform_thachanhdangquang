﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="Xacnhandonhang.aspx.cs" Inherits="Xacnhandonhang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        a
        {
            text-decoration:none;
        }
        .viewsanpham {
            margin:20px auto;
            color:black;
        }
        .viewsanpham p
        {
            margin:5px 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table class="viewsanpham">
        <tr>
            <td class="auto-style3"><h3 style="text-align: left"><span class="auto-style3">Kính Chào Quý Khách!</h3>
    <p style="text-align: left">
        Thông tin đặt hàng của quý khách đã được chúng tôi ghi nhận. Vui lòng kiểm tra email của mình để kiểm tra đơn hàng. Chúng tôi sẽ liên lạc sớm nhất tới quý khách!</p>
    <p style="text-align: left">
        Công ty chúng tôi rất hân hạnh được phục vụ quý khách. Khi cần quý khách có thể liên hệ qua 0165 41 41 41 3</p>
    <p style="text-align: left">
        Xin cảm ơn!</span></p>
    <p style="text-align: right">
        <asp:HyperLink ID="hlVeTrangChu" runat="server" CssClass="auto-style3" NavigateUrl="~/Default.aspx" style="text-align: right; text-decoration: underline; font-weight: 700">Về Trang Chủ</asp:HyperLink>
    </p></td>
        </tr>
    </table>

</asp:Content>

