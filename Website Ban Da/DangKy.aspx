﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="DangKy.aspx.cs" Inherits="DangNhap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
            <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css" /> 
<script src="//code.jquery.com/jquery.js"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script> 
    <style type="text/css">
        a
        {
            text-decoration:none;
        }
        .auto-style2 {
            background: #F37021;
            height: 52px;
            font-weight: bold;
            font-size: medium;
            text-align: center;
            width: 385px;
        }
        .auto-style3 {
            background:#183544;
            padding: 5px 5px;
            width: 385px;
        }
        .dangnhapnhanhdn {
            text-align: center;
        }
        .auto-style5 {
            width: 420px;
        }
        .auto-style6 {
            background: #183544;
            padding: 5px 5px;
            width: 420px;
        }
        .auto-style9 {
            width: 100%;
            height:400px;
        }
        .auto-style10 {
            color: #FFFFFF;
            text-align: center;
        }
        .auto-style11 {
        height: 22px;
        text-align: left;
        width: 144px;
    }
        .auto-style12 {
        color: #FFFFFF;
        height: 22px;
        text-align: left;
        width: 144px;
    }
        .auto-style13 {
        text-align: left;
        width: 144px;
    }
    .auto-style14 {
        color: #FFFFFF;
        text-align: center;
        width: 144px;
    }
        .auto-style15 {
            width: 212px;
            text-align: left;
        }
        .auto-style16 {
            width: 212px;
        }
        .auto-style17 {
            color: #FFFFFF;
            text-align: center;
            width: 212px;
        }
        .baoloi
        {
            margin-left:100px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table class="viewsanphamdn">
        <tr>
            <td class="auto-style5">
                &nbsp;</td>
            <td class="auto-style2">
                <asp:Label ID="Label1" runat="server" CssClass="cttensanphamdn" Text="ĐĂNG KÝ&nbsp"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style3">
                <table class="auto-style9">
                    <tr>
                        <td class="auto-style12">Tên Khách Hàng</td>
                        <td class="auto-style15">
                            <asp:TextBox ID="txtTenKhachHang" runat="server" CssClass="txtdn" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTenKhachHang" ErrorMessage="Chưa Nhập Tên Khách Hàng!" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="auto-style10">
                        <td class="auto-style13">Điện Thoại</td>
                        <td class="auto-style15">
                            <asp:TextBox ID="txtDienThoai" runat="server" CssClass="txtdn" TextMode="Phone" MaxLength="11" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDienThoai" ErrorMessage="Chưa Nhập Số Điện Thoại" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="auto-style10">
                        <td class="auto-style13">Ngày Sinh</td>
                        <td style="text-align: left" class="auto-style16">
                            <asp:DropDownList ID="ddlNgay" runat="server" Width="60px">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                                <asp:ListItem>24</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>26</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>28</asp:ListItem>
                                <asp:ListItem>29</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                                <asp:ListItem>31</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlThang" runat="server" Width="60px">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtNam" runat="server" style="text-align: left" Width="72px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNam" ErrorMessage="Chưa Nhập Năm Sinh" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtNam" ErrorMessage="Năm Sinh Trong Khoảng 1900 - 2017" Font-Bold="True" ForeColor="Red" MaximumValue="2017" MinimumValue="1990" SetFocusOnError="True" Type="Integer">*</asp:RangeValidator>
                        </td>
                    </tr>
                    <tr class="auto-style10">
                        <td class="auto-style13">Giới Tính</td>
                        <td style="text-align: left" class="auto-style16">
                            <asp:RadioButton ID="rdNam" runat="server" Text="Nam" Checked="True" GroupName="gioitinh" />
&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rdNu" runat="server" Text="Nữ" GroupName="gioitinh" />
                        </td>
                    </tr>
                    <tr class="auto-style10">
                        <td class="auto-style11">Địa Chỉ</td>
                        <td class="auto-style15">
                            <asp:TextBox ID="txtDiaChi" runat="server" CssClass="txtdn" Height="51px" TextMode="MultiLine" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDiaChi" ErrorMessage="Chưa Nhập Địa Chỉ" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="auto-style10">
                        <td class="auto-style11">Email</td>
                        <td class="auto-style15">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="txtdn" TextMode="Email" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail" ErrorMessage="Chưa Nhập Email " ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email Không Hợp Lệ!" Font-Bold="True" ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr class="auto-style10">
                        <td class="auto-style13">&nbsp;</td>
                        <td class="auto-style16">&nbsp;</td>
                    </tr>
                    <tr class="auto-style10">
                        <td class="auto-style13">Tên Đăng Nhập:</td>
                        <td class="auto-style15">
                            <asp:TextBox ID="txtTenDangNhap" runat="server" CssClass="txtdn" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtTenDangNhap" ErrorMessage="Chưa Nhập Tên Đăng Nhập" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="auto-style10">
                        <td class="auto-style13">Mật Khẩu:</td>
                        <td class="auto-style15">
                            <asp:TextBox ID="txtMatKhau" runat="server" CssClass="txtdn" TextMode="Password" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtMatKhau" ErrorMessage="Chưa Nhập Mật Khẩu" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style12">Nhập Lại Mật Khẩu:</td>
                        <td class="auto-style15">
                            <asp:TextBox ID="txtNhapLaiMatKhau" runat="server" CssClass="txtdn" TextMode="Password" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtNhapLaiMatKhau" ErrorMessage="Chưa Nhập Nhập Lại Mật Khẩu" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtMatKhau" ControlToValidate="txtNhapLaiMatKhau" ErrorMessage="Mật Khẩu và Nhập Lại Mật Khẩu Phải Trùng Nhau" Font-Bold="True" ForeColor="Red" SetFocusOnError="True">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style14">&nbsp;</td>
                        <td class="auto-style17">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style10" colspan="2">
                            <asp:Button ID="btnDangKy" runat="server" CssClass="nut" Text="ĐĂNG KÝ" OnClick="btnDangKy_Click" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="baoloi" ForeColor="White" style="text-align: justify" />
            </td>
        </tr>
    </table>
    <link rel="stylesheet" href="Scripts/CSS/DangNhapDangKy.css" />

</asp:Content>

