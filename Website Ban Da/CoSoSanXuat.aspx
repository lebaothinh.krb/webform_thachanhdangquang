﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="CoSoSanXuat.aspx.cs" Inherits="CoSoSanXuat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewsanpham
        {
            color:black;
            margin:20px auto;
            text-align:center;
        }
        .otensanpham
        {
            background:#F37021;
            height:52px;
            font-weight: bold;
            font-size:medium;
        }
        .cttensanpham
        {
            margin-left:10px;
            font-weight:400;
            line-height:26px;
            font-size:24px;
            color: #FFFFFF;
            text-align: left;
        }
        .viewsanpham img
        {
            max-height:215px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="viewsanpham">
        <tr>
            <td class="otensanpham">
                <asp:Label ID="lblCoSoSanXuat" runat="server" Text="MỘT SỐ HÌNH ẢNH VỀ CƠ SỞ SẢN XUẤT" CssClass="cttensanpham"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table class="auto-style2">
                    <tr>
                        <td><img src="Image/cososanxuat/IMG_6146.jpg.1379783497056.jpg" />&nbsp;</td>
                        <td><img src="Image/cososanxuat/IMG_6172.jpg" />&nbsp;</td>
                    </tr>
                    <tr>
                        <td><img src="Image/cososanxuat/da-thong-ham-deo-ca.jpg" /></td>
                        <td><img src="Image/cososanxuat/IMG_6160.jpg" /> &nbsp;</td>
                    </tr>
                    <tr>
                        <td><img src="Image/cososanxuat/IMG_6178.jpg" /> &nbsp;</td>
                        <td><img src="Image/cososanxuat/IMG_6180.jpg" /></td>
                    </tr>
                    <tr>
                        <td><img src="Image/cososanxuat/IMG_6185.jpg" /></td>
                        <td><img src="Image/cososanxuat/in-thach-anh.jpg" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

