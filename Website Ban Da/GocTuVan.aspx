﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="GocTuVan.aspx.cs" Inherits="GocTuVan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewsanpham
        {
            color:black;
            margin:20px auto;
            text-align:center;
        }
        .otensanpham
        {
            background:#F37021;
            height:52px;
            font-weight: bold;
            font-size:medium;
        }
        .cttensanpham
        {
            margin-left:10px;
            font-weight:400;
            line-height:26px;
            font-size:24px;
            color: #FFFFFF;
            text-align: left;
        }
        .auto-style3 {
            color: #0099FF;
        }
        .thongtin
        {
            margin: 20px 20px;
            text-align:left;
            height:300px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="viewsanpham">
        <tr>
            <td class="otensanpham">
                <asp:Label ID="lblTuVan" runat="server" Text="GÓC TƯ VẤN" CssClass="cttensanpham"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table class="thongtin">
                    <tr>
                        <td><span class="auto-style3"><strong>THẠCH ẢNH LÀ GÌ?</strong></span><br />
                            Thạch ảnh là một loại hình nghệ thuật mới, đưa hình ảnh vào đá, với hình ảnh cực kì sắc nét, chân thực, được thể hiện trên nhiều loại đá khác nhau, nhằm đảm bảo nhu cầu của khách hàng.<br />
                        </td>
                    </tr>
                    <tr>
                        <td><span class="auto-style3"><strong>ĐÁ ĐEN LÀ ĐÁ GÌ?</strong></span><br />
                            Loại đá này đến nay cũng chỉ có ở cao nguyên Việt Nam, đá thuần chất có sắc đen.<br />
                        </td>
                    </tr>
                    <tr>
                        <td><span class="auto-style3"><strong>ĐÁ KHI BỤI BÁM THÌ LÀM SẠCH THẾ NÀO?</strong></span><br />
                            Trường hợp bụi bám nhẹ thì dùng khăn khô lau bình thường nếu vết bẩn khó tẩy dùng xà phòng sẽ sạch ngay thôi.<br />
                        </td>
                    </tr>
                    <tr>
                        <td><span class="auto-style3"><strong>CÓ ĐẶT HỆ THỐNG PHÂN PHỐI KHÔNG? </strong></span>
                            <br />
                            Thạch Ảnh Đăng Quang chuyên huyên cung cấp sĩ và lẻ cho các đại lí trên cả nước, giá cả thỏa thuận. Nếu quí khách có nhu cầu phân phối sản phẩm, xin hãy gọi tới: <strong>01654141413</strong> -<strong>01645398501</strong></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

