﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="ThongTinSanPham.aspx.cs" Inherits="ThongTinSanPham" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewsanpham {
            margin:20px auto;
            background:white;
        }
        .viewsanpham img
        {
            display:block;
            margin:20px auto;
            height:300px;
        }
        .viewsanpham p 
        {
            margin:20px 50px;
            color:black;
        }
        .viewsanpham .p1
        {
            text-align:center;
            font-size:20px;
            color:red;
            font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="viewsanpham">
        <tr>
            <td>
                <img src="Image/Da/hinh-cuoi-1.jpg" />
               <p><b>Cơ sở thạch ảnh Đăng Quang</b>chuyên tạo ra những sản phẩm có giá trị thẩm mĩ cao, phù hợp với nhu cầu của khách hàng. Bằng công nghệ sáng tạo, không những in trên Đá (Thạch ảnh) mà cơ sở chúng tôi còn in trên Gỗ, Lá cây tự nhiên ( đặc biệt là lá Bồ Đề). Những sản phẩm này đảm bảo với hình ảnh đẹp, sắc nét, chân thực cao. Mong được sự ủng hộ của quí khách.</p>
               <p class="p1">Cơ sản sản xuất Thạch Ảnh Đăng Quang</p>
               <p class="p1">Xin chân thành cảm ơn!</p>
            </td>
        </tr>
    </table>

</asp:Content>

