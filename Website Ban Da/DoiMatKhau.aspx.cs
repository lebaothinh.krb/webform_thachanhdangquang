﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class DoiMatKhau : System.Web.UI.Page
{
    KhachHangBusiness khbn = new KhachHangBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["KHACHHANGDN"] == null) Response.Redirect("~/DangNhap.aspx");
    }
    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)Session["KHACHHANGDN"];
        if (txtMatKhauCu.Text == dt.Rows[0]["MATKHAU"].ToString())
        {
            khbn.DoiMatKhau(dt.Rows[0]["MAKHACHHANG"].ToString(), txtMatKhau.Text);
            Response.Write("<script>alert('Đổi Mật Khẩu Thành Công!')</script>");
        }
        else
        {
            Response.Write("<script>alert('Mật Khẩu Cũ Sai!')</script>");
        }
    }
}