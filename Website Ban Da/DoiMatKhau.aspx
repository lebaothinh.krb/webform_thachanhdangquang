﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="DoiMatKhau.aspx.cs" Inherits="DoiMatKhau" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewsanpham {
            margin:20px auto;

        }
        .nutcapnhat
        {
            background:#ff6a00;
            height:30px;
            width:100px;
            color:white;
        }
        .auto-style2 {
            color: #000000;
        }
        .nutcapnhat {
           
        }
        .auto-style3 {
            height: 18px;
            font-weight: 700;
            font-size: large;
            color: #0066FF;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="viewsanpham">
        <tr class="auto-style2">
            <td class="auto-style3" colspan="2">ĐỔI MẬT KHẨU</td>
        </tr>
        <tr class="auto-style2">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="auto-style2">
            <td>Nhập Mật Khẩu Cũ</td>
            <td style="text-align: left">
                <asp:TextBox ID="txtMatKhauCu" runat="server" TextMode="Password" Width="200px" style="text-align: left"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMatKhauCu" ErrorMessage="Không Được Để Trống" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="auto-style2">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="auto-style2">
            <td>Nhập Nhập Khẩu Mới</td>
            <td>
                <asp:TextBox ID="txtMatKhau" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNhapLaiMatKhau" ControlToValidate="txtMatKhau" ErrorMessage="Mật Khẩu Mới Phải Trùng Nhau" ForeColor="Red">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMatKhau" ErrorMessage="Không Được Để Trống" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="auto-style2">
            <td>Nhập Lại Mật Khẩu Mới</td>
            <td>
                <asp:TextBox ID="txtNhapLaiMatKhau" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNhapLaiMatKhau" ErrorMessage="Không Được Để Trống" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="auto-style2">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="auto-style2">
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnCapNhat" runat="server" Text="Cập Nhật" CssClass="nutcapnhat" OnClick="btnCapNhat_Click" BorderStyle="None" />
            </td>
        </tr>
    </table>

</asp:Content>

