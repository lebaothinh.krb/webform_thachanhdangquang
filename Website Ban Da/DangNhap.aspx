﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="DangNhap.aspx.cs" Inherits="DangNhap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .baoloi
{
    margin-left:100px;
}
        a
        {
            text-decoration:none;
        }
        .auto-style2 {
            background: #F37021;
            height: 52px;
            font-weight: bold;
            font-size: medium;
            text-align: center;
            width: 358px;
        }
        .auto-style3 {
            background:#183544;
            padding: 5px 5px;
            width: 300px;
        }
        .auto-style4 {
            height: 18px;
        }
        .dangnhapnhanhdn {
            text-align: center;
        }
        .auto-style5 {
            width: 419px;
        }
        .auto-style6 {
            background: #183544;
            padding: 5px 5px;
            width: 419px;
        }
        .auto-style7 {
            width: 168px;
        }
        .auto-style8 {
            height: 30px;
            font-style: italic;
            text-decoration: underline;
            width: 168px;
        }
        .auto-style9 {
            text-align: center;
            width: 168px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table class="viewsanphamdn">
        <tr>
            <td class="auto-style5">
                &nbsp;</td>
            <td class="auto-style2">
                <asp:Label ID="Label1" runat="server" CssClass="cttensanphamdn" Text="ĐĂNG NHẬP&nbsp"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6">
                &nbsp;</td>
            <td class="auto-style3">
                <table class="thandn">
                    <tr>
                        <td class="chuchungdn">Tên Đăng Nhập:</td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txtTaiKhoan" runat="server" CssClass="txtdn"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTaiKhoan" ErrorMessage="Chưa Nhập Tài Khoản" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="chuchungdn">Mật Khẩu:</td>
                        <td class="auto-style7">
                            <asp:TextBox ID="txtMatKhau" runat="server" CssClass="txtdn" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMatKhau" ErrorMessage="Chưa Nhập Mật Khẩu" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="auto-style7">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Button ID="Button1" runat="server" Text="ĐĂNG NHẬP" CssClass="nut" OnClick="Button1_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="auto-style7">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="chuchung1dn">
                            <asp:HyperLink ID="HyperLink2" runat="server" ForeColor="White" NavigateUrl="~/DangKy.aspx">Đăng Ký</asp:HyperLink>
                        </td>
                        <td class="auto-style8" style="text-align: right">
                            <asp:HyperLink ID="HyperLink1" runat="server" ForeColor="White" NavigateUrl="~/QuenMatKhau.aspx">Quên Mật Khẩu</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblBaoLoi" runat="server" ForeColor="White"></asp:Label>
                        </td>
                        <td class="auto-style7">&nbsp;</td>
                    </tr>
                    </table>
            </td>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="baoloi" ForeColor="White" style="text-align: left" />
            </td>
        </tr>
    </table>
    <link rel="stylesheet" href="Scripts/CSS/DangNhapDangKy.css" />

</asp:Content>

