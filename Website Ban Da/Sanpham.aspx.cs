﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class Sanpham : System.Web.UI.Page
{
    SanPhamBusiness spbn = new SanPhamBusiness();
    GioHang ghbn = new GioHang();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["MUAHANG"] = null;
            if (Request.QueryString["MASANPHAM"] != null) ghbn.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MASANPHAM"].ToString()));

            lblSanPham.Text = "SẢN PHẨM";
            ddlSanpham.DataSource = spbn.LoadSanPham();
            ddlSanpham.DataBind();

            ddlSanPhamNgang.DataSource = spbn.LoadTop5SoLanXem();
            ddlSanPhamNgang.DataBind();
        }
    }
    protected void ddlSanPhamNgang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}