﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="Sanphamtheochude.aspx.cs" Inherits="Sanphamtheochude" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="Scripts/CSS/Sanphamtheochude.css" />
    <link rel="stylesheet" href="Scripts/CSS/DaMoi.css" />
    <link rel="stylesheet" href="Scripts/CSS/Chitietsanpham.css" />
    <style type="text/css">
        .auto-style2 {
            width: 206px;
            text-align: center;
        }
        .auto-style12 {
            width: 206px;
            height: 32px;
            text-align: center;
        }
        .ddlspn {
            text-align: right;
        }
        .auto-style13 {
            height: 18px;
            font-size: medium;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="viewsanpham">
        <tr>
            <td class="otensanpham">
                <asp:Label ID="lblChuDe" runat="server" CssClass="cttensanpham"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
<asp:DataList ID="ddlSanpham" runat="server" RepeatColumns="4" CssClass="sanphamindex">
            <ItemTemplate>
                <table class="sanpham">
                    <tr>
                        <td class="auto-style2">
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="tensanpham" NavigateUrl='<%# "~/Chitietda?MaSP="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>' ImageUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hinhminhhoa" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="dongia">Đơn Giá:
                            <asp:Label ID="Label1" runat="server" style="color: #FF3300; font-weight: 700" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>' CssClass="gia"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="hyMuaNgay" runat="server" CssClass="nutmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Height="30px" Width="90px" Font-Overline="False">MUA NGAY</asp:HyperLink>
                            <asp:HyperLink ID="hyThemVaoGio" runat="server" CssClass="nutthemvaogio" Height="30px" NavigateUrl='<%# "~/Sanphamtheochude.aspx?MaChuDe="+Eval("MACHUDE")+"&MaMuaHang="+Eval("MASANPHAM") %>' Width="130px">THÊM VÀO GIỎ</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList>
            </td>
        </tr>
        <tr>
            <td class="auto-style13">Có Thể Bạn Muốn Xem</td>
        </tr>
        <tr>
            <td style="text-align: center" class="ofooter"><asp:DataList ID="ddlSanPhamNgang" runat="server" RepeatColumns="5" CssClass="ddlspn" EnableTheming="True" RepeatDirection="Horizontal">
                    <ItemTemplate>
                        <table class="ospnviewsanpham">
                            <tr>
                                <td class="auto-style14">
                                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="spnhinhanh" ImageHeight="260px" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospntensanpham">
                                    <asp:HyperLink ID="HyperLink2" runat="server" ForeColor="Black" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>'></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospngiasanpham">
                                    <asp:Label ID="spnlblGia" runat="server" CssClass="spngia" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="ospnmuangay">
                                    <asp:HyperLink ID="HyperLink3" runat="server" CssClass="spnmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MASANPHAM="+Eval("MASANPHAM") %>'>MUA NGAY</asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>

</asp:Content>

