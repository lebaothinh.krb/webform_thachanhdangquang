﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;
public partial class DangNhap : System.Web.UI.Page
{
    KhachHangBusiness khbn = new KhachHangBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["MUAHANG"] = null;
    }
    protected void btnDangKy_Click(object sender, EventArgs e)
    {
        khbn.ThemKhachHang(txtTenKhachHang.Text,txtDienThoai.Text,ddlThang.Text+"/"+ddlNgay.SelectedValue+"/"+txtNam.Text,((rdNu.Checked==true)?"False":"True"),txtTenDangNhap.Text,txtMatKhau.Text,txtDiaChi.Text,txtEmail.Text);
        Response.Write("<script>alert('Đăng Ký Thành Công!')</script>");
        Response.Redirect("~/DangNhap.aspx");
    }
}