﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="DatHangTheoYeuCau.aspx.cs" Inherits="DatHangTheoYeuCau" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewsanpham{
            margin:20px auto;
            color:black;
            height:300px;
        }
        .otensanpham
        {
            background:#F37021;
            height:52px;
            font-weight: bold;
            font-size:medium;
        }
        .cttensanpham
        {
            margin-left:10px;
            font-weight:400;
            line-height:26px;
            font-size:24px;
            color: #FFFFFF;
            text-align: left;
        }
        .auto-style2 {
            height: 18px;
            text-align: center;
        }
        .nut
        {
            color:white;
            height:30px;
            width:100px;
            background:#F37021;
        }
        .auto-style4 {
            width: 200px;
            padding: 10px 10px;
            text-align: justify;
        }
        .soluonglon
        {
            margin: 10px 10px;
            font-weight: 700;
        }
        .baoloi
        {
            margin-left:20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="viewsanpham">
        <tr>
            <td colspan="3" class="otensanpham">
                <asp:Label ID="lblDatHangTheoYeuCau" runat="server" CssClass="cttensanpham" Text="ĐẶT HÀNG THEO YÊU CẦU&nbsp"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Thể Loại:</td>
            <td>
                <asp:DropDownList ID="ddlTheLoai" runat="server" Width="300px">
                </asp:DropDownList>
            </td>
            <td class="auto-style4" rowspan="8">
                <asp:Label ID="Label1" runat="server" CssClass="soluonglon" Text="Nếu bạn có nhu cầu đặt hàng số lượng lớn. Xin vui lòng liên hệ:"></asp:Label>
                <br />
                <br />
                01654141413<br />
                <br />
                01206222535 &nbsp; <br />
                <br />
                01645398501<br />
                <br />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="baoloi" style="text-align: left" />
            </td>
        </tr>
        <tr>
            <td>Kích Thước (cm):</td>
            <td>
                <asp:TextBox ID="txtKichThuoc" runat="server" Width="300px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtKichThuoc" ErrorMessage="Chưa Nhập Kích Thước" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Hình Ảnh:</td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" />
            </td>
        </tr>
        <tr>
            <td>Nội Dung</td>
            <td>
                <asp:TextBox ID="txtNoiDung" runat="server" Width="300px" Height="58px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNoiDung" ErrorMessage="Chưa Nhập Nội Dung" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Số Lượng</td>
            <td>
                <asp:TextBox ID="txtSoLuong" runat="server" Width="300px"></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtSoLuong" ErrorMessage="Số Lượng Lớn Hơn 0" ForeColor="Red" MaximumValue="99999" MinimumValue="1" SetFocusOnError="True" Type="Integer">*</asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="2">
                <asp:RadioButton ID="rdChungtoi" runat="server" GroupName="VATLIEU" Text="Vật Liệu Của Chúng Tôi" Checked="True" ValidationGroup="vatlieu" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="rdCuaBan" runat="server" GroupName="VATLIEU" Text="Vật Liệu Của Bạn" ValidationGroup="vatlieu" />
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="2">
                <asp:Button ID="Button1" CssClass="nut" runat="server" Text="ĐẶT HÀNG" BorderStyle="None" OnClick="Button1_Click" />
            </td>
        </tr>
    </table>

</asp:Content>

