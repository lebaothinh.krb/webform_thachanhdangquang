﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class Chitietsanpham : System.Web.UI.Page
{
    SanPhamBusiness spbn = new SanPhamBusiness();
    LoaiBussiness lbn = new LoaiBussiness();
    TheLoaiBussiness tlbn = new TheLoaiBussiness();
    ChuDeBussiness cdbn = new ChuDeBussiness();
    HinhThucGiaoHang htgh = new HinhThucGiaoHang();
    HinhThucThanhToan httt = new HinhThucThanhToan();
    GioHang ghbn = new GioHang();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["MaMuaHang"] != null) ghbn.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MaMuaHang"].ToString()));
            Session["MUAHANG"] = null;
            DataTable dt = spbn.LoadMotSanPham(Request.QueryString["MaSanPham"]);
            lblTenSanPham.Text = dt.Rows[0]["TENSANPHAM"].ToString();
            imgHinhMinhHoa.ImageUrl = "~/Image/Da/" + dt.Rows[0]["HINHMINHHOA"].ToString();
            lblDonGia.Text = Convert.ToInt64(dt.Rows[0]["DONGIA"]).ToString();
            lblMoTa.Text = "Tên Sản Phẩm: <b>" + dt.Rows[0]["TENSANPHAM"].ToString() + "</b>. <br><br>Sản phẩm được in trên chất liệu " + lbn.TenLoai(dt.Rows[0]["LOAI"].ToString())+".<br><br>\r\n"+dt.Rows[0]["MOTA"].ToString();
            lblChuDe.Text = cdbn.TenChuDe(dt.Rows[0]["MACHUDE"].ToString());
            lblLoaiTheLoai.Text = tlbn.TenTheLoai(dt.Rows[0]["THELOAI"].ToString()) + " / " + lbn.TenLoai(dt.Rows[0]["LOAI"].ToString());
            ddlSanPhamNgang.DataSource = spbn.LoadTop5SoLanXem();
            ddlSanPhamNgang.DataBind();
            ddlGiaoHang.DataSource = htgh.LoadHinhThucGiaoHang();
            ddlGiaoHang.DataBind();
            ddlHinhThucTT.DataSource = httt.LoadHinhThucTT();
            ddlHinhThucTT.DataBind();
        }
        
    }
    protected void btnDatMua_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Giohang.aspx?MASANPHAM=" + Request.QueryString["MASANPHAM"]);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        ghbn.ThemMotSanPham(Convert.ToInt32(Request.QueryString["MaSanPham"].ToString()));
        Response.Redirect(Page.Request.Url.ToString());
    }
}