﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="TinhTrangDonHang.aspx.cs" Inherits="TinhTrangDonHang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewsanpham {
           margin:20px auto;
           height:400px;
        }
        .auto-style3 {
            color: #000000;
        }
        .auto-style5 {
            height: 18px;
        }
        .thongtin
        {
            padding:5px 20px;
        }
        .tinhtrang
        {
            display:block;
            margin-left:auto;
            margin-right:auto;
        }
        .ctdonhang
        {
            margin-left:auto;
            margin-right:auto;
        }
        .auto-style7 {
            padding: 5px 20px;
            color: #000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table class="viewsanpham">
        <tr>
            <td class="auto-style7" colspan="2">Vui lòng nhập số đơn hàng</td>
        </tr>
        <tr class="auto-style3">
            <td class="thongtin" colspan="2">
    <asp:TextBox ID="txtMaDonHang" runat="server" TextMode="Number" Width="217px"></asp:TextBox>
    <asp:Button ID="btnKiemTra" runat="server" Text="Kiểm Tra" BackColor="#E46D18" BorderStyle="None" ForeColor="White" OnClick="btnKiemTra_Click" Height="30px" Width="100px" />
            </td>
        </tr>
        <tr class="auto-style3">
            <td class="auto-style5">
                &nbsp;</td>
            <td class="auto-style5"></td>
        </tr>
        <tr class="auto-style3">
            <td class="thongtin">
                <asp:Label ID="lblThongTinChung" runat="server"></asp:Label>
            </td>
            <td class="thongtin">
                <asp:Label ID="lblDiaChi" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="auto-style3">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="auto-style3">
            <td colspan="2">
    <asp:Image ID="imgtinhtrang" runat="server" Height="60px" Width="1000px" CssClass="tinhtrang" />
            </td>
        </tr>
        <tr class="auto-style3">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="auto-style3">
            <td colspan="2">
            <asp:GridView ID="gvChitiet" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" DataKeyNames="SODONHANG" CssClass="ctdonhang" GridLines="Horizontal" Width="738px">
                <Columns>
                    <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" ReadOnly="True" />
                    <asp:BoundField DataField="MASANPHAM" HeaderText="Mã Sản Phẩm" ReadOnly="True" />
                    <asp:BoundField DataField="TENSANPHAM" HeaderText="Tên Sản Phẩm" />
                    <asp:BoundField DataField="SOLUONG" HeaderText="Số Lượng" />
                    <asp:BoundField DataField="DONGIA" HeaderText="Đơn Giá" ReadOnly="True" />
                    <asp:BoundField DataField="THANHTIEN" HeaderText="Thành Tiền" ReadOnly="True" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#333333" />
                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#487575" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#275353" />
            </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblBaoLoi" runat="server" style="color: #000000"></asp:Label>
</asp:Content>

