﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageMain.master" AutoEventWireup="true" CodeFile="TimKiem.aspx.cs" Inherits="TimKiem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <link rel="stylesheet" href="Scripts/CSS/DaMoi.css" />
    <table class="viewsanpham">
    <tr>
        <td class="auto-style3">
            <asp:Label ID="lblKetQua" runat="server" CssClass="tieudeindex"></asp:Label>
            </td>
    </tr>
    <tr>
        <td class="osanphamindex">
<asp:DataList ID="dlSanPham" runat="server" RepeatColumns="4" CssClass="sanphamindex">
            <ItemTemplate>
                <table class="sanpham">
                    <tr>
                        <td class="otensanpham">
                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="tensanpham" NavigateUrl='<%# "~/Chitietda?MaSP="+Eval("MASANPHAM") %>' Text='<%# Eval("TENSANPHAM") %>' ImageUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="hinhminhhao">
                            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hinhminhhoa" ImageUrl='<%# "~/Image/Da/"+Eval("HINHMINHHOA") %>' NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>'>HyperLink</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="dongia">Đơn Giá:
                            <asp:Label ID="Label1" runat="server" style="color: #FF3300; font-weight: 700" Text='<%# Eval("DONGIA","{0:#,##0 VNĐ}") %>' CssClass="gia"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="HyperLink7" runat="server" CssClass="nutmuangay" NavigateUrl='<%# "~/Chitietsanpham.aspx?MaSanPham="+Eval("MASANPHAM") %>' Height="30px" Width="90px" Font-Overline="False">MUA NGAY</asp:HyperLink>
                            <asp:HyperLink ID="hyThemVaoGio" runat="server" CssClass="nutthemvaogio" Height="30px" Width="130px" BorderStyle="None" NavigateUrl='<%# "~/Sanphamtheochude.aspx?MaChuDe="+Eval("MACHUDE")+"&MaMuaHang="+Eval("MASANPHAM") %>'>THÊM VÀO GIỎ</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList></td>
    </tr>
</table>
</asp:Content>

