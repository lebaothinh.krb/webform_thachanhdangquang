﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class DatHangTheoYeuCau : System.Web.UI.Page
{
    TheLoaiBussiness tlbn = new TheLoaiBussiness();
    DatHangTheoYeuCauBusiness dhtyc = new DatHangTheoYeuCauBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["KHACHHANGDN"] == null)
        {
            Response.Redirect("~/DangNhap.aspx");
        }
        if (!IsPostBack)
        {
            Session["MUAHANG"] = null;
            ddlTheLoai.DataSource = tlbn.LoadTheLoai();
            ddlTheLoai.DataTextField = "TENTHELOAI";
            ddlTheLoai.DataValueField = "MATHELOAI";
            ddlTheLoai.DataBind();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = (DataTable)Session["KHACHHANGDN"];
            dhtyc.ThemDatHang(dt.Rows[0]["MAKHACHHANG"].ToString(),txtKichThuoc.Text, FileUpload1.FileName, txtNoiDung.Text, ((rdChungtoi.Checked == true) ? rdChungtoi.Text : rdCuaBan.Text),txtSoLuong.Text);
            FileUpload1.SaveAs(MapPath("~/Image/dathangtheoyeucau/" + FileUpload1.FileName));
            Response.Write("<script>alert('Đặt Hàng Thành Công! Chúng tôi sẽ liên hệ với bạn sớm nhất có thể để xác nhận đơn hàng!')</script>");
        }
        catch
        {
            Response.Write("<script>alert('Đặt Hàng Không Thành Công')</script>");
        }
    }
}