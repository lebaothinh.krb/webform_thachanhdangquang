﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class QuenMatKhau : System.Web.UI.Page
{
    KhachHangBusiness khbn = new KhachHangBusiness();
    Email email = new Email();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblBaoLoi.Text = "";
        if (!IsPostBack)
        {
            khbn.XoaNgayHetHan();
            if (Session["KHACHHANGDN"] != null) return;
            if (Request.QueryString["MaQuenMatKhau"] != null)
            {
                DataTable dt = khbn.LoadQuenMatKhau();
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["RANDOM"].ToString() == Request.QueryString["MaQuenMatKhau"].ToString())
                    {
                        pnXacNhanEmail.Visible = true;
                        pnDoiMatKhau.Visible = false;
                        pnQuenMatKhau.Visible = false;
                        return;
                    }
                }
                Response.Redirect("~/QuenMatKhau.aspx");              
            }
            else
            {
                pnXacNhanEmail.Visible = false;
                pnDoiMatKhau.Visible = false;
                pnQuenMatKhau.Visible = true;
            }
        }
    }
    protected void btnGui_Click(object sender, EventArgs e)
    {
        if (txtEmail.Text =="")
        {
            lblBaoLoi.Text = "Không Được Để Trống!";
            return;
        }
        DataTable dt = khbn.LoadKhachHang();
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["EMAIL"].ToString()==txtEmail.Text.Trim())
            {
                string Random ="";
                Random rand = new Random();
                const string characterArray = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                for (int i=0;i<10;i++)
                {
                    string str = characterArray[rand.Next(characterArray.Length)].ToString();
                    Random+=str;
                }
                try
                {
                    khbn.XoaQuenMatKhau(txtEmail.Text.Trim());
                }
                catch { }
                khbn.ThemQuenMatKhau(dr["MAKHACHHANG"].ToString(), dr["EMAIL"].ToString(), Random, DateTime.Now.AddDays(1).Month + "/" + DateTime.Now.AddDays(1).Day + "/" + DateTime.Now.AddDays(1).Year);
                email.QuenMatKhau(txtEmail.Text.Trim(), dr["TENKHACHHANG"].ToString(), Random);
                lblBaoLoi.Text = "Chúng Tôi Đã Gửi Email Đổi Mật Khẩu Cho Bạn. Hãy Kiểm Tra Hòm Thư!";
                return;
            }
        }
        lblBaoLoi.Text = "Email này không tồn tại trong đăng kí tài khoản.";
    }
    protected void btnNhapLaiEmail_Click(object sender, EventArgs e)
    {
        if (txtNhapLaiEmail.Text == "")
        {
            lblBaoLoi.Text = "Không Được Để Trống!";
            return;
        }
        DataTable dt =khbn.LoadQuenMatKhau();
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["EMAIL"].ToString()==txtNhapLaiEmail.Text.Trim() && dr["RANDOM"].ToString()==Request.QueryString["MaQuenMatKhau"].ToString())
            {
                pnDoiMatKhau.Visible = true;
                pnQuenMatKhau.Visible = false;
                pnXacNhanEmail.Visible = false;
                return;
            }
        }
        lblBaoLoi.Text = "Email không đúng!";
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (txtMatKhau.Text == "" || txtNhapLaiMatKhau.Text=="")
        {
            lblBaoLoi.Text = "Không Được Để Trống!";
            return;
        }
        if (txtMatKhau.Text == txtNhapLaiMatKhau.Text)
        {
            DataTable dt = khbn.LoadQuenMatKhau();
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["RANDOM"].ToString()==Request.QueryString["MaQuenMatKhau"].ToString())
                {
                    khbn.DoiMatKhau(dr["MAKHACHHANG"].ToString(), txtMatKhau.Text);
                    khbn.XoaQuenMatKhau(dr["EMAIL"].ToString());
                    break;
                }
            }
            Response.Write("<script>alert('Đổi Mật Khẩu Thành Công!')</script>");
            Response.Redirect("~/DangNhap.aspx");
        }
        else
        {
            lblBaoLoi.Text = "Mật Khẩu Và Nhập Lại Mật Khẩu Phải Trùng Nhau!";
            return;
        }
    }
}