﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_QuanLyKhachHang : System.Web.UI.Page
{
    KhachHangBusiness khbn = new KhachHangBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }
        if (!IsPostBack)
        {
            CapNhat();
        }
    }
    public void CapNhat()
    {
        gvKhachHang.DataSource = khbn.LoadKhachHang();
        gvKhachHang.DataBind();
    }

    protected void gvKhachHang_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvKhachHang.PageIndex = e.NewPageIndex;
        CapNhat();
    }
    protected void gvKhachHang_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvKhachHang.EditIndex = -1;
        CapNhat();
    }
    protected void gvKhachHang_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName=="ThemKhachHang")
        {
            MultiView1.ActiveViewIndex = 1;
            
        }
    }
    protected void gvKhachHang_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        khbn.XoaKhachHang(gvKhachHang.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
        Response.Write("<script>alert('Xóa Thành Công!')</script>");
    }
    protected void gvKhachHang_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvKhachHang.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvKhachHang_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        khbn.CapNhatKhachHang(gvKhachHang.Rows[e.RowIndex].Cells[0].Text, (gvKhachHang.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text, (gvKhachHang.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text, (gvKhachHang.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text, (gvKhachHang.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvKhachHang.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvKhachHang.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text, (gvKhachHang.Rows[e.RowIndex].Cells[7].Controls[0] as TextBox).Text, (gvKhachHang.Rows[e.RowIndex].Cells[8].Controls[0] as TextBox).Text, (gvKhachHang.Rows[e.RowIndex].Cells[9].Controls[0] as TextBox).Text);
        gvKhachHang.EditIndex = -1;
        CapNhat();
        Response.Write("<script>alert('Cập Nhật Thành Công!')</script>");
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        khbn.ThemKhachHang(txtTenKhachHang.Text, txtDienThoai.Text, txtThang.Text +"/"+ txtNgay.Text +"/"+ txtNam.Text, ddlGioiTinh.SelectedValue, txtTenDangNhap.Text, txtMatKhau.Text, txtDiaChi.Text, txtEmail.Text);
        Response.Write("<script>alert('Thêm Thành Công!')</script>");
        CapNhat();
        MultiView1.ActiveViewIndex = 0;
    }
}