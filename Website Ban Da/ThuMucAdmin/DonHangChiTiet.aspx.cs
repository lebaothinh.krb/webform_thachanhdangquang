﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_DonHangChiTiet : System.Web.UI.Page
{
    CTDatHang ctdhbn = new CTDatHang();
    SanPhamBusiness spbn = new SanPhamBusiness();
    public void TinhTien()
    {
        double tongtien = 0;
        foreach (GridViewRow gvr in gvChitiet.Rows)
        {
            tongtien += (Convert.ToDouble(gvr.Cells[2].Text) * Convert.ToDouble(gvr.Cells[3].Text));
        }
        lblTongThanhTien.Text = "Tổng Thành Tiền: " + tongtien.ToString() + " VNĐ";
    }
    public void CapNhat()
    {
        gvChitiet.DataSource = ctdhbn.LoadCTDatHang(Request.QueryString["SoDonHang"].ToString());
        gvChitiet.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }
        if (!IsPostBack)
        {
            CapNhat();
            ddlMaSanPham.SelectedIndex = 0;
        }
        TinhTien();
    }
    protected void gvChitiet_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName=="ThemCt")
        {
            MultiView1.ActiveViewIndex = 1;
            ddlMaSanPham.DataSource = spbn.LoadSanPham();
            ddlMaSanPham.DataTextField = "MASANPHAM";
            ddlMaSanPham.DataValueField = "MASANPHAM";
            ddlMaSanPham.DataBind();
            txtSoLuong.Text = "";
            ddlMaSanPham.SelectedIndex = 0;
        }
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        DataTable dt = spbn.LoadMotSanPham(ddlMaSanPham.SelectedValue);
        ctdhbn.ThemCTDatHang(Request.QueryString["SoDonHang"].ToString(), ddlMaSanPham.SelectedValue, Convert.ToInt32(txtSoLuong.Text), Convert.ToDouble(dt.Rows[0]["DONGIA"].ToString()));
        Response.Write("<script>alert('Thêm Thành Công')</script>");
        CapNhat();
        TinhTien();
        MultiView1.ActiveViewIndex = 0;
    }
    protected void ddlMaSanPham_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = spbn.LoadMotSanPham(ddlMaSanPham.SelectedValue);
        txtTenSanPham.Text = dt.Rows[0]["TENSANPHAM"].ToString();
    }
    protected void gvChitiet_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ctdhbn.XoaCTDatHang(gvChitiet.Rows[e.RowIndex].Cells[0].Text, gvChitiet.Rows[e.RowIndex].Cells[1].Text);
        CapNhat();
        TinhTien();
        Response.Write("<script>alert('Xóa Thành Công')</script>");
    }
    protected void gvChitiet_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvChitiet.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvChitiet_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        ctdhbn.CapNhatCTDatHang(gvChitiet.Rows[e.RowIndex].Cells[0].Text, gvChitiet.Rows[e.RowIndex].Cells[1].Text, (gvChitiet.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text);
        gvChitiet.EditIndex = -1;
        CapNhat();
        TinhTien();
        Response.Write("<script>alert('Cập Nhật Thành Công')</script>");
    }
    protected void gvChitiet_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvChitiet.EditIndex = -1;
        CapNhat();
        TinhTien();
    }
}