﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_QuanTriHeThong : System.Web.UI.Page
{
    Admin admin = new Admin();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblThongBao.Text = "";
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }

        if (!IsPostBack)
        {
            CapNhat();
        }
    }
    public void CapNhat()
    {
        if (gvAdmin.Rows.Count > 0)
            lblThongBao.Visible = false;
        else
            lblThongBao.Text = "Không Có Admin Dưới Quyền Của Bạn";
        gvAdmin.DataSource = admin.LoadAdmin(Session["QuyenHanAdmin"].ToString());
        gvAdmin.DataBind();
    }
    protected void gvAdmin_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAdmin.PageIndex = e.NewPageIndex;
        CapNhat();
    }
    protected void gvAdmin_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvAdmin.EditIndex = -1;
        CapNhat();
    }
    protected void gvAdmin_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ThemAdmin")
        {
            MultiView1.ActiveViewIndex = 1;
            txtTenAdmin.Text = "";
            txtDienThoai.Text = "";
            txtNgay.Text = "";
            txtThang.Text = "";
            txtNam.Text = "";
            txtTenDangNhap.Text = "";
            txtMatKhauAdmin.Text="";
            txtDiaChi.Text = "";
            txtEmail.Text = "";
            txtQuyenHan.Text="";
            ddlGioiTinh.SelectedIndex = 0;
        }
    }
    protected void gvAdmin_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        admin.XoaAdmin(gvAdmin.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
        Response.Write("<script>alert('Xóa Thành Công!')</script>");
    }
    protected void gvAdmin_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DateTime a = Convert.ToDateTime((gvAdmin.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text);
        admin.CapNhatAdmin(gvAdmin.DataKeys[e.RowIndex].Value.ToString(), (gvAdmin.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text, (gvAdmin.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text,a.Month.ToString()+"/"+a.Day.ToString()+"/"+a.Year.ToString(), (gvAdmin.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvAdmin.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvAdmin.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text, (gvAdmin.Rows[e.RowIndex].Cells[7].Controls[0] as TextBox).Text, (gvAdmin.Rows[e.RowIndex].Cells[8].Controls[0] as TextBox).Text, gvAdmin.Rows[e.RowIndex].Cells[9].Text);
        gvAdmin.EditIndex = -1;
        CapNhat();
        Response.Write("<script>alert('Cập Nhật Thành Công!')</script>");
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(txtQuyenHan.Text) >= Convert.ToInt32(Session["QuyenHanAdmin"].ToString()))
        {
            lblThongBao.Text = "Quyền hạn admin không được lớn hơn quyền hạn của bạn";
            return;
        }
        admin.ThemAdmin(txtTenAdmin.Text, txtDienThoai.Text, txtThang.Text + "/" + txtNgay.Text + "/" + txtNam.Text, ddlGioiTinh.SelectedValue, txtTenDangNhap.Text, txtMatKhauAdmin.Text, txtDiaChi.Text, txtEmail.Text, txtQuyenHan.Text);
        CapNhat();
        Response.Write("<script>alert('Thêm Thành Công!')</script>");
        MultiView1.ActiveViewIndex = 0;
    }
    protected void lbtnThemAdmin_Click(object sender, EventArgs e)
    { 
        MultiView1.ActiveViewIndex = 1;
        txtTenAdmin.Text = "";
        txtDienThoai.Text = "";
        txtNgay.Text = "";
        txtThang.Text = "";
        txtNam.Text = "";
        txtTenDangNhap.Text = "";
        txtMatKhauAdmin.Text = "";
        txtDiaChi.Text = "";
        txtEmail.Text = "";
        txtQuyenHan.Text = "";
        ddlGioiTinh.SelectedIndex = 0;
    }
    protected void gvAdmin_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvAdmin.EditIndex = e.NewEditIndex;
        CapNhat();
    }
}