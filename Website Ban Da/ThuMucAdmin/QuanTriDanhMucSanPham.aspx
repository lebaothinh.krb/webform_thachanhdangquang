﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="QuanTriDanhMucSanPham.aspx.cs" Inherits="ThuMucAdmin_QuanTriDanhMucSanPham" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
        .auto-style2 {
            font-size: large;
            color: #990000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <div class="auto-style1">
            <strong><span class="auto-style2">QUẢN LÝ SẢN PHẨM</span></strong><br />
        </div>
    
        <br />
        <asp:HyperLink ID="HyperLink1" runat="server" style="font-style: italic" NavigateUrl="~/ThuMucAdmin/AdminThemSanPham.aspx">Thêm Sản Phẩm</asp:HyperLink>
        <br />
        <br />
    
        <asp:GridView ID="gvSanPham" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="gvSanPham_RowCancelingEdit" OnRowDeleting="gvSanPham_RowDeleting" OnRowEditing="gvSanPham_RowEditing" OnRowUpdating="gvSanPham_RowUpdating" AllowPaging="True" DataKeyNames="MASANPHAM" OnPageIndexChanging="gvSanPham_PageIndexChanging" PageSize="8">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="MASANPHAM" HeaderText="Mã Sản Phẩm" ReadOnly="True" />
                <asp:BoundField DataField="TENSANPHAM" HeaderText="Tên Sản Phẩm" />
                <asp:BoundField DataField="DONVITINH" HeaderText="Đơn Vị Tính" />
                <asp:BoundField DataField="DONGIA" HeaderText="Đơn Giá" />
                <asp:BoundField DataField="MOTA" HeaderText="Mô Tả" />
                <asp:BoundField DataField="HINHMINHHOA" HeaderText="Hình Minh Họa" />
                <asp:BoundField DataField="MACHUDE" HeaderText="Chủ Đề" />
                <asp:BoundField DataField="NGAYCAPNHAT" HeaderText="Ngày Cập Nhật" />
                <asp:BoundField DataField="SOLUONGBAN" HeaderText="Số Lượng Bán" />
                <asp:BoundField DataField="SOLANXEM" HeaderText="Số Lần Xem" />
                <asp:BoundField DataField="LOAI" HeaderText="Loại" />
                <asp:BoundField DataField="THELOAI" HeaderText="Thể Loại" />
                <asp:CommandField ButtonType="Image" CancelText="" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditText="" HeaderText="Xóa" ShowDeleteButton="True" UpdateText="" />
                <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" EditImageUrl="~/Image/edit.png" HeaderText="Sửa" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" UpdateText="Cập Nhật" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
    
    </div>
</asp:Content>

