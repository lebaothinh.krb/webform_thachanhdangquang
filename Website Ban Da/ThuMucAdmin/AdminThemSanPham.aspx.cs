﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;
using Public;

public partial class ThuMucAdmin_AdminThemSanPham : System.Web.UI.Page
{
    LoaiBussiness loaibn = new LoaiBussiness();
    TheLoaiBussiness tlbn = new TheLoaiBussiness();
    ChuDeBussiness cdbn = new ChuDeBussiness();
    SanPhamBusiness spbn = new SanPhamBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }
        if (!IsPostBack)
        {
            ddlLoai.DataSource = loaibn.LoadLoai();
            ddlLoai.DataTextField = "TENLOAI";
            ddlLoai.DataValueField = "MALOAI";
            ddlLoai.DataBind();

            ddlTheLoai.DataSource = tlbn.LoadTheLoai();
            ddlTheLoai.DataTextField = "TENTHELOAI";
            ddlTheLoai.DataValueField = "MATHELOAI";
            ddlTheLoai.DataBind();

            ddlMaChuDe.DataSource = cdbn.LoadChuDe();
            ddlMaChuDe.DataTextField = "TENCHUDE";
            ddlMaChuDe.DataValueField = "MACHUDE";
            ddlMaChuDe.DataBind();

            lblNgayCapNhat.Text = DateTime.Now.ToShortDateString();
        }
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {

            spbn.ThemSanPham(txtTenSanPham.Text, txtDonViTinh.Text, Convert.ToDouble(txtDonGia.Text), ckeditor1.Text, FileUpload1.FileName, Convert.ToInt32(ddlMaChuDe.SelectedValue),DateTime.Now.Month+"/"+DateTime.Now.Day+"/"+DateTime.Now.Year, Convert.ToInt32(ddlLoai.SelectedValue), Convert.ToInt32(ddlTheLoai.SelectedValue));
            FileUpload1.SaveAs(MapPath("~/Image/Da/" + FileUpload1.FileName));
            Response.Redirect(Page.Request.Url.ToString());
            Response.Write("<script>alert('Thêm Thành Công!')</script>");

    }
}