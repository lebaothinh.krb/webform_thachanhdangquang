﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_QuanLyChuDeLoaiTheLoai : System.Web.UI.Page
{
    ChuDeBussiness cdbn = new ChuDeBussiness();
    LoaiBussiness lbn = new LoaiBussiness();
    TheLoaiBussiness tlbn = new TheLoaiBussiness();
    public void CapNhat()
    {
        gvChuDe.DataSource = cdbn.LoadChuDe();
        gvChuDe.DataBind();
        gvLoai.DataSource = lbn.LoadLoai();
        gvLoai.DataBind();
        gvTheLoai.DataSource = tlbn.LoadTheLoai();
        gvTheLoai.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }
        if (!IsPostBack)
        {
            CapNhat();
        }
    }
    protected void QuanLyChuDe_Click(object sender, EventArgs e)
    {
        mtvQuanLy.ActiveViewIndex = 0;
    }
    protected void QuanLyLoai_Click(object sender, EventArgs e)
    {
        mtvQuanLy.ActiveViewIndex = 1;
    }
    protected void QuanLyTheLoai_Click(object sender, EventArgs e)
    {
        mtvQuanLy.ActiveViewIndex = 2;
    }
    protected void gvChuDe_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        cdbn.XoaChuDe(gvChuDe.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
    }
    protected void gvChuDe_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvChuDe.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvChuDe_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        cdbn.CapNhatChuDe(gvChuDe.DataKeys[e.RowIndex].Value.ToString(), (gvChuDe.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text, (gvChuDe.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text);
        gvChuDe.EditIndex = -1;
        CapNhat();
    }
    protected void gvChuDe_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvChuDe.EditIndex = -1;
        CapNhat();
    }
    protected void gvChuDe_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ThemChuDe")
        {
            txtTenChuDe.Text = "";
            txtMoTaChuDe.Text = "";
            mtvQuanLy.ActiveViewIndex = 3;
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        cdbn.ThemChuDe(txtTenChuDe.Text, txtMoTaChuDe.Text);
        Response.Write("<script>alert('Thêm Chủ Đề Thành Công!')</script>");
        CapNhat();
        mtvQuanLy.ActiveViewIndex = 0;
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        lbn.ThemLoai(txtTenLoai.Text, txtMoTaLoai.Text);
        Response.Write("<script>alert('Thêm Loại Thành Công!')</script>");
        CapNhat();
        mtvQuanLy.ActiveViewIndex = 1;
    }
    protected void btnThemTheLoai_Click(object sender, EventArgs e)
    {
        tlbn.ThemTheLoai(txtTenTheLoai.Text, txtMoTaTheLoai.Text);
        Response.Write("<script>alert('Thêm Thể Loại Thành Công!')</script>");
        CapNhat();
        mtvQuanLy.ActiveViewIndex = 2;
    }
    protected void gvLoai_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ThemLoai")
        {
            mtvQuanLy.ActiveViewIndex = 4;
            txtTenLoai.Text = "";
            txtMoTaLoai.Text = "";
        }
    }
    protected void gvTheLoai_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ThemTheLoai")
        {
            mtvQuanLy.ActiveViewIndex = 5;
            txtTenTheLoai.Text = "";
            txtMoTaTheLoai.Text = "";

        }
    }
    protected void gvLoai_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        lbn.XoaLoai(gvLoai.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
    }
    protected void gvLoai_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvLoai.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvLoai_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        lbn.CapNhatLoai(gvLoai.DataKeys[e.RowIndex].Value.ToString(), (gvLoai.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text, (gvLoai.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text);
        gvLoai.EditIndex = -1;
        CapNhat();
    }
    protected void gvLoai_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvLoai.EditIndex = -1;
        CapNhat();
    }
    protected void gvTheLoai_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvTheLoai.EditIndex = -1;
        CapNhat();
    }
    protected void gvTheLoai_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        tlbn.XoaTheLoai(gvTheLoai.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
    }
    protected void gvTheLoai_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvTheLoai.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvTheLoai_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        tlbn.CapNhatTheLoai(gvTheLoai.DataKeys[e.RowIndex].Value.ToString(), (gvTheLoai.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text, (gvTheLoai.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text);
        gvTheLoai.EditIndex = -1;
        CapNhat();
    }
}