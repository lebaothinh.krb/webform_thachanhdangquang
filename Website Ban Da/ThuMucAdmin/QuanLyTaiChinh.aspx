﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="QuanLyTaiChinh.aspx.cs" Inherits="ThuMucAdmin_QuanLyTaiChinh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Chọn Xem Mức Doanh Thu
        <asp:DropDownList ID="ddlDoanhThu" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDoanhThu_SelectedIndexChanged">
            <asp:ListItem Value="0">Hôm Nay</asp:ListItem>
            <asp:ListItem Value="1">Tháng Nay</asp:ListItem>
            <asp:ListItem Value="2">Năm Nay</asp:ListItem>
        </asp:DropDownList>
        <br />
    
        <asp:MultiView ID="mtvQuanLyTaiChinh" runat="server" ActiveViewIndex="0">
            <asp:View ID="HomNay" runat="server">
                <br />
                <asp:GridView ID="gvHomNay" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="SODONHANG" ForeColor="#333333" GridLines="None"  style="text-align: center">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" />
                        <asp:BoundField DataField="TENKHACHHANG" HeaderText="Tên Khách Hàng" />
                        <asp:BoundField DataField="NGAYDATHANG" HeaderText="Ngày Đặt Hàng" />
                        <asp:BoundField DataField="NGAYGIAO" HeaderText="Ngày Giao" />
                        <asp:BoundField DataField="TRIGIA" HeaderText="Trị Giá" />
                        <asp:BoundField DataField="TENTINHTRANG" HeaderText="Tình Trạng" />
                        <asp:BoundField DataField="TINHTRANGTHANHTOAN" HeaderText="Thanh Toán" />
                        <asp:BoundField DataField="TENNGUOINHAN" HeaderText="Tên Người Nhận" />
                        <asp:BoundField DataField="DIACHINGUOINHAN" HeaderText="Địa Chỉ Người Nhận" />
                        <asp:BoundField DataField="DIENTHOAINGUOINHAN" HeaderText="SĐT Người Nhận" />
                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi Chú" />
                        <asp:BoundField DataField="TENHINHTHUCGH" HeaderText="HT Giao Hàng" />
                        <asp:BoundField DataField="TENHINHTHUCTT" HeaderText="HT Thanh Toán" />
                        <asp:BoundField HeaderText="Chi Tiết" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <br />
                <asp:Label ID="lblHomNay" runat="server"></asp:Label>
            </asp:View>
            <asp:View ID="ThangNay" runat="server">
                <br />
                <asp:GridView ID="gvThangNay" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="SODONHANG" ForeColor="#333333" GridLines="None" style="text-align: center">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" />
                        <asp:BoundField DataField="TENKHACHHANG" HeaderText="Tên Khách Hàng" />
                        <asp:BoundField DataField="NGAYDATHANG" HeaderText="Ngày Đặt Hàng" />
                        <asp:BoundField DataField="NGAYGIAO" HeaderText="Ngày Giao" />
                        <asp:BoundField DataField="TRIGIA" HeaderText="Trị Giá" />
                        <asp:BoundField DataField="TENTINHTRANG" HeaderText="Tình Trạng" />
                        <asp:BoundField DataField="TINHTRANGTHANHTOAN" HeaderText="Thanh Toán" />
                        <asp:BoundField DataField="TENNGUOINHAN" HeaderText="Tên Người Nhận" />
                        <asp:BoundField DataField="DIACHINGUOINHAN" HeaderText="Địa Chỉ Người Nhận" />
                        <asp:BoundField DataField="DIENTHOAINGUOINHAN" HeaderText="SĐT Người Nhận" />
                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi Chú" />
                        <asp:BoundField DataField="TENHINHTHUCGH" HeaderText="HT Giao Hàng" />
                        <asp:BoundField DataField="TENHINHTHUCTT" HeaderText="HT Thanh Toán" />
                        <asp:BoundField HeaderText="Chi Tiết" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <br />
                <asp:Label ID="lblThangNay" runat="server"></asp:Label>
            </asp:View>
            <asp:View ID="NamNay" runat="server">
                <br />
                <asp:GridView ID="gvNamNay" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="SODONHANG" ForeColor="#333333" GridLines="None" style="text-align: center">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" />
                        <asp:BoundField DataField="TENKHACHHANG" HeaderText="Tên Khách Hàng" />
                        <asp:BoundField DataField="NGAYDATHANG" HeaderText="Ngày Đặt Hàng" />
                        <asp:BoundField DataField="NGAYGIAO" HeaderText="Ngày Giao" />
                        <asp:BoundField DataField="TRIGIA" HeaderText="Trị Giá" />
                        <asp:BoundField DataField="TENTINHTRANG" HeaderText="Tình Trạng" />
                        <asp:BoundField DataField="TINHTRANGTHANHTOAN" HeaderText="Thanh Toán" />
                        <asp:BoundField DataField="TENNGUOINHAN" HeaderText="Tên Người Nhận" />
                        <asp:BoundField DataField="DIACHINGUOINHAN" HeaderText="Địa Chỉ Người Nhận" />
                        <asp:BoundField DataField="DIENTHOAINGUOINHAN" HeaderText="SĐT Người Nhận" />
                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi Chú" />
                        <asp:BoundField DataField="TENHINHTHUCGH" HeaderText="HT Giao Hàng" />
                        <asp:BoundField DataField="TENHINHTHUCTT" HeaderText="HT Thanh Toán" />
                        <asp:BoundField HeaderText="Chi Tiết" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <br />
                <asp:Label ID="lblNamNay" runat="server"></asp:Label>
            </asp:View>
        </asp:MultiView>
</asp:Content>

