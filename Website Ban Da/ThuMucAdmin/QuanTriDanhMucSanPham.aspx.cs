﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_QuanTriDanhMucSanPham : System.Web.UI.Page
{
    SanPhamBusiness sp = new SanPhamBusiness();
    void CapNhat()
    {
        gvSanPham.DataSource = sp.LoadSanPham();
        gvSanPham.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }
        if (!IsPostBack)
        {
            CapNhat();
        }
    }
    protected void gvSanPham_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int dong = e.RowIndex;
        sp.XoaSanPham(gvSanPham.Rows[dong].Cells[0].Text.ToString());
        CapNhat();
    }
    protected void gvSanPham_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvSanPham.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvSanPham_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvSanPham.EditIndex = -1;
        CapNhat();
    }
    protected void gvSanPham_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        sp.SuaSanPham((gvSanPham.DataKeys[e.RowIndex].Value.ToString()), (gvSanPham.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[7].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[8].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[9].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[10].Controls[0] as TextBox).Text, (gvSanPham.Rows[e.RowIndex].Cells[11].Controls[0] as TextBox).Text);
        gvSanPham.EditIndex = -1;
        CapNhat();
    }
    protected void gvSanPham_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSanPham.PageIndex = e.NewPageIndex;
        CapNhat();
    }
}