﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="QuanLyKhachHang.aspx.cs" Inherits="ThuMucAdmin_QuanLyKhachHang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewkhachhang {
            margin:0px auto;
        }
        .auto-style2 {
            height: 18px;
            width: 253px;
        }
        .auto-style3 {
        }
        .auto-style4 {
            height: 18px;
            text-align: left;
        }
        .auto-style5 {
            width: 253px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:GridView ID="gvKhachHang" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="MAKHACHHANG" ForeColor="#333333" GridLines="None" OnPageIndexChanging="gvKhachHang_PageIndexChanging" OnRowCancelingEdit="gvKhachHang_RowCancelingEdit" OnRowCommand="gvKhachHang_RowCommand" OnRowDeleting="gvKhachHang_RowDeleting" OnRowEditing="gvKhachHang_RowEditing" OnRowUpdating="gvKhachHang_RowUpdating" style="text-align: center">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="MAKHACHHANG" HeaderText="Mã Khách Hàng" ReadOnly="True" />
                    <asp:BoundField DataField="TENKHACHHANG" HeaderText="Tên Khách Hàng" />
                    <asp:BoundField DataField="DIENTHOAI" HeaderText="Điện Thoại" />
                    <asp:BoundField DataField="NGAYSINH" HeaderText="Ngày Sinh" />
                    <asp:BoundField DataField="GIOITINH" HeaderText="Giới Tính" />
                    <asp:BoundField DataField="TENDANGNHAP" HeaderText="Tên Đăng Nhập" />
                    <asp:BoundField DataField="MATKHAU" HeaderText="Mật Khẩu" />
                    <asp:BoundField DataField="DIACHI" HeaderText="Địa Chỉ" />
                    <asp:BoundField DataField="EMAIL" HeaderText="Email" />
                    <asp:BoundField DataField="DADUYET" HeaderText="Đã Duyệt" />
                    <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" DeleteImageUrl="~/Image/delete.png" EditImageUrl="~/Image/edit.png" HeaderText="Xóa Sửa" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" />
                    <asp:ButtonField ButtonType="Image" CommandName="ThemKhachHang" HeaderText="Thêm" ImageUrl="~/Image/add.png" Text="Thêm" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table class="viewkhachhang" align="center">
                <tr>
                    <td colspan="2" style="text-align: center; font-weight: 700; font-size: large">THÊM KHÁCH HÀNG</td>
                </tr>
                <tr>
                    <td class="auto-style3">Tên Khách Hàng</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtTenKhachHang" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Điện Thoại</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtDienThoai" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Ngày Sinh</td>
                    <td class="auto-style5">Ngày
                        <asp:TextBox ID="txtNgay" runat="server" Width="30px"></asp:TextBox>
                        &nbsp; Tháng
                        <asp:TextBox ID="txtThang" runat="server" Width="30px"></asp:TextBox>
                        &nbsp; Năm
                        <asp:TextBox ID="txtNam" runat="server" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Giới Tính</td>
                    <td class="auto-style5">
                        <asp:DropDownList ID="ddlGioiTinh" runat="server" Width="200px">
                            <asp:ListItem Value="1">Nam</asp:ListItem>
                            <asp:ListItem Value="0">Nữ</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">Địa Chỉ</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtDiaChi" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Email</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtEmail" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Đã Duyệt</td>
                    <td class="auto-style5">
                        <asp:DropDownList ID="ddlDaDuyet" runat="server" Width="250px">
                            <asp:ListItem Value="1">Đã Duyệt</asp:ListItem>
                            <asp:ListItem Value="0">Chưa Duyệt</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Tên Đăng Nhập</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtTenDangNhap" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Mật Khẩu</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtMatKhau" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center">
                        <asp:Button ID="btnThem" runat="server" CssClass="nuthemkhachhang" OnClick="btnThem_Click" Text="THÊM" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

