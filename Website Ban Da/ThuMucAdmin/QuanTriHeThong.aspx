﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="QuanTriHeThong.aspx.cs" Inherits="ThuMucAdmin_QuanTriHeThong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .viewadmin
        {
            margin:0px auto;
        }
        .auto-style2 {
            height: 19px;
            text-align: center;
            font-weight: 700;
            color: #FF6600;
        }
        .auto-style3 {
            height: 22px;
        }
        .nutthemadmin
        {
            height:30px;
            width:100px;
            background:#ff6a00;
            color:white;
        }
        .auto-style4 {
            width: 302px;
        }
        .auto-style5 {
            height: 22px;
            width: 302px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:GridView ID="gvAdmin" runat="server" AutoGenerateColumns="False" DataKeyNames="MAADMIN" OnPageIndexChanging="gvAdmin_PageIndexChanging" OnRowCancelingEdit="gvAdmin_RowCancelingEdit" OnRowCommand="gvAdmin_RowCommand" OnRowDeleting="gvAdmin_RowDeleting" OnRowUpdating="gvAdmin_RowUpdating" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowEditing="gvAdmin_RowEditing">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="MAADMIN" HeaderText="Mã Admin" ReadOnly="True" />
                    <asp:BoundField DataField="TENADMIN" HeaderText="Tên Admin" />
                    <asp:BoundField DataField="DIENTHOAIADMIN" HeaderText="Điện Thoại" />
                    <asp:BoundField DataField="NGAYSINHADMIN" HeaderText="Ngày Sinh" />
                    <asp:BoundField DataField="GIOITINHADMIN" HeaderText="Giới Tính" />
                    <asp:BoundField DataField="TENDANGNHAPADMIN" HeaderText="Tên Đăng Nhập" />
                    <asp:BoundField DataField="MATKHAUADMIN" HeaderText="Mật Khẩu" />
                    <asp:BoundField DataField="DIACHIADMIN" HeaderText="Địa Chỉ" />
                    <asp:BoundField DataField="EMAILADMIN" HeaderText="Email" />
                    <asp:BoundField DataField="QUYENHANADMIN" HeaderText="Quyền Hạn" ReadOnly="True" />
                    <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" DeleteImageUrl="~/Image/delete.png" EditImageUrl="~/Image/edit.png" HeaderText="Xóa Sửa" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" />
                    <asp:ButtonField ButtonType="Image" CommandName="ThemAdmin" HeaderText="Thêm" ImageUrl="~/Image/add.png" Text="Button" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <br />
            
            <br />
            <asp:LinkButton ID="lbtnThemAdmin" runat="server" OnClick="lbtnThemAdmin_Click">Thêm Admin Dưới Quyền</asp:LinkButton>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table class="viewadmin" align="center">
                <tr>
                    <td class="auto-style2" colspan="2">THÊM ADMIN</td>
                </tr>
                <tr>
                    <td>Tên Admin</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtTenAdmin" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Điện Thoại Admin</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtDienThoai" runat="server" Width="300px" TextMode="Number"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Ngày Sinh Admin</td>
                    <td class="auto-style4">
                        Ngày
                        <asp:TextBox ID="txtNgay" runat="server" Width="50px"></asp:TextBox>
                        &nbsp;Tháng
                        <asp:TextBox ID="txtThang" runat="server" Width="50px"></asp:TextBox>
                        Năm
                        <asp:TextBox ID="txtNam" runat="server" Width="64px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Giới Tính Admin</td>
                    <td class="auto-style4">
                        <asp:DropDownList ID="ddlGioiTinh" runat="server" Width="300px">
                            <asp:ListItem Value="True">Nam</asp:ListItem>
                            <asp:ListItem Value="False">Nữ</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Tên Đăng Nhập Admin</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtTenDangNhap" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Mật Khẩu Admin</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtMatKhauAdmin" runat="server" Width="300px" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Địa Chỉ Admin</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtDiaChi" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Email Admin</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtEmail" runat="server" Width="300px" TextMode="Email"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Quyền Hạn</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtQuyenHan" runat="server" Width="300px" TextMode="Number"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <asp:Button ID="btnThem" runat="server" CssClass="nutthemadmin" Text="THÊM" OnClick="btnThem_Click" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <asp:Label ID="lblThongBao" runat="server" Text="Label"></asp:Label>
</asp:Content>

