﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_QuanLyTaiChinh : System.Web.UI.Page
{
    DonDatHangBusiness ddhbn = new DonDatHangBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }
        if (!IsPostBack)
        {
            ddlDoanhThu.SelectedIndex = 0;
            mtvQuanLyTaiChinh.ActiveViewIndex = 0;
            gvHomNay.DataSource = ddhbn.LoadDonHangHomNay();
            gvHomNay.DataBind();
            double tongtien = 0;
            foreach (GridViewRow gvr in gvHomNay.Rows)
            {
                tongtien += Convert.ToDouble(gvr.Cells[4].Text);
            }
            lblHomNay.Text = "Tổng Doanh Thu Ngày Hôm Nay Là: " + tongtien.ToString() + " VNĐ";
        } 
    }
    protected void ddlDoanhThu_SelectedIndexChanged(object sender, EventArgs e)
    {
        double tongtien = 0;
        if (ddlDoanhThu.SelectedIndex == 0)
        {
            mtvQuanLyTaiChinh.ActiveViewIndex = 0;
            gvHomNay.DataSource = ddhbn.LoadDonHangHomNay();
            gvHomNay.DataBind();
            foreach (GridViewRow gvr in gvHomNay.Rows)
            {
                tongtien += Convert.ToDouble(gvr.Cells[4].Text);
            }
            lblHomNay.Text = "Tổng Doanh Thu Ngày Hôm Nay Là: " + tongtien.ToString() + " VNĐ";
        }
        if (ddlDoanhThu.SelectedIndex == 1)
        {
            mtvQuanLyTaiChinh.ActiveViewIndex = 1;
            gvThangNay.DataSource = ddhbn.LoadDonHangThangNay();
            gvThangNay.DataBind();
            foreach (GridViewRow gvr in gvThangNay.Rows)
            {
                tongtien += Convert.ToDouble(gvr.Cells[4].Text);
            }
            lblThangNay.Text = "Tổng Doanh Thu Tháng Nay Là: " + tongtien.ToString() + " VNĐ";
        }
        if (ddlDoanhThu.SelectedIndex ==2)
        {
            mtvQuanLyTaiChinh.ActiveViewIndex = 2;
            gvNamNay.DataSource = ddhbn.LoadDonHangNamNay();
            gvNamNay.DataBind();
            foreach (GridViewRow gvr in gvNamNay.Rows)
            {
                tongtien += Convert.ToDouble(gvr.Cells[4].Text);
            }
            lblNamNay.Text = "Tổng Doanh Thu Năm Nay Là: " + tongtien.ToString() + " VNĐ";
        }
    }
}