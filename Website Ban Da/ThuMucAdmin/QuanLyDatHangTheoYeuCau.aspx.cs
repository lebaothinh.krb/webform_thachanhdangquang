﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_QuanLyDatHangTheoYeuCau : System.Web.UI.Page
{
    DatHangTheoYeuCauBusiness dhtyc = new DatHangTheoYeuCauBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }
        lblThongBao.Text = "";
        if (!IsPostBack)
        {
            CapNhat();
        }
    }
    public void CapNhat()
    {
        gvDatHang.DataSource = dhtyc.LoadDatHang();
        gvDatHang.DataBind();
        if (gvDatHang.Rows.Count==0)
        {
            lblThongBao.Text = "Chưa Có Đơn Đặt Hàng!";
        }
    }
    protected void gvDatHang_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDatHang.PageIndex = e.NewPageIndex;
        CapNhat();
    }
    protected void gvDatHang_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDatHang.EditIndex = -1;
        CapNhat();
    }
    protected void gvDatHang_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName =="ThemDatHang")
        {
            txtKichThuoc.Text = "";
            txtMaKhachHang.Text = "";
            txtNoiDung.Text = "";
            txtSoLuong.Text = "";
            MultiView1.ActiveViewIndex = 1;
        }
    }
    protected void gvDatHang_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        dhtyc.XoaDatHang(gvDatHang.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
        Response.Write("<script>alert('Xóa Thành Công!')</script>");
    }
    protected void gvDatHang_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDatHang.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvDatHang_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        dhtyc.CapNhatDatHang(gvDatHang.DataKeys[e.RowIndex].Value.ToString(), (gvDatHang.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text, (gvDatHang.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text, (gvDatHang.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text, (gvDatHang.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvDatHang.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvDatHang.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text);
        gvDatHang.EditIndex = -1;
        CapNhat();
        Response.Write("<script>alert('Cập Nhật Thành Công!')</script>");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        dhtyc.ThemDatHang(txtMaKhachHang.Text, txtKichThuoc.Text, FileUpload1.FileName, txtNoiDung.Text, (rdChungtoi.Checked) ? rdChungtoi.Text : rdCuaBan.Text, txtSoLuong.Text);
        FileUpload1.SaveAs(MapPath("~/Image/dathangtheoyeucau/" + FileUpload1.FileName));
        MultiView1.ActiveViewIndex = 0;
        CapNhat();
        Response.Write("<script>alert('Thêm Thành Công!')</script>");
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        txtKichThuoc.Text = "";
        txtMaKhachHang.Text = "";
        txtNoiDung.Text = "";
        txtSoLuong.Text = "";
        MultiView1.ActiveViewIndex = 1;
    }
}