﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_DangNhapAdmin : System.Web.UI.Page
{
    Admin admin = new Admin();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DataTable dt = admin.LoadAdmin();
        foreach(DataRow dr in dt.Rows)
        {
            if (dr["TENDANGNHAPADMIN"].ToString()==txtTaiKhoan.Text && dr["MATKHAUADMIN"].ToString()==txtMatKhau.Text)
            {
                Session["QuyenHanAdmin"] = dr["QUYENHANADMIN"].ToString();
                Session["ADMINDN"] = dr["TENADMIN"].ToString();
                Response.Redirect("~/ThuMucAdmin/QuanTriHeThong.aspx");
            }
        }
    }
}