﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="QuanLyDonDatHang.aspx.cs" Inherits="ThuMucAdmin_QuanLyDonDatHang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .nutdonhang
        {
            width:200px;
            height:40px;
            background:#ff6a00;
            color:white;
            margin-bottom:20px;
            font-weight:bold;
        }
        .nutdonhang:hover
        {
            background:#EEEDEA;
            border:2px solid #ff6a00;
            color:#ff6a00;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Button CssClass="nutdonhang" ID="btnChuaxacnhan" runat="server" Text="Chưa Xác Nhận" OnClick="btnChuaxacnhan_Click" />
        <asp:Button CssClass="nutdonhang" ID="btnChoGiao" runat="server" Text="Chờ Được Giao" OnClick="btnChoGiao_Click" />
        <asp:Button CssClass="nutdonhang" ID="btnDaGiao" runat="server" Text="Đã Giao" OnClick="btnDaGiao_Click" />
        <asp:Button CssClass="nutdonhang" ID="btnDaNhanHang" runat="server" Text="Đã Nhận Hàng" OnClick="btnDaNhanHang_Click" />
        <asp:Button ID="btnDoiTra" runat="server" CssClass="nutdonhang" OnClick="btnDoiTra_Click" Text="Đổi Trả" />
    <br />
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="View1" runat="server">
                <asp:GridView ID="gvChoXacNhan" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="SODONHANG" OnRowCancelingEdit="gvChoXacNhan_RowCancelingEdit" OnRowCommand="gvChoXacNhan_RowCommand" OnRowDeleting="gvChoXacNhan_RowDeleting" OnRowEditing="gvChoXacNhan_RowEditing" OnRowUpdating="gvChoXacNhan_RowUpdating" style="text-align: center" OnPageIndexChanging="gvChoXacNhan_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="MAKHACHHANG" HeaderText="Mã Khách Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYDATHANG" HeaderText="Ngày Đặt Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYGIAO" HeaderText="Ngày Giao" />
                        <asp:BoundField DataField="TRIGIA" HeaderText="Trị Giá" />
                        <asp:BoundField DataField="TINHTRANGDONHANG" HeaderText="Tình Trạng" />
                        <asp:BoundField DataField="TINHTRANGTHANHTOAN" HeaderText="Thanh Toán" />
                        <asp:BoundField DataField="TENNGUOINHAN" HeaderText="Tên Người Nhận" />
                        <asp:BoundField DataField="DIACHINGUOINHAN" HeaderText="Địa Chỉ Người Nhận" />
                        <asp:BoundField DataField="DIENTHOAINGUOINHAN" HeaderText="SĐT Người Nhận" />
                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi Chú" />
                        <asp:BoundField DataField="HINHTHUCGIAOHANG" HeaderText="HT Giao Hàng" />
                        <asp:BoundField DataField="HINHTHUCTHANHTOAN" HeaderText="HT Thanh Toán" />
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" InsertText="" NewText="" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" UpdateText="Cập Nhật" />
                        <asp:HyperLinkField DataNavigateUrlFields="SODONHANG" DataNavigateUrlFormatString="~\ThuMucAdmin\DonHangChiTiet.aspx?SoDonHang={0}" HeaderText="Chi Tiết" Text="Chi Tiết" />
                        <asp:TemplateField HeaderText="Duyệt" ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SODONHANG") %>' CommandName="Duyet" ImageUrl="~/Image/checked.png" Text="" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:GridView ID="gvChoGiaoHang" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="SODONHANG" OnPageIndexChanging="gvChoGiaoHang_PageIndexChanging" OnRowCancelingEdit="gvChoGiaoHang_RowCancelingEdit" OnRowCommand="gvChoGiaoHang_RowCommand" OnRowDeleting="gvChoGiaoHang_RowDeleting" OnRowEditing="gvChoGiaoHang_RowEditing" OnRowUpdating="gvChoGiaoHang_RowUpdating" style="text-align: center">
                    <Columns>
                        <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="MAKHACHHANG" HeaderText="Mã Khách Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYDATHANG" HeaderText="Ngày Đặt Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYGIAO" HeaderText="Ngày Giao" />
                        <asp:BoundField DataField="TRIGIA" HeaderText="Trị Giá" />
                        <asp:BoundField DataField="TINHTRANGDONHANG" HeaderText="Tình Trạng" />
                        <asp:BoundField DataField="TINHTRANGTHANHTOAN" HeaderText="Thanh Toán" />
                        <asp:BoundField DataField="TENNGUOINHAN" HeaderText="Tên Người Nhận" />
                        <asp:BoundField DataField="DIACHINGUOINHAN" HeaderText="Địa Chỉ Người Nhận" />
                        <asp:BoundField DataField="DIENTHOAINGUOINHAN" HeaderText="SĐT Người Nhận" />
                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi Chú" />
                        <asp:BoundField DataField="HINHTHUCGIAOHANG" HeaderText="HT Giao Hàng" />
                        <asp:BoundField DataField="HINHTHUCTHANHTOAN" HeaderText="HT Thanh Toán" />
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" InsertText="" NewText="" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" UpdateText="Cập Nhật" />
                        <asp:HyperLinkField DataNavigateUrlFields="SODONHANG" DataNavigateUrlFormatString="~\ThuMucAdmin\DonHangChiTiet.aspx?SoDonHang={0}" HeaderText="Chi Tiết" Text="Chi Tiết" />
                        <asp:TemplateField HeaderText="Duyệt" ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SODONHANG") %>' CommandName="Duyet" ImageUrl="~/Image/checked.png" Text="" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
            </asp:View>
            <asp:View ID="View3" runat="server">
                <asp:GridView ID="gvDaGiao" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="SODONHANG" OnPageIndexChanging="gvDaGiao_PageIndexChanging" OnRowCancelingEdit="gvDaGiao_RowCancelingEdit" OnRowCommand="gvDaGiao_RowCommand" OnRowDeleting="gvDaGiao_RowDeleting" OnRowEditing="gvDaGiao_RowEditing" OnRowUpdating="gvDaGiao_RowUpdating" style="text-align: center">
                    <Columns>
                        <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="MAKHACHHANG" HeaderText="Mã Khách Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYDATHANG" HeaderText="Ngày Đặt Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYGIAO" HeaderText="Ngày Giao" />
                        <asp:BoundField DataField="TRIGIA" HeaderText="Trị Giá" />
                        <asp:BoundField DataField="TINHTRANGDONHANG" HeaderText="Tình Trạng" />
                        <asp:BoundField DataField="TINHTRANGTHANHTOAN" HeaderText="Thanh Toán" />
                        <asp:BoundField DataField="TENNGUOINHAN" HeaderText="Tên Người Nhận" />
                        <asp:BoundField DataField="DIACHINGUOINHAN" HeaderText="Địa Chỉ Người Nhận" />
                        <asp:BoundField DataField="DIENTHOAINGUOINHAN" HeaderText="SĐT Người Nhận" />
                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi Chú" />
                        <asp:BoundField DataField="HINHTHUCGIAOHANG" HeaderText="HT Giao Hàng" />
                        <asp:BoundField DataField="HINHTHUCTHANHTOAN" HeaderText="HT Thanh Toán" />
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" InsertText="" NewText="" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" UpdateText="Cập Nhật" />
                        <asp:HyperLinkField DataNavigateUrlFields="SODONHANG" DataNavigateUrlFormatString="~\ThuMucAdmin\DonHangChiTiet.aspx?SoDonHang={0}" HeaderText="Chi Tiết" Text="Chi Tiết" />
                        <asp:TemplateField HeaderText="Duyệt" ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SODONHANG") %>' CommandName="Duyet" ImageUrl="~/Image/checked.png" Text="" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
            </asp:View>
            <asp:View ID="View4" runat="server">
                <asp:GridView ID="gvGiaoThanhCong" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="SODONHANG" OnPageIndexChanging="gvGiaoThanhCong_PageIndexChanging" OnRowCancelingEdit="gvGiaoThanhCong_RowCancelingEdit" OnRowCommand="gvGiaoThanhCong_RowCommand" OnRowDeleting="gvGiaoThanhCong_RowDeleting" OnRowEditing="gvGiaoThanhCong_RowEditing" OnRowUpdating="gvGiaoThanhCong_RowUpdating" style="text-align: center">
                    <Columns>
                        <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="MAKHACHHANG" HeaderText="Mã Khách Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYDATHANG" HeaderText="Ngày Đặt Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYGIAO" HeaderText="Ngày Giao" />
                        <asp:BoundField DataField="TRIGIA" HeaderText="Trị Giá" />
                        <asp:BoundField DataField="TINHTRANGDONHANG" HeaderText="Tình Trạng" />
                        <asp:BoundField DataField="TINHTRANGTHANHTOAN" HeaderText="Thanh Toán" />
                        <asp:BoundField DataField="TENNGUOINHAN" HeaderText="Tên Người Nhận" />
                        <asp:BoundField DataField="DIACHINGUOINHAN" HeaderText="Địa Chỉ Người Nhận" />
                        <asp:BoundField DataField="DIENTHOAINGUOINHAN" HeaderText="SĐT Người Nhận" />
                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi Chú" />
                        <asp:BoundField DataField="HINHTHUCGIAOHANG" HeaderText="HT Giao Hàng" />
                        <asp:BoundField DataField="HINHTHUCTHANHTOAN" HeaderText="HT Thanh Toán" />
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" InsertText="" NewText="" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" UpdateText="Cập Nhật" />
                        <asp:HyperLinkField DataNavigateUrlFields="SODONHANG" DataNavigateUrlFormatString="~\ThuMucAdmin\DonHangChiTiet.aspx?SoDonHang={0}" HeaderText="Chi Tiết" Text="Chi Tiết" />
                        <asp:TemplateField HeaderText="Đổi Trả" ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SODONHANG") %>' CommandName="DoiTra" ImageUrl="~/Image/checked.png" Text="" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
            </asp:View>
            <asp:View ID="View5" runat="server">
                <asp:GridView ID="gvDoiTra" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="SODONHANG" OnRowCancelingEdit="gvDoiTra_RowCancelingEdit" OnRowCommand="gvDoiTra_RowCommand" OnRowDeleting="gvDoiTra_RowDeleting" OnRowEditing="gvDoiTra_RowEditing" OnRowUpdating="gvDoiTra_RowUpdating" style="text-align: center">
                    <Columns>
                        <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="MAKHACHHANG" HeaderText="Mã Khách Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYDATHANG" HeaderText="Ngày Đặt Hàng" ReadOnly="True" />
                        <asp:BoundField DataField="NGAYGIAO" HeaderText="Ngày Giao" />
                        <asp:BoundField DataField="TRIGIA" HeaderText="Trị Giá" />
                        <asp:BoundField DataField="TINHTRANGDONHANG" HeaderText="Tình Trạng" />
                        <asp:BoundField DataField="TINHTRANGTHANHTOAN" HeaderText="Thanh Toán" />
                        <asp:BoundField DataField="TENNGUOINHAN" HeaderText="Tên Người Nhận" />
                        <asp:BoundField DataField="DIACHINGUOINHAN" HeaderText="Địa Chỉ Người Nhận" />
                        <asp:BoundField DataField="DIENTHOAINGUOINHAN" HeaderText="SĐT Người Nhận" />
                        <asp:BoundField DataField="GHICHU" HeaderText="Ghi Chú" />
                        <asp:BoundField DataField="HINHTHUCGIAOHANG" HeaderText="HT Giao Hàng" />
                        <asp:BoundField DataField="HINHTHUCTHANHTOAN" HeaderText="HT Thanh Toán" />
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" InsertText="" NewText="" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" UpdateText="Cập Nhật" />
                        <asp:HyperLinkField DataNavigateUrlFields="SODONHANG" DataNavigateUrlFormatString="~\ThuMucAdmin\DonHangChiTiet.aspx?SoDonHang={0}" HeaderText="Chi Tiết" Text="Chi Tiết" />
                        <asp:TemplateField HeaderText="Duyệt" ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SODONHANG") %>' CommandName="Duyet" ImageUrl="~/Image/checked.png" Text="Button" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>
            </asp:View>
        </asp:MultiView>
</asp:Content>

