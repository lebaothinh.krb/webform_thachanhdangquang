﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="QuanLyDatHangTheoYeuCau.aspx.cs" Inherits="ThuMucAdmin_QuanLyDatHangTheoYeuCau" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            width: 302px;
        }
        .auto-style4 {
            text-align: center;
            width: 302px;
        }
        .auto-style5 {
            height: 19px;
        }
        .auto-style6 {
            text-align: center;
            width: 302px;
            height: 19px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:GridView ID="gvDatHang" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="MADATHANG" ForeColor="#333333" GridLines="None" OnPageIndexChanging="gvDatHang_PageIndexChanging" OnRowCancelingEdit="gvDatHang_RowCancelingEdit" OnRowCommand="gvDatHang_RowCommand" OnRowDeleting="gvDatHang_RowDeleting" OnRowEditing="gvDatHang_RowEditing" OnRowUpdating="gvDatHang_RowUpdating">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="MADATHANG" HeaderText="Mã Đặt Hàng" />
                    <asp:BoundField DataField="MAKHACHHANG" HeaderText="Mã Khách Hàng" />
                    <asp:BoundField DataField="KICHTHUOC" HeaderText="Kích Thước" />
                    <asp:BoundField DataField="HINHANH" HeaderText="Hình Ảnh" />
                    <asp:BoundField DataField="NOIDUNG" HeaderText="Nội Dung" />
                    <asp:BoundField DataField="VATLIEU" HeaderText="Vật Liệu" />
                    <asp:BoundField DataField="SOLUONG" HeaderText="Số Lượng" />
                    <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" DeleteImageUrl="~/Image/delete.png" EditImageUrl="~/Image/edit.png" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" />
                    <asp:ButtonField ButtonType="Image" CommandName="ThemDatHang" HeaderText="Thêm" ImageUrl="~/Image/add.png" Text="Button" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <br />
            <asp:Button ID="btnThem" runat="server" OnClick="btnThem_Click" Text="Thêm Đơn Đặt Hàng" />
        </asp:View>
        <asp:View ID="View2" runat="server">

            <table align="center" class="viewsanpham">
                <tr>
                    <td>Kích Thước (cm):</td>
                    <td class="auto-style3">
                        <asp:TextBox ID="txtKichThuoc" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Hình Ảnh:</td>
                    <td class="auto-style3">
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" />
                    </td>
                </tr>
                <tr>
                    <td>Nội Dung</td>
                    <td class="auto-style3">
                        <asp:TextBox ID="txtNoiDung" runat="server" Height="58px" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <asp:RadioButton ID="rdChungtoi" runat="server" Checked="True" GroupName="VATLIEU" Text="Vật Liệu Của Chúng Tôi" ValidationGroup="vatlieu" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rdCuaBan" runat="server" GroupName="VATLIEU" Text="Vật Liệu Của Bạn" ValidationGroup="vatlieu" />
                    </td>
                </tr>
                <tr>
                    <td>Số Lượng</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtSoLuong" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">Mã Khách Hàng</td>
                    <td class="auto-style6">
                        <asp:TextBox ID="txtMaKhachHang" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <asp:Button ID="btnDatHang" runat="server" BorderStyle="None" CssClass="nut" OnClick="Button1_Click" Text="ĐẶT HÀNG" />
                    </td>
                </tr>
            </table>

        </asp:View>
    </asp:MultiView>
    <asp:Label ID="lblThongBao" runat="server" Text="Label"></asp:Label>
</asp:Content>

