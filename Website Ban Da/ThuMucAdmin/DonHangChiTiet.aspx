﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="DonHangChiTiet.aspx.cs" Inherits="ThuMucAdmin_DonHangChiTiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:GridView ID="gvChitiet" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="SODONHANG" OnRowCommand="gvChitiet_RowCommand" OnRowCancelingEdit="gvChitiet_RowCancelingEdit" OnRowDeleting="gvChitiet_RowDeleting" OnRowEditing="gvChitiet_RowEditing" OnRowUpdating="gvChitiet_RowUpdating">
                <Columns>
                    <asp:BoundField DataField="SODONHANG" HeaderText="Số Đơn Hàng" ReadOnly="True" />
                    <asp:BoundField DataField="MASANPHAM" HeaderText="Mã Sản Phẩm" ReadOnly="True" />
                    <asp:BoundField DataField="SOLUONG" HeaderText="Số Lượng" />
                    <asp:BoundField DataField="DONGIA" HeaderText="Đơn Giá" ReadOnly="True" />
                    <asp:BoundField DataField="THANHTIEN" HeaderText="Thành Tiền" ReadOnly="True" />
                    <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" InsertText="" NewText="" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" UpdateText="Cập Nhật" />
                    <asp:ButtonField ButtonType="Image" CommandName="ThemCt" HeaderText="Thêm" ImageUrl="~/Image/add.png" />
                </Columns>
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#330099" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <SortedAscendingCellStyle BackColor="#FEFCEB" />
                <SortedAscendingHeaderStyle BackColor="#AF0101" />
                <SortedDescendingCellStyle BackColor="#F6F0C0" />
                <SortedDescendingHeaderStyle BackColor="#7E0000" />
            </asp:GridView>
            <br />
            <asp:Label ID="lblTongThanhTien" runat="server"></asp:Label>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table class="auto-style1">
                <tr>
                    <td colspan="2" style="font-size: large; font-weight: 700; text-align: center">THÊM SẢN PHẨM</td>
                </tr>
                <tr>
                    <td class="auto-style2">Mã Sản Phẩm</td>
                    <td>
                        <asp:DropDownList ID="ddlMaSanPham" runat="server" OnSelectedIndexChanged="ddlMaSanPham_SelectedIndexChanged" Width="300px" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Tên Sản Phẩm</td>
                    <td>
                        <asp:TextBox ID="txtTenSanPham" runat="server" Enabled="False" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Số Lượng</td>
                    <td>
                        <asp:TextBox ID="txtSoLuong" runat="server" TextMode="Number" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center">
                        <asp:Button ID="btnThem" runat="server" BackColor="#0099FF" Height="30px" OnClick="btnThem_Click" Text="Thêm" Width="100px" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <br />
    <br />
    
</asp:Content>

