﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="QuanLyChuDeLoaiTheLoai.aspx.cs" Inherits="ThuMucAdmin_QuanLyChuDeLoaiTheLoai" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .nutdonhang
        {
            background:#ff6a00;
            color:white;
            margin-bottom:20px;
            font-weight:bold;
            width:150px;
            height:30px;
        }
        .nutdonhang:hover
        {
            background:#EEEDEA;
            border:2px solid #ff6a00;
            color:#ff6a00;
        }
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            font-size: large;
        }
        .auto-style5 {
            width: 243px;
        }
        .auto-style6 {
            width: 243px;
            text-align: right;
        }
        .auto-style7 {
            height: 64px;
        }
        .auto-style8 {
            width: 243px;
            text-align: right;
            height: 49px;
        }
        .auto-style9 {
            height: 49px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Button CssClass="nutdonhang" ID="btnQuanLyChuDe" runat="server" Text="Quản Lý Chủ Đề" OnClick="QuanLyChuDe_Click" />
        <asp:Button CssClass="nutdonhang" ID="btnQuanLyLoai" runat="server" Text="Quản Lý Loại" OnClick="QuanLyLoai_Click" />
        <asp:Button CssClass="nutdonhang" ID="btnQuanLyTheLoai" runat="server" Text="Quản Lý Thể Loại" OnClick="QuanLyTheLoai_Click" />
        
        <br />
        <asp:MultiView ID="mtvQuanLy" runat="server" ActiveViewIndex="0">
            <asp:View ID="ChuDe" runat="server">
                <asp:GridView ID="gvChuDe" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="MACHUDE" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="gvChuDe_RowCancelingEdit" OnRowCommand="gvChuDe_RowCommand" OnRowDeleting="gvChuDe_RowDeleting" OnRowEditing="gvChuDe_RowEditing" OnRowUpdating="gvChuDe_RowUpdating">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="MACHUDE" HeaderText="Mã Chủ Đề" ReadOnly="True" />
                        <asp:BoundField DataField="TENCHUDE" HeaderText="Tên Chủ Đề" />
                        <asp:BoundField DataField="MOTACHUDE" HeaderText="Mô Tả Chủ Đề" />
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" NewText="" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/Image/update.png" UpdateText="Cập Nhật" />
                        <asp:ButtonField ButtonType="Image" CommandName="ThemChuDe" HeaderText="Thêm" ImageUrl="~/Image/add.png" Text="Thêm" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </asp:View>
            <asp:View ID="Loai" runat="server">
                <asp:GridView ID="gvLoai" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="MALOAI" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="gvLoai_RowCancelingEdit" OnRowCommand="gvLoai_RowCommand" OnRowDeleting="gvLoai_RowDeleting" OnRowEditing="gvLoai_RowEditing" OnRowUpdating="gvLoai_RowUpdating">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="MALOAI" HeaderText="Mã Loại" ReadOnly="True" />
                        <asp:BoundField DataField="TENLOAI" HeaderText="Tên Loại" />
                        <asp:BoundField DataField="MOTALOAI" HeaderText="Mô Tả Loại" />
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" NewText="" ShowDeleteButton="True" ShowEditButton="True" />
                        <asp:ButtonField ButtonType="Image" CommandName="ThemLoai" HeaderText="Thêm" ImageUrl="~/Image/add.png" Text="Thêm" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </asp:View>
            <asp:View ID="TheLoai" runat="server">
                <asp:GridView ID="gvTheLoai" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="MATHELOAI" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="gvTheLoai_RowCancelingEdit" OnRowCommand="gvTheLoai_RowCommand" OnRowDeleting="gvTheLoai_RowDeleting" OnRowEditing="gvTheLoai_RowEditing" OnRowUpdating="gvTheLoai_RowUpdating">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="MATHELOAI" HeaderText="Mã Thể Loại" ReadOnly="True" />
                        <asp:BoundField DataField="TENTHELOAI" HeaderText="Tên Thể Loại" />
                        <asp:BoundField DataField="MOTATHELOAI" HeaderText="Mô Tả Thể Loại" />
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/Image/cancel.png" CancelText="Thôi" DeleteImageUrl="~/Image/delete.png" DeleteText="Xóa" EditImageUrl="~/Image/edit.png" EditText="Sửa" HeaderText="Xóa Sửa" NewText="" ShowDeleteButton="True" ShowEditButton="True" />
                        <asp:ButtonField ButtonType="Image" CommandName="ThemTheLoai" HeaderText="Thêm" ImageUrl="~/Image/add.png" Text="Thêm" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </asp:View>
            <asp:View ID="ThemChuDe" runat="server">
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2" colspan="2" style="text-align: center"><strong>THÊM MỚI CHỦ ĐỀ</strong></td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Tên Chủ Đề</td>
                        <td>
                            <asp:TextBox ID="txtTenChuDe" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Mô Tả Chủ Đề</td>
                        <td class="auto-style9">
                            <asp:TextBox ID="txtMoTaChuDe" runat="server" Height="39px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style7" colspan="2" style="text-align: center">
                            <asp:Button ID="btnThemChuDe" runat="server" CssClass="nutdonhang" OnClick="Button1_Click" Text="THÊM" Width="126px" />
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="ThemLoai" runat="server">
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2" colspan="2" style="text-align: center"><strong>THÊM MỚI LOẠI</strong></td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Tên Loại</td>
                        <td>
                            <asp:TextBox ID="txtTenLoai" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Mô Tả Loại</td>
                        <td class="auto-style9">
                            <asp:TextBox ID="txtMoTaLoai" runat="server" Height="39px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style7" colspan="2" style="text-align: center">
                            <asp:Button ID="btnThemLoai" runat="server" CssClass="nutdonhang" OnClick="Button1_Click1" Text="THÊM" Width="126px" />
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="ThemTheLoai" runat="server">
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2" colspan="2" style="text-align: center"><strong>THÊM MỚI THỂ LOẠI</strong></td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Tên Thể Loại</td>
                        <td>
                            <asp:TextBox ID="txtTenTheLoai" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Mô Tả Thể Loại</td>
                        <td class="auto-style9">
                            <asp:TextBox ID="txtMoTaTheLoai" runat="server" Height="39px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style7" colspan="2" style="text-align: center">
                            <asp:Button ID="btnThemTheLoai" runat="server" CssClass="nutdonhang" OnClick="btnThemTheLoai_Click" Text="THÊM" Width="126px" />
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
</asp:Content>

