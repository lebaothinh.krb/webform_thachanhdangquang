﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="AdminThemSanPham.aspx.cs" Inherits="ThuMucAdmin_AdminThemSanPham" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            width: 57px;
        }
        .auto-style4 {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="auto-style1">
            <tr>
                <td colspan="3" style="text-align: center; font-weight: 700">THÊM SẢN PHẨM</td>
            </tr>
            <tr>
                <td class="auto-style4">Tên Sản Phẩm:</td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:TextBox ID="txtTenSanPham" runat="server" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Đơn Vị Tính:</td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:TextBox ID="txtDonViTinh" runat="server" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Đơn Giá:</td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:TextBox ID="txtDonGia" runat="server" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Mô Tả:</td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <CKEditor:CKEditorControl ID="ckeditor1" runat="server" style="margin-right: 0px" ToolbarLocation="Top" Width="981px"></CKEditor:CKEditorControl>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Hình Minh Họa:</td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" />
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Mã Chủ Đề:</td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:DropDownList ID="ddlMaChuDe" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Ngày Cập Nhật:</td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:Label ID="lblNgayCapNhat" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Loại:</td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:DropDownList ID="ddlLoai" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Thể Loại: </td>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:DropDownList ID="ddlTheLoai" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="3">
                    <asp:Button ID="btnThem" runat="server" Text="Thêm Sản Phẩm" OnClick="btnThem_Click" style="text-align: center" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="lblBaoLoi" runat="server"></asp:Label>
                </td>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
</asp:Content>

