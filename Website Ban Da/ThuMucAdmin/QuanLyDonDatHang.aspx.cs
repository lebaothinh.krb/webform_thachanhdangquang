﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLogic;

public partial class ThuMucAdmin_QuanLyDonDatHang : System.Web.UI.Page
{
    DonDatHangBusiness ddhbn = new DonDatHangBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["QuyenHanAdmin"] == null)
        {
            Response.Redirect("~/ThuMucAdmin/DangNhapAdmin.aspx");
        }
        if (!IsPostBack)
        {
            CapNhat();
        }
    }
    void CapNhat()
    {
        gvChoXacNhan.DataSource = ddhbn.LoadDonHangTheoTinhTrang("1");
        gvChoXacNhan.DataBind();
        gvChoGiaoHang.DataSource = ddhbn.LoadDonHangTheoTinhTrang("2");
        gvChoGiaoHang.DataBind();
        gvDaGiao.DataSource = ddhbn.LoadDonHangTheoTinhTrang("3");
        gvDaGiao.DataBind();
        gvGiaoThanhCong.DataSource = ddhbn.LoadDonHangTheoTinhTrang("4");
        gvGiaoThanhCong.DataBind();
        gvDoiTra.DataSource = ddhbn.LoadDonHangTheoTinhTrang("5");
        gvDoiTra.DataBind();
    }
    protected void btnChuaxacnhan_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btnChoGiao_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void btnDaGiao_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    protected void btnDaNhanHang_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;
    }
    protected void btnDoiTra_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 4;
    }
    protected void gvChoXacNhan_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName=="Duyet")
        {
            ddhbn.DuyetDonHang("2",e.CommandArgument.ToString());
            CapNhat();
        }
    }
    protected void gvChoXacNhan_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ddhbn.XoaDonHang(gvChoXacNhan.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
    }
    protected void gvChoXacNhan_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvChoXacNhan.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvChoXacNhan_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DateTime a = Convert.ToDateTime((gvChoXacNhan.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text);
        ddhbn.CapNhatDonHang(gvChoXacNhan.DataKeys[e.RowIndex].Value.ToString(),a.Month+"/"+a.Day+"/"+a.Year, (gvChoXacNhan.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvChoXacNhan.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvChoXacNhan.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text, (gvChoXacNhan.Rows[e.RowIndex].Cells[7].Controls[0] as TextBox).Text, (gvChoXacNhan.Rows[e.RowIndex].Cells[8].Controls[0] as TextBox).Text, (gvChoXacNhan.Rows[e.RowIndex].Cells[9].Controls[0] as TextBox).Text, (gvChoXacNhan.Rows[e.RowIndex].Cells[10].Controls[0] as TextBox).Text, (gvChoXacNhan.Rows[e.RowIndex].Cells[11].Controls[0] as TextBox).Text, (gvChoXacNhan.Rows[e.RowIndex].Cells[12].Controls[0] as TextBox).Text);
        gvChoXacNhan.EditIndex = -1;
        CapNhat();
    }
    protected void gvChoXacNhan_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvChoXacNhan.EditIndex = -1;
        CapNhat();
    }
    protected void gvChoXacNhan_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvChoXacNhan.PageIndex = e.NewPageIndex;
        CapNhat();
    }
    protected void gvChoGiaoHang_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvChoGiaoHang.PageIndex = e.NewPageIndex;
        CapNhat();
    }
    protected void gvChoGiaoHang_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChoGiaoHangDuyet")
        {
            ddhbn.DuyetDonHang("3", e.CommandArgument.ToString());
            CapNhat();
        }
    }
    protected void gvChoGiaoHang_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ddhbn.XoaDonHang(gvChoGiaoHang.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
    }
    protected void gvChoGiaoHang_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvChoGiaoHang.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvChoGiaoHang_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DateTime a = Convert.ToDateTime((gvChoGiaoHang.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text);
        ddhbn.CapNhatDonHang(gvChoGiaoHang.DataKeys[e.RowIndex].Value.ToString(), a.Month + "/" + a.Day + "/" + a.Year, (gvChoGiaoHang.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvChoGiaoHang.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvChoGiaoHang.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text, (gvChoGiaoHang.Rows[e.RowIndex].Cells[7].Controls[0] as TextBox).Text, (gvChoGiaoHang.Rows[e.RowIndex].Cells[8].Controls[0] as TextBox).Text, (gvChoGiaoHang.Rows[e.RowIndex].Cells[9].Controls[0] as TextBox).Text, (gvChoGiaoHang.Rows[e.RowIndex].Cells[10].Controls[0] as TextBox).Text, (gvChoGiaoHang.Rows[e.RowIndex].Cells[11].Controls[0] as TextBox).Text, (gvChoGiaoHang.Rows[e.RowIndex].Cells[12].Controls[0] as TextBox).Text);
        gvChoGiaoHang.EditIndex = -1;
        CapNhat();
    }
    protected void gvChoGiaoHang_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvChoGiaoHang.EditIndex = -1;
        CapNhat();
    }
    protected void gvDaGiao_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDaGiao.PageIndex = e.NewPageIndex;
        CapNhat();
    }
    protected void gvDaGiao_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDaGiao.EditIndex = -1;
        CapNhat();
    }
    protected void gvDaGiao_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Duyet")
        {
            ddhbn.DuyetDonHang("4", e.CommandArgument.ToString());
            CapNhat();
        }
    }
    protected void gvDaGiao_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ddhbn.XoaDonHang(gvDaGiao.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
    }
    protected void gvDaGiao_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDaGiao.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvDaGiao_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DateTime a = Convert.ToDateTime((gvDaGiao.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text);
        ddhbn.CapNhatDonHang(gvDaGiao.DataKeys[e.RowIndex].Value.ToString(), a.Month + "/" + a.Day + "/" + a.Year, (gvDaGiao.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvDaGiao.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvDaGiao.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text, (gvDaGiao.Rows[e.RowIndex].Cells[7].Controls[0] as TextBox).Text, (gvDaGiao.Rows[e.RowIndex].Cells[8].Controls[0] as TextBox).Text, (gvDaGiao.Rows[e.RowIndex].Cells[9].Controls[0] as TextBox).Text, (gvDaGiao.Rows[e.RowIndex].Cells[10].Controls[0] as TextBox).Text, (gvDaGiao.Rows[e.RowIndex].Cells[11].Controls[0] as TextBox).Text, (gvDaGiao.Rows[e.RowIndex].Cells[12].Controls[0] as TextBox).Text);
        gvDaGiao.EditIndex = -1;
        CapNhat();
    }
    protected void gvGiaoThanhCong_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvGiaoThanhCong.EditIndex = -1;
        CapNhat();
    }
    protected void gvGiaoThanhCong_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName=="DoiTra")
        {
            ddhbn.DuyetDonHang("5", e.CommandArgument.ToString());
            CapNhat();
        }
    }
    protected void gvGiaoThanhCong_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ddhbn.XoaDonHang(gvGiaoThanhCong.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
    }
    protected void gvGiaoThanhCong_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvGiaoThanhCong.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvGiaoThanhCong_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DateTime a = Convert.ToDateTime((gvGiaoThanhCong.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text);
        ddhbn.CapNhatDonHang(gvGiaoThanhCong.DataKeys[e.RowIndex].Value.ToString(), a.Month + "/" + a.Day + "/" + a.Year, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[7].Controls[0] as TextBox).Text, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[8].Controls[0] as TextBox).Text, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[9].Controls[0] as TextBox).Text, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[10].Controls[0] as TextBox).Text, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[11].Controls[0] as TextBox).Text, (gvGiaoThanhCong.Rows[e.RowIndex].Cells[12].Controls[0] as TextBox).Text);
        gvGiaoThanhCong.EditIndex = -1;
        CapNhat();
    }
    protected void gvGiaoThanhCong_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGiaoThanhCong.PageIndex = e.NewPageIndex;
        CapNhat();
    }
    protected void gvDoiTra_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDoiTra.EditIndex = -1;
        CapNhat();
    }
    protected void gvDoiTra_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName=="Duyet")
        {
            ddhbn.DuyetDonHang("4", e.CommandArgument.ToString());
            CapNhat();
        }
    }
    protected void gvDoiTra_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ddhbn.XoaDonHang(gvDoiTra.DataKeys[e.RowIndex].Value.ToString());
        CapNhat();
    }
    protected void gvDoiTra_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDoiTra.EditIndex = e.NewEditIndex;
        CapNhat();
    }
    protected void gvDoiTra_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DateTime a = Convert.ToDateTime((gvDoiTra.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text);
        ddhbn.CapNhatDonHang(gvDoiTra.DataKeys[e.RowIndex].Value.ToString(), a.Month + "/" + a.Day + "/" + a.Year, (gvDoiTra.Rows[e.RowIndex].Cells[4].Controls[0] as TextBox).Text, (gvDoiTra.Rows[e.RowIndex].Cells[5].Controls[0] as TextBox).Text, (gvDoiTra.Rows[e.RowIndex].Cells[6].Controls[0] as TextBox).Text, (gvDoiTra.Rows[e.RowIndex].Cells[7].Controls[0] as TextBox).Text, (gvDoiTra.Rows[e.RowIndex].Cells[8].Controls[0] as TextBox).Text, (gvDoiTra.Rows[e.RowIndex].Cells[9].Controls[0] as TextBox).Text, (gvDoiTra.Rows[e.RowIndex].Cells[10].Controls[0] as TextBox).Text, (gvDoiTra.Rows[e.RowIndex].Cells[11].Controls[0] as TextBox).Text, (gvDoiTra.Rows[e.RowIndex].Cells[12].Controls[0] as TextBox).Text);
        gvDoiTra.EditIndex = -1;
        CapNhat();
    }
}