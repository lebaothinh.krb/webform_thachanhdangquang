﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DangNhapAdmin.aspx.cs" Inherits="ThuMucAdmin_DangNhapAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


</head>
<body>
    <form id="form1" runat="server">
    <div>
    
                <table class="thandn" align="center">
                    <tr>
                        <td class="chuchungdn">Tên Đăng Nhập:</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="txtTaiKhoan" runat="server" CssClass="txtdn"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTaiKhoan" ErrorMessage="Chưa Nhập Tài Khoản" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="chuchungdn">Mật Khẩu:</td>
                        <td class="auto-style2">
                            <asp:TextBox ID="txtMatKhau" runat="server" CssClass="txtdn" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMatKhau" ErrorMessage="Chưa Nhập Mật Khẩu" ForeColor="Red" SetFocusOnError="True" Font-Bold="True">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Button ID="btnDangNhap" runat="server" Text="ĐĂNG NHẬP" CssClass="nut" OnClick="Button1_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1" colspan="2">
                            <asp:Label ID="lblBaoLoi" runat="server" ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                    </tr>
                    </table>
    
    </div>
    </form>
</body>
</html>
