﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    public class TinhTrangThanhToan
    {
        int _MaTinhTrang;

        public int MaTinhTrang
        {
            get { return _MaTinhTrang; }
            set { _MaTinhTrang = value; }
        }
        string _TenTinhTrang;

        public string TenTinhTrang
        {
            get { return _TenTinhTrang; }
            set { _TenTinhTrang = value; }
        }
        string _MoTaTinhTrang;

        public string MoTaTinhTrang
        {
            get { return _MoTaTinhTrang; }
            set { _MoTaTinhTrang = value; }
        }
    }
}
