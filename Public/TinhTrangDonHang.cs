﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    class TinhTrangDonHang
    {
        private int _MaTinhTrang;

        public int MaTinhTrang
        {
            get { return _MaTinhTrang; }
            set { _MaTinhTrang = value; }
        }
        private int _TenTinhTrang;

        public int TenTinhTrang
        {
            get { return _TenTinhTrang; }
            set { _TenTinhTrang = value; }
        }
        private int _MoTaTinhTrang;

        public int MoTaTinhTrang
        {
            get { return _MoTaTinhTrang; }
            set { _MoTaTinhTrang = value; }
        }
    }
}
