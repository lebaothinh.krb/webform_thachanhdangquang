﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    class HinhThucGiaoHang
    {
        private int _MaHinhThucGH;

        public int MaHinhThucGH
        {
            get { return _MaHinhThucGH; }
            set { _MaHinhThucGH = value; }
        }
        private string _TenHinhThucGH;

        public string TenHinhThucGH
        {
            get { return _TenHinhThucGH; }
            set { _TenHinhThucGH = value; }
        }
        private string _MoTaHinhThucGH;

        public string MoTaHinhThucGH
        {
            get { return _MoTaHinhThucGH; }
            set { _MoTaHinhThucGH = value; }
        }
        private double _GiaTienGiao;

        public double GiaTienGiao
        {
            get { return _GiaTienGiao; }
            set { _GiaTienGiao = value; }
        }
        private string _DonViTinh;

        public string DonViTinh
        {
            get { return _DonViTinh; }
            set { _DonViTinh = value; }
        }
        private int _ThoiGianDuKien;

        public int ThoiGianDuKien
        {
            get { return _ThoiGianDuKien; }
            set { _ThoiGianDuKien = value; }
        }
    }
}
