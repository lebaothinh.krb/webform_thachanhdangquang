﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace Public
{
    public class Loai
    {
        private int _MaLoai;

        public int MaLoai
        {
            get { return _MaLoai; }
            set { _MaLoai = value; }
        }
        private string _TenLoai;

        public string TenLoai
        {
            get { return _TenLoai; }
            set { _TenLoai = value; }
        }
        private string _MoTaLoai;

        public string MoTaLoai
        {
            get { return _MoTaLoai; }
            set { _MoTaLoai = value; }
        }
    }
}
