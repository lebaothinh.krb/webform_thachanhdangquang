﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace Public
{
    public class TheLoai
    {
        private int _MaTheLoai;

        public int MaTheLoai
        {
            get { return _MaTheLoai; }
            set { _MaTheLoai = value; }
        }
        private string _TenTheLoai;

        public string TenTheLoai
        {
            get { return _TenTheLoai; }
            set { _TenTheLoai = value; }
        }
        private string _MoTaTheLoai;

        public string MoTaTheLoai
        {
            get { return _MoTaTheLoai; }
            set { _MoTaTheLoai = value; }
        }
    }
}
