﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    public class ChuDe
    {
        private int _MaChuDe;

        public int MaChuDe
        {
            get { return _MaChuDe; }
            set { _MaChuDe = value; }
        }
        private string _TenChuDe;

        public string TenChuDe
        {
            get { return _TenChuDe; }
            set { _TenChuDe = value; }
        }
        private string _MoTaChuDe;

        public string MoTaChuDe
        {
            get { return _MoTaChuDe; }
            set { _MoTaChuDe = value; }
        }
    }
}
