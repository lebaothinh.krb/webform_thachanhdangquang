﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    public class QuenMatKhau
    {
        KhachHang _MaKhachHang;

        public int MaKhachHang
        {
            get { return _MaKhachHang.MaKhachHang; }
            set { _MaKhachHang.MaKhachHang = value; }
        }
        string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        string _Random;

        public string Random
        {
            get { return _Random; }
            set { _Random = value; }
        }
        DateTime _NgayHetHan;

        public DateTime NgayHetHan
        {
            get { return _NgayHetHan; }
            set { _NgayHetHan = value; }
        }
    }
}
