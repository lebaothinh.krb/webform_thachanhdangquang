﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    class DonDatHang
    {
        private int _SoDonHang;

        public int SoDonHang
        {
            get { return _SoDonHang; }
            set { _SoDonHang = value; }
        }
        private KhachHang _MaKhachHang;

        public int MaKhachHang
        {
            get { return this._MaKhachHang.MaKhachHang; }
            set { this._MaKhachHang.MaKhachHang = value; }
        }
        private DateTime _NgayDatHang;

        public DateTime NgayDatHang
        {
            get { return _NgayDatHang; }
            set { _NgayDatHang = value; }
        }
        private DateTime _NgayGiao;

        public DateTime NgayGiao
        {
            get { return _NgayGiao; }
            set { _NgayGiao = value; }
        }
        private double _TriGia;

        public double TriGia
        {
            get { return _TriGia; }
            set { _TriGia = value; }
        }
        private TinhTrangDonHang ttdh;

        public int TinhTrangDonHang
        {
            get { return this.ttdh.MaTinhTrang; }
            set { this.ttdh.MaTinhTrang = value; }
        }

        private bool _TinhTrangThanhToan;

        public bool TinhTrangThanhToan
        {
            get { return _TinhTrangThanhToan; }
            set { _TinhTrangThanhToan = value; }
        }

        private string _TenNguoiNhan;

        public string TenNguoiNhan
        {
            get { return _TenNguoiNhan; }
            set { _TenNguoiNhan = value; }
        }
        private string _DiaChiNguoiNhan;

        public string DiaChiNguoiNhan
        {
            get { return _DiaChiNguoiNhan; }
            set { _DiaChiNguoiNhan = value; }
        }
        private HinhThucThanhToan httt;

        public int HinhThucThanhToan
        {
            get { return this.httt.MaHinhThucTT; }
            set { this.httt.MaHinhThucTT = value; }
        }
        private HinhThucGiaoHang htgh;

        public int HinhThucGiaoHang
        {
            get { return this.htgh.MaHinhThucGH; }
            set { this.htgh.MaHinhThucGH = value; }
        }
    }
}
