﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    class Admin
    {
        private int _MaAdmin;

        public int MaAdmin
        {
            get { return _MaAdmin; }
            set { _MaAdmin = value; }
        }
        private string _TenAdmin;

        public string TenAdmin
        {
            get { return _TenAdmin; }
            set { _TenAdmin = value; }
        }
        private string _DienThoaiAdmin;

        public string DienThoaiAdmin
        {
            get { return _DienThoaiAdmin; }
            set { _DienThoaiAdmin = value; }
        }
        private DateTime _NgaySinhAdmin;

        public DateTime NgaySinhAdmin
        {
            get { return _NgaySinhAdmin; }
            set { _NgaySinhAdmin = value; }
        }
        private bool _GioiTinhAdmin;

        public bool GioiTinhAdmin
        {
            get { return _GioiTinhAdmin; }
            set { _GioiTinhAdmin = value; }
        }
        private string _TenDangNhapAdmin;

        public string TenDangNhapAdmin
        {
            get { return _TenDangNhapAdmin; }
            set { _TenDangNhapAdmin = value; }
        }
        private string _MatKhauAdmin;

        public string MatKhauAdmin
        {
            get { return _MatKhauAdmin; }
            set { _MatKhauAdmin = value; }
        }
        private string _DiaChiAdmin;

        public string DiaChiAdmin
        {
            get { return _DiaChiAdmin; }
            set { _DiaChiAdmin = value; }
        }
        private string _EmailAdmin;

        public string EmailAdmin
        {
            get { return _EmailAdmin; }
            set { _EmailAdmin = value; }
        }
        private int _QuyenHanAdmin;

        public int QuyenHanAdmin
        {
            get { return _QuyenHanAdmin; }
            set { _QuyenHanAdmin = value; }
        }
    }
}
