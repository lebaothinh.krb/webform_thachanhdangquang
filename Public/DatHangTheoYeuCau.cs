﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    public class DatHangTheoYeuCau
    {
        int _MaDatHang;

        public int MaDatHang
        {
            get { return _MaDatHang; }
            set { _MaDatHang = value; }
        }

        KhachHang _MaKhachHang;

        public int MaKhachHang
        {
            get { return _MaKhachHang.MaKhachHang; }
            set { _MaKhachHang.MaKhachHang = value; }
        }


        string _KichThuoc;

        public string KichThuoc
        {
            get { return _KichThuoc; }
            set { _KichThuoc = value; }
        }
        string _HinhAnh;

        public string HinhAnh
        {
            get { return _HinhAnh; }
            set { _HinhAnh = value; }
        }
        string _NoiDung;

        public string NoiDung
        {
            get { return _NoiDung; }
            set { _NoiDung = value; }
        }
        string _VatLieu;

        public string VatLieu
        {
            get { return _VatLieu; }
            set { _VatLieu = value; }
        }

        int _SoLuong;

        public int SoLuong
        {
            get { return _SoLuong; }
            set { _SoLuong = value; }
        }
    }
}
