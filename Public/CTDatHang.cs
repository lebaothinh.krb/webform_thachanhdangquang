﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    class CTDatHang
    {
        private int _SoDonHang;

        public int SoDonHang
        {
            get { return _SoDonHang; }
            set { _SoDonHang = value; }
        }
        private SanPham _MaSanPham;

        public int MaSanPham
        {
            get { return _MaSanPham.MaSanPham; }
            set { _MaSanPham.MaSanPham = value; }
        }
        private int _SoLuong;

        public int SoLuong
        {
            get { return _SoLuong; }
            set { _SoLuong = value; }
        }
        private double _DonGia;

        public double DonGia
        {
            get { return _DonGia; }
            set { _DonGia = value; }
        }
        private double _ThanhTien;

        public double ThanhTien
        {
            get { return _ThanhTien; }
            set { _ThanhTien = value; }
        }
    }
}
