﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    class HinhThucThanhToan
    {
        private int _MaHinhThucTT;

        public int MaHinhThucTT
        {
            get { return _MaHinhThucTT; }
            set { _MaHinhThucTT = value; }
        }
        private string _TenHinhThucTT;

        public string TenHinhThucTT
        {
            get { return _TenHinhThucTT; }
            set { _TenHinhThucTT = value; }
        }
        private string _MoTaHinhThucTT;

        public string MoTaHinhThucTT
        {
            get { return _MoTaHinhThucTT; }
            set { _MoTaHinhThucTT = value; }
        }
    }
}
