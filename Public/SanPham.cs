﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace Public
{
    public class SanPham
    {
        public SanPham(DataRow dr)
        {
            this.MaSanPham = Convert.ToInt32(dr["MASANPHAM"]);
            this.TenSanPham = dr["TENSANPHAM"].ToString();
            this.DonGia = Convert.ToDouble(dr["DONGIA"]);
            this.DonViTinh = dr["DONVITINH"].ToString();
            this.HinhMinhHoa = dr["HINHMINHHOA"].ToString();
            this.Loai = Convert.ToInt32(dr["LOAI"]);
            this.MaChuDe = Convert.ToInt32(dr["MACHUDE"]);
            this.MoTa = dr["MOTA"].ToString();
            this.NgayCapNhat = Convert.ToDateTime(dr["NGAYCAPNHAT"]);
            this.SoLanXem = Convert.ToInt32(dr["SOLANXEM"]);
            this.SoLuongBan = Convert.ToInt32(dr["SOLUONGBAN"]);
            this.TheLoai = Convert.ToInt32(dr["THELOAI"]);
        }
        private int _MaSanPham;

        public int MaSanPham
        {
            get { return _MaSanPham; }
            set { _MaSanPham = value; }
        }
        private string _TenSanPham;

        public string TenSanPham
        {
            get { return _TenSanPham; }
            set { _TenSanPham = value; }
        }
        private string _DonViTinh;

        public string DonViTinh
        {
            get { return _DonViTinh; }
            set { _DonViTinh = value; }
        }
        private double _DonGia;

        public double DonGia
        {
            get { return _DonGia; }
            set { _DonGia = value; }
        }
        private string _MoTa;

        public string MoTa
        {
            get { return _MoTa; }
            set { _MoTa = value; }
        }
        private string _HinhMinhHoa;

        public string HinhMinhHoa
        {
            get { return _HinhMinhHoa; }
            set { _HinhMinhHoa = value; }
        }
        private ChuDe _ChuDe;

        public int MaChuDe
        {
            get { return this._ChuDe.MaChuDe; }
            set { this._ChuDe.MaChuDe = value; }
        }
        private DateTime _NgayCapNhat;

        public DateTime NgayCapNhat
        {
            get { return _NgayCapNhat; }
            set { _NgayCapNhat = value; }
        }
        private int _SoLuongBan;

        public int SoLuongBan
        {
            get { return _SoLuongBan; }
            set { _SoLuongBan = value; }
        }
        private int _SoLanXem;

        public int SoLanXem
        {
            get { return _SoLanXem; }
            set { _SoLanXem = value; }
        }
        private Loai _Loai;

        public int Loai
        {
            get { return this._Loai.MaLoai; }
            set { this._Loai.MaLoai = value; }
        }
        private TheLoai _TheLoai;

        public int TheLoai
        {
            get { return this._TheLoai.MaTheLoai; }
            set { this._TheLoai.MaTheLoai = value; }
        }
    }
}
