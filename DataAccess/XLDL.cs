﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for XLDL
/// </summary>
public class XLDL
{

    //
    // TODO: Add constructor logic here
    //
    public string strCon = ConfigurationManager.ConnectionStrings["conn"].ConnectionString.ToString();
    public DataTable GetTable(string sql)
    {

        SqlDataAdapter DtA = new SqlDataAdapter(sql, strCon);
        DataTable dt = new DataTable();
        DtA.Fill(dt);
        return dt;

    }
    public void Execute(string sql)
    {
        using (SqlConnection sqlCon = new SqlConnection(strCon))
        {
            sqlCon.Open();
            SqlCommand cmd = new SqlCommand(sql, sqlCon);
            cmd.ExecuteNonQuery();
            sqlCon.Close();
        }
    }
    public string GetValue(string sql)
    {
        using (SqlConnection sqlCon = new SqlConnection(strCon))
        {
            sqlCon.Open();
            SqlCommand cmd = new SqlCommand(sql, sqlCon);
            string value = cmd.ExecuteScalar().ToString();
            sqlCon.Close();
            return value;
        }
    }
}