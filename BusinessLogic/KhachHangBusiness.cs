﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Public;
using System.Data;

namespace BusinessLogic
{
    public class KhachHangBusiness
    {
        XLDL x = new XLDL();
        public DataTable LoadKhachHang()
        {
            return x.GetTable("select * from KHACHHANG");
        }
        public DataTable LoadMotKhachHang(string MaKhachHang)
        {
            return x.GetTable("select * from KHACHHANG WHERE MAKHACHHANG="+MaKhachHang);
        }
        public void ThemKhachHang(string TenKhachHang, string DienThoai, string NgaySinh, string GioiTinh, string TenDangNhap, string MatKhau, string DiaChi, string Email)
        {
            x.Execute("INSERT INTO KHACHHANG (TENKHACHHANG,DIENTHOAI,NGAYSINH,GIOITINH,TENDANGNHAP,MATKHAU,DIACHI,EMAIL,DADUYET) VALUES (N'"+TenKhachHang+"','"+DienThoai+"','"+NgaySinh+"','"+GioiTinh+"','"+TenDangNhap+"','"+MatKhau+"',N'"+DiaChi+"','"+Email+"','False')");
        }
        public void DoiMatKhau (string MaKhachHang, string MatKhauMoi)
        {
            x.Execute("UPDATE KHACHHANG SET MATKHAU='"+MatKhauMoi+"' WHERE MAKHACHHANG='"+MaKhachHang+"'");
        }
        public void XoaKhachHang (string MaKhachHang)
        {
            x.Execute("DELETE FROM KHACHHANG WHERE MAKHACHHANG="+MaKhachHang);
        }
        public void CapNhatKhachHang (string MaKhachHang, string TenKhachHang, string DienThoai, string NgaySinh, string GioiTinh, string TenDangNhap, string MatKhau, string DiaChi, string Email, string DaDuyet)
        {
            x.Execute("UPDATE KHACHHANG SET TENKHACHHANG=N'"+TenKhachHang+"',DIENTHOAI='"+DienThoai+"',NGAYSINH='"+NgaySinh+"',GIOITINH='"+GioiTinh+"',TENDANGNHAP=N'"+TenDangNhap+"',MATKHAU=N'"+MatKhau+"',DIACHI=N'"+DiaChi+"',EMAIL='"+Email+"',DADUYET='"+DaDuyet+"' WHERE MAKHACHHANG='"+MaKhachHang+"'");
        }
    }
}
