﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BusinessLogic;

namespace BusinessLogic
{
    public class Admin
    {
        XLDL x = new XLDL();
        public DataTable LoadAdmin()
        {
            return x.GetTable("SELECT * FROM ADMIN");
        }
        public DataTable LoadAdminTheoQuyenHan(string QuyenHanAdmin)
        {
            return x.GetTable("SELECT * FROM ADMIN WHERE QUYENHANADMIN<"+QuyenHanAdmin);
        }
        public void ThemAdmin (string TenAdmin, string SDTAdmin, string NgaySinhAdmin, string GioiTinh, string TenDangNhapAdmin, string MatKhauAdmin, string DiaChiAdmin, string EmailAdmin, string QuyenHanAdmin)
        {
            x.Execute("INSERT INTO ADMIN VALUES (N'"+TenAdmin+"','"+SDTAdmin+"','"+NgaySinhAdmin+"','"+GioiTinh+"','"+TenDangNhapAdmin+"','"+MatKhauAdmin+"',N'"+DiaChiAdmin+"','"+EmailAdmin+"','"+QuyenHanAdmin+"')");
        }
        public void XoaAdmin (string MaAdmin)
        {
            x.Execute("DELETE FROM ADMIN WHERE MAADMIN=" + MaAdmin);
        }
        public void CapNhatAdmin (string MaAdmin, string TenAdmin, string SDTAdmin, string NgaySinhAdmin, string GioiTinh, string TenDangNhapAdmin, string MatKhauAdmin, string DiaChiAdmin, string EmailAdmin, string QuyenHanAdmin)
        {
            x.Execute("UPDATE ADMIN SET TENADMIN=N'"+TenAdmin+"',DIENTHOAIADMIN='"+SDTAdmin+"',NGAYSINHADMIN='"+NgaySinhAdmin+"',GIOITINHADMIN='"+GioiTinh+"',TENDANGNHAPADMIN='"+TenDangNhapAdmin+"',MATKHAUADMIN='"+MatKhauAdmin+"',DIACHIADMIN=N'"+DiaChiAdmin+"',EMAILADMIN='"+EmailAdmin+"',QUYENHANADMIN='"+QuyenHanAdmin+"' WHERE MAADMIN="+MaAdmin);
        }
    }
}
