﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Public;

namespace BusinessLogic
{
    public class LoaiBussiness
    {
        XLDL x = new XLDL();
        public DataTable LoadLoai()
        {
            return x.GetTable("select * from LOAI");
        }
        public void ThemLoai (Loai loai)
        {
            x.Execute("INSERT INTO LOAI (TENLOAI,MOTALOAI) VALUES (N'"+loai.TenLoai+"',N'"+loai.MoTaLoai+"')");
        }
        public string LayTenLoai (string MaLoai)
        {
            return x.GetValue("SELECT TENLOAI FROM LOAI WHERE MALOAI="+MaLoai);
        }
        public DataTable LoadMotLoai(string MaLoai)
        {
            return x.GetTable("select * from LOAI WHERE MALOAI=" + MaLoai);
        }
        public void ThemLoai(string TenLoai, string MoTaLoai)
        {
            x.Execute("INSERT INTO LOAI VALUES (N'" + TenLoai + "',N'" + MoTaLoai + "')");
        }
        public void CapNhatLoai(string MaLoai, string TenLoai, string MoTaLoai)
        {
            x.Execute("UPDATE LOAI SET TENLOAI=N'" + TenLoai + "',MOTALOAI=N'" + MoTaLoai + "' WHERE MALOAI=" + MaLoai);
        }
        public void XoaLoai(string MaLoai)
        {
            x.Execute("DELETE FROM LOAI WHERE MALOAI=" + MaLoai);
        }
    }
}
