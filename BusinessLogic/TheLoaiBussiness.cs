﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace BusinessLogic
{
    public class TheLoaiBussiness
    {
        XLDL x = new XLDL();
        public DataTable LoadTheLoai()
        {
            return x.GetTable("select MATHELOAI,TENTHELOAI,MOTATHELOAI from THELOAI");
        }
        public string LayTenTheLoai (string MaTheLoai)
        {
            return x.GetValue("SELECT TENTHELOAI FROM THELOAI WHERE MATHELOAI="+MaTheLoai);
        }
        public DataTable LoadMotTheLoai(string MaTheLoai)
        {
            return x.GetTable("select * from THELOAI WHERE MATHELOAI=" + MaTheLoai);
        }
        public void ThemTheLoai (string TenTheLoai,string MoTaTheLoai)
        {
            x.Execute("INSERT INTO THELOAI VALUES (N'"+TenTheLoai+"',N'"+MoTaTheLoai+"')");
        }
        public void CapNhatTheLoai (string MaTheLoai, string TenTheLoai,string MoTaTheLoai)
        {
            x.Execute("UPDATE THELOAI SET TENTHELOAI=N'"+TenTheLoai+"',MOTATHELOAI=N'"+MoTaTheLoai+"' WHERE MATHELOAI="+MaTheLoai);
        }
        public void XoaTheLoai (string MaTheLoai)
        {
            x.Execute("DELETE FROM THELOAI WHERE MATHELOAI="+MaTheLoai);
        }
    }
}
