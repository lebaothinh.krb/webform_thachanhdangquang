﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BusinessLogic
{
    public class HinhThucGiaoHang
    {
        XLDL x = new XLDL();
        public DataTable LoadHinhThucGiaoHang()
        {
            return x.GetTable("select * from HINHTHUCGIAOHANG");
        }
    }
}
