﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using System.Data;

namespace BusinessLogic
{
    public class QuenMatKhauBusiness
    {
        XLDL x = new XLDL();
        public DataTable LoadQuenMatKhau()
        {
            return x.GetTable("select * from QUENMATKHAU");
        }
        public void ThemQuenMatKhau(string MaKhachHang, string Email, string Random, string NgayHetHan)
        {
            x.Execute("INSERT INTO QUENMATKHAU VALUES ('" + MaKhachHang + "','" + Email + "','" + Random + "','" + NgayHetHan + "')");
        }
        public void XoaQuenMatKhau(string Email)
        {
            x.Execute("DELETE FROM QUENMATKHAU WHERE EMAIL='" + Email + "'");
        }
        public void XoaNgayHetHan()
        {
            x.Execute("DELETE FROM QUENMATKHAU WHERE NGAYHETHAN = '" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "'");
        }
    }
}
