﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace BusinessLogic
{
    public class ChuDeBussiness
    {
        XLDL x = new XLDL();
        public DataTable LoadChuDe()
        {
            return x.GetTable("select * from CHUDE");
        }
        public string LayTenChuDe (string MaChuDe)
        {
            return x.GetValue("SELECT TENCHUDE FROM CHUDE WHERE MACHUDE="+MaChuDe);
        }
        public DataTable LoadMotChuDe(string MaChuDe)
        {
            return x.GetTable("select * from CHUDE WHERE MACHUDE=" + MaChuDe);
        }
        public void ThemChuDe(string TenChuDe, string MoTaChuDe)
        {
            x.Execute("INSERT INTO CHUDE VALUES (N'" + TenChuDe + "',N'" + MoTaChuDe + "')");
        }
        public void CapNhatChuDe(string MaChuDe, string TenChuDe, string MoTaChuDe)
        {
            x.Execute("UPDATE CHUDE SET TENCHUDE=N'" + TenChuDe + "',MOTACHUDE=N'" + MoTaChuDe + "' WHERE MACHUDE=" + MaChuDe);
        }
        public void XoaChuDe(string MaChuDe)
        {
            x.Execute("DELETE FROM CHUDE WHERE MACHUDE=" + MaChuDe);
        }
    }
}
