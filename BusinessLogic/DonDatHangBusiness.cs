﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BusinessLogic
{
    public class DonDatHangBusiness
    {
        XLDL x = new XLDL();
        CTDatHang ctdh = new CTDatHang();
        public DataTable LoadDonHang()
        {
            return x.GetTable("select * from DONDATHANG,CTDATHANG WHERE DONDATHANG.SODONHANG = CTDATHANG.SODONHANG");
        }
        public DataTable LoadDonHangChiTiet (string SoDonHang)
        {
            return x.GetTable("select * FROM DONDATHANG,KHACHHANG,TINHTRANGDONHANG,HINHTHUCGIAOHANG,HINHTHUCTHANHTOAN WHERE DONDATHANG.MAKHACHHANG = KHACHHANG.MAKHACHHANG AND DONDATHANG.TINHTRANGDONHANG = TINHTRANGDONHANG.MATINHTRANG AND DONDATHANG.HINHTHUCGIAOHANG = HINHTHUCGIAOHANG.MAHINHTHUCGH AND DONDATHANG.HINHTHUCTHANHTOAN = HINHTHUCTHANHTOAN.MAHINHTHUCTT AND SODONHANG="+SoDonHang);
        }
        public DataTable LoadMotDonHang (string SoDonHang)
        {
            return x.GetTable("select * from DONDATHANG,CTDATHANG WHERE DONDATHANG.SODONHANG = CTDATHANG.SODONHANG AND SODONHANG="+SoDonHang);
        }
        public string TimMaDonHangMax ()
        {
            return x.GetValue("select MAX(SODONHANG) from DONDATHANG");
        }
        public void ThemDonHang (string MaKhachHang,string NgayDatHang,string NgayGiao,string TriGia, string TinhTrangDonHang,string TinhTrangThanhToan, string TenNguoiNhan, string DiaChiNguoiNhan, string SoDienThoaiNguoiNhan, string HinhThucThanhToan, string HinhThucGiaoHang,string GhiChu, string Email)
        {
            x.Execute("INSERT INTO DONDATHANG (MAKHACHHANG,NGAYDATHANG,NGAYGIAO,TRIGIA,TINHTRANGDONHANG,TINHTRANGTHANHTOAN,TENNGUOINHAN,DIACHINGUOINHAN,HINHTHUCTHANHTOAN,HINHTHUCGIAOHANG,DIENTHOAINGUOINHAN,GHICHU,EMAIL) VALUES ('"+MaKhachHang+"','"+NgayDatHang+"','"+NgayGiao+"','"+TriGia+"','"+TinhTrangDonHang+"','"+TinhTrangThanhToan+"',N'"+TenNguoiNhan+"',N'"+DiaChiNguoiNhan+"','"+HinhThucThanhToan+"','"+HinhThucGiaoHang+"','"+SoDienThoaiNguoiNhan+"',N'"+GhiChu+"','"+Email+"')");
            
        }
        public void XoaDonHang (string SoDonHang)
        {
            x.Execute("DELETE FROM CTDATHANG WHERE SODONHANG="+SoDonHang);
            ctdh.XoaCTDatHang(SoDonHang);
        }
        public DataTable LoadDonHangTheoTinhTrang (string MaTinhTrang)
        {
            return x.GetTable("select * FROM DONDATHANG WHERE  TINHTRANGDONHANG=" + MaTinhTrang);
        }
        public void CapNhatDonHang (string SoDonHang,string NgayGiao, string TriGia, string TinhTrangDonHang,string TinhTrangThanhToan,string TenNguoiNhan,string DiaChiNguoiNhan, string DienThoaiNguoiNhan,string GhiChu,string HinhThucGiaoHang,string HinhThucThanhToan, string Email)
        {
            x.Execute("UPDATE DONDATHANG SET NGAYGIAO='"+NgayGiao+"',TRIGIA='"+TriGia+"',TINHTRANGDONHANG='"+TinhTrangDonHang+"',TINHTRANGTHANHTOAN='"+TinhTrangThanhToan+"',TENNGUOINHAN=N'"+TenNguoiNhan+"',DIACHINGUOINHAN=N'"+DiaChiNguoiNhan+"',DIENTHOAINGUOINHAN='"+DienThoaiNguoiNhan+"',GHICHU=N'"+GhiChu+"',HINHTHUCTHANHTOAN='"+HinhThucThanhToan+"',HINHTHUCGIAOHANG='"+HinhThucGiaoHang+"',EMAIL='"+Email+"' WHERE SODONHANG='"+SoDonHang+"'");
        }
        public DataTable LoadDonHangHomNay()
        {
            return x.GetTable("select * FROM DONDATHANG,KHACHHANG,TINHTRANGDONHANG,HINHTHUCGIAOHANG,HINHTHUCTHANHTOAN WHERE DONDATHANG.MAKHACHHANG = KHACHHANG.MAKHACHHANG AND DONDATHANG.TINHTRANGDONHANG = TINHTRANGDONHANG.MATINHTRANG AND DONDATHANG.HINHTHUCGIAOHANG = HINHTHUCGIAOHANG.MAHINHTHUCGH AND DONDATHANG.HINHTHUCTHANHTOAN = HINHTHUCTHANHTOAN.MAHINHTHUCTT AND DAY(DONDATHANG.NGAYDATHANG)=DAY(GETDATE()) and MONTH(DONDATHANG.NGAYDATHANG) = MONTH(GETDATE()) AND YEAR(DONDATHANG.NGAYDATHANG)=YEAR(GETDATE())");
        }
        public DataTable LoadDonHangThangNay()
        {
            return x.GetTable("select * FROM DONDATHANG,KHACHHANG,TINHTRANGDONHANG,HINHTHUCGIAOHANG,HINHTHUCTHANHTOAN WHERE DONDATHANG.MAKHACHHANG = KHACHHANG.MAKHACHHANG AND DONDATHANG.TINHTRANGDONHANG = TINHTRANGDONHANG.MATINHTRANG AND DONDATHANG.HINHTHUCGIAOHANG = HINHTHUCGIAOHANG.MAHINHTHUCGH AND DONDATHANG.HINHTHUCTHANHTOAN = HINHTHUCTHANHTOAN.MAHINHTHUCTT AND MONTH(DONDATHANG.NGAYDATHANG)=MONTH(GETDATE()) AND YEAR(DONDATHANG.NGAYDATHANG)=YEAR(GETDATE())");
        }
        public DataTable LoadDonHangNamNay()
        {
            return x.GetTable("select * FROM DONDATHANG,KHACHHANG,TINHTRANGDONHANG,HINHTHUCGIAOHANG,HINHTHUCTHANHTOAN WHERE DONDATHANG.MAKHACHHANG = KHACHHANG.MAKHACHHANG AND DONDATHANG.TINHTRANGDONHANG = TINHTRANGDONHANG.MATINHTRANG AND DONDATHANG.HINHTHUCGIAOHANG = HINHTHUCGIAOHANG.MAHINHTHUCGH AND DONDATHANG.HINHTHUCTHANHTOAN = HINHTHUCTHANHTOAN.MAHINHTHUCTT AND YEAR(DONDATHANG.NGAYDATHANG)=YEAR(GETDATE())");
        }
        public DataTable LoadDonHangTheoNgay(string Ngay, string Thang, string Nam)
        {
            return x.GetTable("select * FROM DONDATHANG,KHACHHANG,TINHTRANGDONHANG,HINHTHUCGIAOHANG,HINHTHUCTHANHTOAN WHERE DONDATHANG.MAKHACHHANG = KHACHHANG.MAKHACHHANG AND DONDATHANG.TINHTRANGDONHANG = TINHTRANGDONHANG.MATINHTRANG AND DONDATHANG.HINHTHUCGIAOHANG = HINHTHUCGIAOHANG.MAHINHTHUCGH AND DONDATHANG.HINHTHUCTHANHTOAN = HINHTHUCTHANHTOAN.MAHINHTHUCTT AND DAY(DONDATHANG.NGAYDATHANG)='"+Ngay+"' and MONTH(DONDATHANG.NGAYDATHANG) = '"+Thang+"' AND YEAR(DONDATHANG.NGAYDATHANG)='"+Nam+"'");
        }
        public void DuyetDonHang(string TinhTrangDonHang,string SoDonHang)
        {
            x.Execute("UPDATE DONDATHANG SET TINHTRANGDONHANG='"+TinhTrangDonHang+"' WHERE SODONHANG='"+SoDonHang+"'");
        }
    }
}
