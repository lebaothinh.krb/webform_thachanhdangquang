﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Public;

namespace BusinessLogic
{
    public class DatHangTheoYeuCauBusiness
    {
        XLDL x = new XLDL();
        public DataTable LoadDatHang()
        {
            return x.GetTable("select * from DATHANGTHEOYEUCAU");
        }
        public void ThemDatHang(string MaKhachHang ,string KichThuoc,string HinhAnh, string NoiDung, string VatLieu,string SoLuong)
        {
            x.Execute("INSERT INTO DATHANGTHEOYEUCAU VALUES ('"+MaKhachHang+"',N'"+KichThuoc+"',N'"+HinhAnh+"',N'"+NoiDung+"',N'"+VatLieu+"','"+SoLuong+"')");
        }
        public void XoaDatHang (string MaDatHang)
        {
            x.Execute("DELETE FROM DATHANGTHEOYEUCAU WHERE MADATHANG="+MaDatHang);
        }
        public void CapNhatDatHang (string MaDatHang, string MaKhachHang ,string KichThuoc,string HinhAnh, string NoiDung, string VatLieu,string SoLuong)
        {
            x.Execute("UPDATE DATHANGTHEOYEUCAU SET MAKHACHHANG='"+MaKhachHang+"',KICHTHUOC='"+KichThuoc+"',HINHANH=N'"+HinhAnh+"',NOIDUNG=N'"+NoiDung+"',VATLIEU=N'"+VatLieu+"',SOLUONG='"+SoLuong+"' WHERE MADATHANG="+MaDatHang);
        }
    }
}
