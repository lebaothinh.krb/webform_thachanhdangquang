﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BusinessLogic
{
    public class CTDatHang
    {
        XLDL x = new XLDL();
        public void ThemCTDatHang (string SoDonHang,string MaSanPham, int SoLuong, Double DonGia)
        {
            x.Execute("INSERT INTO CTDATHANG VALUES ('"+SoDonHang+"','"+MaSanPham+"','"+SoLuong+"','"+DonGia+"','"+DonGia*SoLuong+"')");
        }
        public void XoaCTDatHang (string SoDonHang)
        {
            x.Execute("DELETE FROM CTDATHANG WHERE SODONHANG="+SoDonHang);
        }
        public void XoaCTDatHang (string SoDonHang,string MaSanPham)
        {
            x.Execute("DELETE from CTDATHANG WHERE SODONHANG='"+SoDonHang+"' AND  MASANPHAM='"+MaSanPham+"'");
        }
        public DataTable LoadCTDatHang (string SoDonHang)
        {
            return x.GetTable("SELECT * FROM CTDATHANG WHERE SODONHANG="+SoDonHang);
        }
        public DataTable LoadCTDatHangChiTiet(string SoDonHang)
        {
            return x.GetTable("SELECT * FROM CTDATHANG,SANPHAM WHERE SANPHAM.MASANPHAM=CTDATHANG.MASANPHAM and SODONHANG=" + SoDonHang);
        }
        public void CapNhatCTDatHang(string SoDonHang,string MaSanPham, string SoLuong)
        {
            x.Execute("UPDATE CTDATHANG SET SOLUONG='"+SoLuong+"' WHERE SODONHANG='"+SoDonHang+"' AND MASANPHAM='"+MaSanPham+"'");
        }
    }
}
