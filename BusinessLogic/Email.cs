﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Data;

namespace BusinessLogic
{
    public class Email
    {
        public void XacNhanDonHang(string TenNguoiDatHang,string TenNguoiNhan, string EmailNguoiNhan, string TieuDe,string NgayGiao,string NgayDatHang, string DienThoaiGiaoHang,string MaDonHang,string DiaChiGiaoHang,string HinhThucThanhToan,DataTable GioHang,DataTable HinhMinhHoa)
        {
            MailMessage mail = new MailMessage();
            MailAddress fromAddress = new MailAddress("adthachanhdangquang@gmail.com");
            MailAddress toAddress = null;
            mail.From = fromAddress;
            toAddress = new MailAddress(EmailNguoiNhan, TenNguoiDatHang);
            mail.Subject = TieuDe;
            string ThongTinDonHang = "";
            foreach (DataRow dr in GioHang.Rows)
            {
                foreach (DataRow dr1 in HinhMinhHoa.Rows)
                {
                    if (dr["MASANPHAM"].ToString()==dr1["MASANPHAM"].ToString())
                    {
                        Attachment attach = new Attachment(@"E:\Thiet Ke & Lap Trinh Web\Do an cuoi ki\Website Ban Da\Image\Da\" + dr1["HINHMINHHOA"].ToString());
                        mail.Attachments.Add(attach);
                        break;
                    }
                }
                ThongTinDonHang += "Mã Sản Phẩm: "+dr["MASANPHAM"].ToString() + "<br>Tên Sản Phẩm: " + dr["TENSANPHAM"].ToString() + "<br>Đơn Giá: " + dr["DONGIA"].ToString() + "<br>Số Lượng: " + dr["SOLUONG"].ToString() + "<br>Thành Tiền: " + dr["THANHTIEN"].ToString() + "<br><br>";
            }
            mail.Body = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:600px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"m_-28856729815253782main-content\" style=\"width:600px\">\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"vertical-align:top\"><a href=\"http://info.lazada.vn/c/r?ACTION=ri&amp;EMID=09004HT037P0H01AKSJDO&amp;UID=2HMSHRXIOMCHB4NUDLYQ\" target=\"_blank\" title=\"\"><img alt=\"\" src=\"https://sites.google.com/site/thachanhdhangquang/_/rsrc/1379860731981/config/customLogo.gif?revision=22\" style=\"width:600px\" /></a>\r\n\t\t\t\t\t\t<p><strong>K&iacute;nh ch&agrave;o qu&yacute; kh&aacute;ch "+TenNguoiDatHang+"</strong></p>\r\n\r\n\t\t\t\t\t\t<p>Thạch Ảnh Đăng Quang&nbsp;vừa nhận được&nbsp;đơn h&agrave;ng # "+MaDonHang+"&nbsp;của qu&yacute; kh&aacute;ch đặt ng&agrave;y&nbsp;"+NgayDatHang+"&nbsp;với h&igrave;nh thức thanh to&aacute;n l&agrave;&nbsp;"+HinhThucThanhToan+". Ch&uacute;ng t&ocirc;i sẽ gửi th&ocirc;ng b&aacute;o đến qu&yacute; kh&aacute;ch qua một email kh&aacute;c ngay khi sản phẩm được giao cho đơn vị vận chuyển.</p>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:48%\">\r\n\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td style=\"vertical-align:top\">\r\n\t\t\t\t\t\t\t\t\t<p>Thời gian giao h&agrave;ng dự kiến:</p>\r\n\r\n\t\t\t\t\t\t\t\t\t<p> "+NgayGiao+"</p>\r\n\r\n\t\t\t\t\t\t\t\t\t<table cellpadding=\"10\" style=\"width:100%\">\r\n\t\t\t\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"vertical-align:middle\"><a href=\"http://localhost:1170/TinhTrangDonHang.aspx?SoDonHang="+MaDonHang+"\">T&Igrave;NH TRẠNG ĐƠN H&Agrave;NG</a></td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t</table>\r\n\r\n\t\t\t\t\t\t<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:48%\">\r\n\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td style=\"vertical-align:top\">\r\n\t\t\t\t\t\t\t\t\t<p>Đơn h&agrave;ng sẽ được giao đến:</p>\r\n\r\n\t\t\t\t\t\t\t\t\t<p><strong>"+TenNguoiNhan+"</strong></p>\r\n\r\n\t\t\t\t\t\t\t\t\t<p><strong>"+DiaChiGiaoHang+"&nbsp; Phone: "+DienThoaiGiaoHang+"</strong></p>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<p><strong>Sau đ&acirc;y l&agrave; th&ocirc;ng tin chi tiết về đơn hàng:<br>"+ThongTinDonHang+"</strong></p>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td>&nbsp;</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td style=\"vertical-align:bottom\">\r\n\t\t\t\t\t\t\t\t\t<p><strong>Qu&yacute; kh&aacute;ch cần hỗ trợ th&ecirc;m?</strong></p>\r\n\r\n\t\t\t\t\t\t\t\t\t<p>Nếu c&oacute; bất cứ thắc mắc n&agrave;o, để li&ecirc;n hệ với ch&uacute;ng t&ocirc;i, qu&yacute; kh&aacute;ch vui l&ograve;ng để lại tin nhắn tại&nbsp;<a href=\"http://localhost:1170/Default.aspx\" target=\"_blank\">đ&acirc;y</a>.</p>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n";
            mail.To.Add(toAddress);
            mail.IsBodyHtml = true;
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.Credentials =
                new System.Net.NetworkCredential("adthachanhdangquang@gmail.com", "01676709322");
            smtpClient.Send(mail);
        }
      public void QuenMatKhau (string EmailNguoiNhan, string TenNguoiNhan,string Random)
      {
          MailMessage mail = new MailMessage();
          MailAddress fromAddress = new MailAddress("adthachanhdangquang@gmail.com");
          MailAddress toAddress = null;
          mail.From = fromAddress;
          toAddress = new MailAddress(EmailNguoiNhan, TenNguoiNhan);
          mail.Subject = "Thạch Ảnh Đăng Quang gửi email thay đổi mật khẩu của bạn";
          mail.Body = "Chào " + TenNguoiNhan + ". <br>Thạch Ảnh Đăng Quang nhận được yêu cầu đổi mật khẩu của bạn. Vui lòng nhấp vào đường dẫn dưới đây để đổi mật khẩu tài khoản của bạn:<br>http://localhost:1170/QuenMatKhau.aspx?MaQuenMatKhau="+Random;
          mail.To.Add(toAddress);
          mail.IsBodyHtml = true;
          SmtpClient smtpClient = new SmtpClient();
          smtpClient.Host = "smtp.gmail.com";
          smtpClient.Port = 587;
          smtpClient.EnableSsl = true;
          smtpClient.Credentials =
              new System.Net.NetworkCredential("adthachanhdangquang@gmail.com", "01676709322");
          smtpClient.Send(mail);
      }
    }
}
