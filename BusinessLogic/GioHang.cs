﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Web;

namespace BusinessLogic
{
    public class GioHang
    {
        public Double TinhTongTien ()
        {
            DataTable dt = (DataTable)HttpContext.Current.Session["GIOHANG"];
            Double tongtien = 0;
            foreach (DataRow dr in dt.Rows)
            {
                tongtien += (Convert.ToDouble(dr["DONGIA"]) * Convert.ToDouble(dr["SOLUONG"]));
            }
            return tongtien;
        }
        public void ThemMotSanPham (int MaSanPham)
        {
            SanPhamBusiness spbn = new SanPhamBusiness();
            DataTable dt = spbn.LoadMotSanPham(MaSanPham.ToString());
            string TenSanPham = dt.Rows[0]["TENSANPHAM"].ToString();
            float DonGia = float.Parse(dt.Rows[0]["DONGIA"].ToString());
            int SoLuong = 1;
            ThemVaoGioHang(MaSanPham, TenSanPham, DonGia, SoLuong);
        }
        public void ThemVaoGioHang(int MaSanPham, string TenSanPham, float DonGia, int SoLuong)
        {
            DataTable dt;
            if (HttpContext.Current.Session["GIOHANG"] == null)
            {
                dt = new DataTable();
                dt.Columns.Add("MASANPHAM");
                dt.Columns.Add("TENSANPHAM");
                dt.Columns.Add("DONGIA");
                dt.Columns.Add("SOLUONG");
                dt.Columns.Add("THANHTIEN");
            }
            else
            {
                dt = (DataTable)HttpContext.Current.Session["GIOHANG"];
            }
            int dong = SPDaCoTrongGioHang(MaSanPham, dt);
            if (dong != -1)
            {
                dt.Rows[dong]["SOLUONG"] = Convert.ToInt32(dt.Rows[dong]["SOLUONG"]) + SoLuong;
            }
            else
            {
                DataRow dr = dt.NewRow();
                dr["MASANPHAM"] = MaSanPham;
                dr["TENSANPHAM"] = TenSanPham;
                dr["DONGIA"] = DonGia;
                dr["SOLUONG"] = SoLuong;
                dr["THANHTIEN"] = DonGia * SoLuong;
                dt.Rows.Add(dr);
            }
            HttpContext.Current.Session["GIOHANG"] = dt;
        }
        public int SPDaCoTrongGioHang(int MaSanPham, DataTable dt)
        {
            int dong = -1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i]["MASANPHAM"].ToString()) == MaSanPham)
                {
                    dong = i;
                    break;
                }
            }
            return dong;
        }
    }
}
