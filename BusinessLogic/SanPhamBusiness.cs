﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Public;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace BusinessLogic
{
    public class SanPhamBusiness
    {
        XLDL x = new XLDL();
        public DataTable LoadSanPham()
        {
            return x.GetTable("select * from SANPHAM");
        }
        public DataTable LoadTop8ChuDe(string MaChuDe)
        {
            return x.GetTable("select TOP 8 * from SANPHAM WHERE MACHUDE='" + MaChuDe + "' ORDER BY NGAYCAPNHAT DESC");
        }
        public DataTable LoadTop8TheLoai(string MaTheLoai)
        {
            return x.GetTable("select TOP 8 * from SANPHAM WHERE MATHELOAI='" + MaTheLoai + "' ORDER BY NGAYCAPNHAT DESC");
        }
        public void ThemSanPham(SanPham sp)
        {
            x.Execute("INSERT INTO SANPHAM (TENSANPHAM,DONVITINH,DONGIA,MOTA,HINHMINHHOA,MACHUDE,NGAYCAPNHAT,SOLUONGBAN,SOLANXEM,LOAI,THELOAI) VALUES (N'"+sp.TenSanPham+"',N'"+sp.DonViTinh+"','"+sp.DonGia+"',N'"+sp.MoTa+"','"+sp.HinhMinhHoa+"','"+sp.MaChuDe+"','"+sp.NgayCapNhat+"','0','0','"+sp.Loai+"','"+sp.TheLoai+"')");
        }
        public void ThemSanPham(string TenSanPham, string DonViTinh, Double DonGia, string MoTa, string HinhMinhHoa, Int32 MaChuDe,string NgayCapNhat,Int32 Loai, Int32 TheLoai)
        {
            x.Execute("INSERT INTO SANPHAM (TENSANPHAM,DONVITINH,DONGIA,MOTA,HINHMINHHOA,MACHUDE,NGAYCAPNHAT,SOLUONGBAN,SOLANXEM,LOAI,THELOAI,EMAIL) VALUES (N'" + TenSanPham + "',N'" + DonViTinh + "','" + DonGia + "',N'" + MoTa + "','" + HinhMinhHoa + "','" + MaChuDe + "','" + NgayCapNhat + "','0','0','" + Loai + "','" + TheLoai + "')");
        }
        public void XoaSanPham(String MaSanPham)
        {
            x.Execute("DELETE FROM SANPHAM WHERE MASANPHAM="+MaSanPham);
        }
        public void CapNhatSanPham (string MaSanPhamCu, string TenSanPham, string DonViTinh, string DonGia, string MoTa, string HinhMinhHoa, string MaChuDe, string NgayCapNhat, string SoLuongBan, string SoLanXem,string Loai, string TheLoai)
        {
            x.Execute("UPDATE SANPHAM SET TENSANPHAM=N'"+TenSanPham+"',DONVITINH=N'"+DonViTinh+"',DONGIA='"+DonGia+"',MOTA=N'"+MoTa+"',HINHMINHHOA='"+HinhMinhHoa+"',MACHUDE='"+MaChuDe+"',NGAYCAPNHAT='"+NgayCapNhat+"',SOLUONGBAN='"+SoLuongBan+"',SOLANXEM='"+SoLanXem+"',LOAI='"+Loai+"',THELOAI='"+TheLoai+"' WHERE MASANPHAM='"+MaSanPhamCu+"'");
        }
        public DataTable LoadMotSanPham (string MaSanPham)
        {
            return x.GetTable("select * from SANPHAM WHERE MASANPHAM="+MaSanPham);
        }
        public DataTable LoadTop5SoLanXem()
        {
            return x.GetTable("select TOP 5 * from SANPHAM ORDER BY SOLANXEM DESC");
        }
        public DataTable LoadSanPhamLoai(String MaLoai)
        {
            return x.GetTable("select * from SANPHAM WHERE LOAI=" + MaLoai);
        }
        public DataTable LoadSanPhamChuDe (String MaChuDe)
        {
            return x.GetTable("select * from SANPHAM WHERE MACHUDE="+MaChuDe);
        }
        public DataTable LoadSanPhamTheLoai (String MaTheLoai)
        {
            return x.GetTable("select * from SANPHAM WHERE THELOAI="+MaTheLoai);
        }
        public DataTable TimKiem (string Lenh)
        {
            return x.GetTable("SELECT * FROM (SELECT SANPHAM.MASANPHAM, SANPHAM.TENSANPHAM,SANPHAM.DONGIA,SANPHAM.MOTA, SANPHAM.DONVITINH,SANPHAM.HINHMINHHOA,SANPHAM.LOAI,SANPHAM.MACHUDE,SANPHAM.NGAYCAPNHAT,SANPHAM.SOLANXEM,SANPHAM.SOLUONGBAN,SANPHAM.THELOAI,CHUDE.TENCHUDE,LOAI.TENLOAI,THELOAI.TENTHELOAI FROM SANPHAM,CHUDE,LOAI,THELOAI WHERE SANPHAM.LOAI = LOAI.MALOAI AND SANPHAM.MACHUDE = CHUDE.MACHUDE AND SANPHAM.THELOAI = THELOAI.MATHELOAI) A WHERE A.TENSANPHAM LIKE N'%"+Lenh+"%' OR A.DONGIA LIKE N'%"+Lenh+"%' OR A.MOTA LIKE N'%"+Lenh+"%' OR A.TENCHUDE LIKE N'%"+Lenh+"%' OR A.TENLOAI LIKE N'%"+Lenh+"%' OR A.TENTHELOAI LIKE N'%"+Lenh+"%'");
        }
        public void CapNhatSoLanXem (string MaSanPham)
        {
            x.Execute("UPDATE SANPHAM SET SOLANXEM=(SOLANXEM+1) WHERE MASANPHAM="+MaSanPham);
        }
        public void CapNhatSoLuongBan (string MaSanPham)
        {
            x.Execute("UPDATE SANPHAM SET SOLUONGBAN=(SOLUONGBAN+1) WHERE MASANPHAM=" + MaSanPham);
        }
    }
}
